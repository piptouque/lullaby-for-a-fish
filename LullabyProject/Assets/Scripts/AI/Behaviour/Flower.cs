﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

using Utility;

namespace AI.Behaviour
{
    /// <summary>
    /// A per-flower component responsible for managing animation and phenotype parameters of a flower
    /// </summary>
    public class Flower : MonoBehaviour
    {
        #region Serialised data
        
        [Header("Dynamic")]
        [SerializeField] GameObject leafPrefab;
        [SerializeField] GameObject petalPrefab;
        [Header("Parts")]
        [SerializeField] GameObject stalkObject;
        [SerializeField] GameObject pistilObject;
        [SerializeField] GameObject stalkCentreObject;
        [SerializeField] GameObject groundObject;

        #endregion

        #region Public

        public void EnableAnimation(bool enable)
        {
            m_stalkAnimator.enabled = enable;
            foreach (var leaf in m_leaves)
            {
                leaf.EnableAnimation(enable);
            }

            foreach (var petal in m_petals)
            {
               petal.EnableAnimation(enable); 
            }
        }

        public void SetAnimationSpeed(float speed)
        {
           m_animationSpeed = speed;
           m_stalkAnimator.speed = m_animationSpeed;
           foreach (var leaf in m_leaves)
           {
               leaf.SetAnimationSpeed(m_animationSpeed);
           }
           foreach (var petal in m_petals)
           {
               petal.SetAnimationSpeed(m_animationSpeed);
           }
        }
        public void ComputeFlower(FlowerData data, FlowerData previousData)
        {
            //todo: set stalk length.
            
            bool wasAlive = previousData.isAlive;
            bool isAlive  = data.isAlive;
            bool hasChanged = data.hasChangedColour; // not used.

            if (wasAlive == isAlive)
            {
                // no change, nothing to do
                return;
            }
            if (wasAlive)
            {
                // just died.
                TriggerDying();
                return;
            }
            // the flower was just born. 
            
            SetPetals(data);
            SetLeaves(data);

            // do not forget to update the speed for the petals!
            UpdateSpeed();
            
            TriggerBorn();
        }

        #endregion

        #region Private

        /// <summary>
        /// Instantiates data.petalNumber petals, sets their rotations and their colour.
        /// </summary>
        /// <param name="data"></param>
        void SetPetals(FlowerData data)
        {
            SetPetals(data.isAlive ? data.numberPetals : 0, data.petalColour);
        }

        void SetLeaves(FlowerData data)
        {
            SetLeaves(data.isAlive ? data.numberLeaves : 0);
        }
        
        void SetPetals(int numberPetals, Color petalColour)
        {
            for (int i = 0; i < m_petals.Count; ++i)
            {
                bool isActive = i < numberPetals;
                m_petalObjects[i].SetActive(isActive);
            }
            for(int i = 0; i < numberPetals; i++)
            {
                if (i >= m_petals.Count)
                {
                    // The petals should be positioned relative to the pistil!
                    GameObject petalObject = Instantiate(
                        petalPrefab, 
                        pistilObject.transform
                        );
                    petalObject.name = $"{gameObject.name}_{petalPrefab.name}_{i}";
                    m_petalObjects.Add(petalObject);
                    var petal = petalObject.GetComponent<Petal>();
                    Assert.IsNotNull(petal);
                    m_petals.Add(petal);
                }
                
                // Homogeneous angles
                float angle = i * Mathf.PI * 2 / numberPetals;
                float angleDegree = -angle*Mathf.Rad2Deg;
                m_petals[i].SetAngle(angleDegree);
                
                // colour
                m_petals[i].SetColour(petalColour);
            }
        }
        void SetLeaves(int numberLeaves)
        {
            for (int i = 0; i < m_leaves.Count; ++i)
            {
                bool isActive = i < numberLeaves;
                m_leafObjects[i].SetActive(isActive);
            }
            for(int i = 0; i < numberLeaves; i++)
            {
                // The leaves should be positioned relative to the centre of the stalk!
                if (i >= m_leaves.Count)
                {
                    GameObject leafObject = Instantiate(leafPrefab, stalkCentreObject.transform);
                    leafObject.name = $"{gameObject.name}_{leafPrefab.name}_{i}";
                    m_leafObjects.Add(leafObject);
                    var leaf = leafObject.GetComponent<Leaf>();
                    Assert.IsNotNull(leaf);
                    m_leaves.Add(leaf);
                }
                // Random angle
                // IN DEGREES WITH QUATERNION (Unity is awful with radian and degree mix-up.)
                float angle = UnityEngine.Random.Range(0f, 360f);
                // height from ground to pistil.
                // todo: setting height does not work. whatever.
                float groundHeight = groundObject.transform.localPosition.y;
                float pistilHeight = pistilObject.transform.localPosition.y;
                float stalkCentreHeight = stalkCentreObject.transform.localPosition.y;
                float height = UnityEngine.Random.Range(groundHeight, pistilHeight) - stalkCentreHeight;
                m_leaves[i].SetHeightAngle(height, angle);
            }
        }
        

        void TriggerBorn()
        {
           // anim.Set ... pour fleur, plant 
           m_stalkAnimator.ResetTrigger(Animations.s_tDyingId);
           m_stalkAnimator.SetTrigger(Animations.s_tBornId);
           //
           foreach (var leaf in m_leaves)
           {
               leaf.TriggerBorn();
           }
           //
           foreach (var petal in m_petals)
           {
               petal.TriggerBorn();
           }
        }

        void TriggerDying()
        {
           m_stalkAnimator.ResetTrigger(Animations.s_tBornId);
           m_stalkAnimator.SetTrigger(Animations.s_tDyingId);
           //
           foreach (var leaf in m_leaves)
           {
               leaf.TriggerDying();
           }
           //
           foreach (var petal in m_petals)
           {
               petal.TriggerDying();
           }
        }
        
        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            Assert.IsNotNull(leafPrefab);
            Assert.IsNotNull(petalPrefab);
            
            Assert.IsNotNull(stalkObject);
            Assert.IsNotNull(pistilObject);
            Assert.IsNotNull(stalkCentreObject);
            Assert.IsNotNull(groundObject);

            m_stalkAnimator = stalkObject.GetComponent<Animator>();
            
            Assert.IsNotNull(m_stalkAnimator);

            m_petals = new List<Petal>();
            m_leaves = new List<Leaf>();
            m_petalObjects = new List<GameObject>();
            m_leafObjects = new List<GameObject>();

        }

        #endregion

        #region Private utility


        void UpdateSpeed()
        {
            SetAnimationSpeed(m_animationSpeed);
        }

        #endregion
        
        #region Private data

        List<Petal> m_petals;
        List<Leaf> m_leaves;
        List<GameObject> m_petalObjects;
        List<GameObject> m_leafObjects;
        int m_numberPetals;
        int m_numberLeaves;
        
        Animator m_stalkAnimator;

        float m_animationSpeed;

        #endregion

    }
}

