﻿
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Assertions;

using Interaction.Behaviour;
using Music;
using MptUnity.Audio;

namespace AI.Behaviour
{
    [RequireComponent(typeof(BoxInteractiveRangeModifier))]
    public class Garden : AbstractMusicInteractiveObject
    {
        #region Serialised data
        
        [Header("Grid")]
        [SerializeField] Vector2Int gridSize = new Vector2Int(3,3);
        [Range(0.1f, 5f)]
        [SerializeField] 
        float gridStep = 1.0f;
        [Range(0f, 1f)] [SerializeField] float startingLifeProb = 0.02f;
        [SerializeField] bool flowerToGround;
        [SerializeField] Vector3 groundDir = Vector3.down;
        [SerializeField] LayerMask groundMask;

        [Header("Flower")]
        [SerializeField] GameObject flowerPrefab;
        [SerializeField] Vector2Int petalNumberRange = new Vector2Int(3, 7);
        [SerializeField] Vector2Int leafNumberRange = new Vector2Int(0, 3);
        
        [Header("Genotype")] 
        [Range(0f, 1f)]
        [SerializeField] 
        float fitnessSurvivalThreshold = 0.5f;
        /// <summary>
        /// Minimum fitness for a flower to stay alive from a simulation step to the next.
        /// </summary>
        [SerializeField] GenotypeStrategy.Distribution petalColourDistribIdeal;
        [SerializeField] GenotypeStrategy.Distribution petalColourDistribChaotic;

        [Header("Interactions")] 
        [Range(1f, 5f)]
        [SerializeField]
        float boxMultiplier = 2f;
        
        [Header("Animation")]
        [Range(1f, 5f)]
        [SerializeField] float animationSpeed = 1f;
        [Range(0f, 3f)]
        [SerializeField] float maxAnimationDelay;
        [SerializeField] bool useRippleAnimationPattern;
        [Range(0.01f, 2f)] [SerializeField] float animationRippleSpeed = 1.4f;
        [FormerlySerializedAs("animationRippleModifier")] [Range(0f, 1f)] [SerializeField] float animationRandomModifier = 0.1f;
        
        #endregion

        #region Public

        public bool IsInGrid(int x, int z)
        {
            return (x>=0 && x<GetWidth()) && (z>=0 && z<GetDepth());
        }

        public int GetWidth()
        {
            return m_gridSizeCached.x;
        }
        
        public int GetDepth()
        {
            return m_gridSizeCached.y;
        }

        public bool IsFitToBeAlive(float fitness)
        {
            return fitness >= fitnessSurvivalThreshold;
        }

        public List<FlowerData> GetNeighborhood(int x, int z)
        {
            var neighborPos = new List<Vector2Int>();
            for (int i=-1; i<2; ++i) 
            {
                for (int j=-1; j<2; ++j) 
                {
                    if (i==0 && j==0)
                        continue;
                    neighborPos.Add( new Vector2Int(x + i, z + j));
                }
            }
 
            return neighborPos
                //Contain positions of existing neighbors
                .Where( pos => IsInGrid(pos.x, pos.y))
                //Return of flower
                .Select( pos => m_currentFlowerData[pos.x, pos.y])
                //Convert enum to List
                .ToList();
        }
        
        #endregion

        #region MonoBehaviour events

        // Population aura une grille de fleur
        // fonction qui prend une fleur et renvoie tous ses voisins (list)
        protected override void Awake()
        {
            base.Awake();

            Assert.IsTrue(fitnessSurvivalThreshold <= 1f);
            Assert.IsNotNull(flowerPrefab);
            if (petalNumberRange.x > petalNumberRange.y)
            {
                petalNumberRange = new Vector2Int(petalNumberRange.y, petalNumberRange.x);
            }
            if (leafNumberRange.x > leafNumberRange.y)
            {
                leafNumberRange = new Vector2Int(leafNumberRange.y, leafNumberRange.x);
            }

            m_boxRangeModifier = GetComponent<BoxInteractiveRangeModifier>();
            Assert.IsNotNull(m_boxRangeModifier);
            
        }

        protected override void Start()
        {
            base.Start();
            
            Reset();
        }

        void OnDrawGizmos()
        {
            // flower grid positions
            for (int x = 0; x < gridSize.x; ++x)
            {
                for (int z = 0; z < gridSize.y; ++z)
                {
            
                    Gizmos.color = Color.red;
                    var gridPos = new Vector3(x - gridSize.x / 2, 0f, z - gridSize.y / 2) * gridStep;
                    var pos = transform.TransformPoint(gridPos);
                    Gizmos.DrawSphere(pos, 0.5f * gridStep);
                }
            }
        }

        #endregion

        #region AbstractMusicInteractiveObject resolution

        /// <summary>
        /// Called when the FlutePlayer stops playing a note,
        /// Or when it exits the range of the object.
        /// </summary>
        /// <param name="noteColour"></param>
        /// <param name="note"></param>
        protected override void OnNoteStop(ENoteColour noteColour, MusicalNote note)
        {
            StepGrid(noteColour);
            if (IsGridDead())
            {
                // todo: reset grid when empty
                SetGridRandom();
            }
            UpdateGridDelayed();
        }

        protected override void OnRangeEnter()
        {
            // populate and show the grid if it is empty.
            if (IsGridDead())
            {
                SetGridRandom();
                UpdateGridDelayed();
            }
            // enable animations!
            EnableAnimation(true);
        }

        protected override void OnRangeExit()
        {
            EnableAnimation(false);
        }

        #endregion
        
        #region Private utility

        void UpdateGridDelayed()
        {
            // Update the grid with a 'wave' pattern
            // -> delay proportional to the distance to the player.
            // todo
            var playerPos = GetFlute().GetPlayer().GetPosition();
            for (int x = 0; x < GetWidth(); ++x)
            {
                for (int z = 0; z < GetDepth(); ++z)
                {
                    float delay;
                    if (useRippleAnimationPattern)
                    {
                        // update flower with a delay proportional to the distance to the player, with a random modifier.
                        //
                        bool justBorn = m_currentFlowerData[x, z].isAlive && !m_previousFlowerData[x, z].isAlive;
                        //
                        var pos = m_flowerObjects[x, z].transform.position;
                        // we want some kind of normalised distance to the player: taking into account
                        // the size of the box and the grid step (distance between two flowers) seems like a good idea.
                        float distRatio = Vector3.Distance(playerPos, pos) / (GetBoxMaxDistance() * gridStep);
                        float randMod = Random.Range(-1f, 1f) * animationRandomModifier * 0.5f;
                        // We animate from the player if the flower was just born, farthest from them if not (just died).
                        float distMod = justBorn ? distRatio : 1f - distRatio;
                        delay = Mathf.Max(0f, (distMod + randMod) / animationRippleSpeed);
                    }
                    else
                    {
                        delay = Random.Range(0f, animationRandomModifier * maxAnimationDelay);
                    }
                    StartCoroutine(UpdateFlowerDelayed(x, z, delay));
                }
            }
        }
        
        void UpdateGrid()
        {
            for (int x = 0; x < GetWidth(); ++x)
            {
                for (int z = 0; z < GetDepth(); ++z)
                {
                    // update flower
                    UpdateFlower(x, z);
                }
            }
        }

        void StepGrid(ENoteColour noteColour)
        {
            int w = GetWidth();
            int d = GetDepth();
            //

            // the cached data will only be used in update
            // 
            
            // first, compute fitness for each flower
            // and copy flower grid to cache
            // -> map operation
            for (int x = 0; x < w; ++x)
            {
                for (int z = 0; z < d; ++z)
                {
                    m_currentFlowerData[x, z].fitness = m_fitnessFunc.GetValue(m_currentFlowerData[x, z], noteColour);
                }
            }

            for (int x = 0; x < w; ++x)
            {
                for (int z = 0; z < d; ++z)
                {
                    FlowerData flower = m_currentFlowerData[x, z];
                    List<FlowerData> neighbours = GetNeighborhood(x, z);
                    
                    m_flowerDataCache[x, z] = ComputeReproduction(flower, neighbours, m_flowerDidReproduce, noteColour);
                }
            }

            // ici, flowerDataCache est à jour,
            // il faut donc répercuter ces changements dans m_currentFlowerData
            // (aussi m à j m_previousFlowerData
            for (int x = 0; x < w; ++x)
            {
                for (int z = 0; z < d; ++z)
                {
                    if (m_flowerDidReproduce[x, z])
                    {
                        m_flowerDataCache[x, z].canReproduce = false;
                    }
                }
            }
            
            for (int x = 0; x < w; ++x)
            {
                for (int z = 0; z < d; ++z)
                {
                    m_previousFlowerData[x, z] = new FlowerData(m_currentFlowerData[x, z]);
                    m_currentFlowerData[x, z] = new FlowerData(m_flowerDataCache[x, z]);
                }
            }

            // au temps t
            // pour chaque emplacement de fleur f :
            //     on récupère le voisinage f1, f2, f3, f4 (les quatre)
            //    -> on doit déterminer ce qu'il y aura à l'emplacement f au temps t + 1
            //    -> on a aussi la 'fitness' de f, (si f est morte, 'fitness' == 0), f1, f2...
            //    -> déterminer le max des fitness (f, et f1, f2, f3, f4 seulement si elles peuvent se reproduire)
            //    -> si le max est plus grand qu'un certain seuil, alors il y aura une fleur en f en t + 1.
            //  

            // on a deux fleurs f et f1 :
            // si les deux fleurs sont mortes, rien ne se passe.
            // si une des fleurs 'morte', et que l'autre peut se reproduire -> fleur morte f1 est remplacée par enfant de f 
        }

        bool IsGridDead()
        {
            // no LINQ for multi-dimensional arrays, great.
            foreach (var flower in m_currentFlowerData)
            {
                if (flower.isAlive)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flower"></param>
        /// <param name="neighbours"></param>
        /// <param name="flowerDidReproduce"></param>
        /// <param name="noteColour"></param>
        /// <returns>The flower at the same position as input, resulting from reproduction or survival.</returns>
        FlowerData ComputeReproduction(FlowerData flower, 
            List<FlowerData> neighbours, 
            bool[,] flowerDidReproduce, 
            ENoteColour noteColour)
        {
            FlowerData res = new FlowerData(flower);
            res.isAlive = IsFitToBeAlive(flower.fitness);
            
            List<FlowerData> canReproduceNeighbours = neighbours.Where(f => f.isAlive && f.canReproduce).ToList();
            
            if (!canReproduceNeighbours.Any())
            {
                // survival of lone flower.
                res.hasChangedColour = false;
                return res;
            }
            
            var neighbourFitness = canReproduceNeighbours.Select(f => f.fitness);

            float maxFitnessNeighbours = neighbourFitness.Max();
            if (res.fitness >= maxFitnessNeighbours)
            {
                // the flower survives and is not 'eaten' by neighbouring flowers.
                res.hasChangedColour = false;
                return res;
            }
            
            float maxFitness = Mathf.Max(flower.fitness, maxFitnessNeighbours);
            
            res.isAlive = IsFitToBeAlive(maxFitness);
            if (!res.isAlive)
            {
                // the flower does not survive!
                return res;
            }
            
            // à partir d'ici, le code n'est pas exécuté pour une fleur morte.
            res = m_genotypeFunc.ComputeGenotype(flower.position, canReproduceNeighbours, noteColour);
            var didReproduceNeighbourPos= canReproduceNeighbours
                .Select(f => f.position);
            foreach (var pos in didReproduceNeighbourPos)
            {
                flowerDidReproduce[pos.x, pos.y] = true;
            }
            return res;
        }

        /// <summary>
        /// Met à jour le composant Flower en (x, z) à partir du FlowerData correspondant.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="z"></param>
        void UpdateFlower(int x, int z)
        {
            Assert.IsTrue(IsInGrid(x, z));

            m_flowers[x, z].ComputeFlower(m_currentFlowerData[x, z], m_previousFlowerData[x, z]);
        }
        
        IEnumerator UpdateFlowerDelayed(int x, int z, float delay)
        {
            yield return new WaitForSeconds(delay);
            UpdateFlower(x, z); 
        }

        void Reset()
        {
            // initialise flower grid and instantiate flower objects.
            if (gridSize != m_gridSizeCached)
            {
               ReallocateGrid(); 
            }
            
            // Reset the values set in the inspector.
            ResetValues();
            // set the default state for the data.
            SetGridDead();
            
        }

        void ResetValues()
        {
            m_fitnessFunc = new FitnessStrategy();
            m_genotypeFunc = new GenotypeStrategy();
            
            groundDir.Normalize();

            m_genotypeFunc.SetPetalDistribution(
                petalColourDistribIdeal,
                petalColourDistribChaotic
            );
            m_genotypeFunc.SetPetalLeafNumberRanges(
                petalNumberRange,
                leafNumberRange
            );

            // first, instantiate the objects and get the animation components.
            for (int x = 0; x < GetWidth(); ++x)
            {
                for (int z = 0; z < GetDepth(); ++z)
                {
                    m_flowers[x, z].SetAnimationSpeed(animationSpeed);
                }
            }
            
            var size = boxMultiplier * new Vector3(GetWidth() * gridStep, 20f, GetDepth() * gridStep);
            m_boxRangeModifier.SetBox(size, transform.rotation);
        }

        void ReallocateGrid()
        {
            m_gridSizeCached = gridSize;
            //
            
            int w = GetWidth();
            int d = GetDepth();
            m_currentFlowerData = new FlowerData[w, d];
            m_previousFlowerData = new FlowerData[w, d]; // tous à null
            m_flowerDataCache = new FlowerData[w, d];
            m_flowerDidReproduce = new bool[w, d];

            if (m_flowerObjects != null)
            {
                foreach (var flowerObject in m_flowerObjects)
                {
                    // bad bad bad in Editor mode. whatever.
                    Destroy(flowerObject);
                }
            }
            
            m_flowerObjects   = new GameObject[w, d]; // tous à null
            m_flowers = new Flower[w, d];

            // first, instantiate the objects and get the animation components.
            for (int x = 0; x < w; ++x)
            {
                for (int z = 0; z < d; ++z)
                {
                    m_flowerObjects[x, z] = CreateFlower(x, z);
                    
                    // get FlowerAnim component.
                    m_flowers[x, z] = m_flowerObjects[x, z].GetComponent<Flower>();
                    Assert.IsNotNull(m_flowers[x, z]);
                    
                }
            }
        }

        void SetGridDead()
        {
            // then, create the data for the grid.
            for (int x = 0; x < GetWidth(); ++x)
            {
                for (int z = 0; z < GetDepth(); ++z)
                {
                    // allocation
                    m_currentFlowerData[x, z] = new FlowerData(new Vector2Int(x, z));
                    m_previousFlowerData[x, z] = new FlowerData(new Vector2Int(x, z));
                    // set-up 
                    m_currentFlowerData[x, z].isAlive = false;
                    m_previousFlowerData[x, z].isAlive = false;
                }
            }
        }
        
        void SetGridRandom()
        {
            // then, create the data for the grid.
            for (int x = 0; x < GetWidth(); ++x)
            {
                for (int z = 0; z < GetDepth(); ++z)
                {
                    float p = Random.Range(0f, 1f);
                    int numberPetals = Random.Range(petalNumberRange.x, petalNumberRange.y);
                    int numberLeaves = Random.Range(leafNumberRange.x, leafNumberRange.y);
                    Color petalColour = Random.ColorHSV();
                    bool isAlive = p <= startingLifeProb;
                    // allocation
                    m_currentFlowerData[x, z] = new FlowerData(new Vector2Int(x, z));
                    m_currentFlowerData[x, z].SetPetalColour(petalColour);
                    m_previousFlowerData[x, z] = new FlowerData(new Vector2Int(x, z));
                    m_previousFlowerData[x, z].SetPetalColour(petalColour);
                    // set-up 
                    m_currentFlowerData[x, z].isAlive = isAlive;
                    m_currentFlowerData[x, z].canReproduce = true;
                    m_currentFlowerData[x, z].numberPetals = numberPetals;
                    m_currentFlowerData[x, z].numberLeaves = numberLeaves;
                    m_previousFlowerData[x, z].isAlive = false;
                    //
                }
            }
        }

        GameObject CreateFlower(int x, int z)
        {
            var flowerObject = Instantiate(
                flowerPrefab,
                transform
            );
            flowerObject.name = $"{flowerPrefab.name}_[{x}, {z}]";
            
            SetFlowerTransform(flowerObject.transform, x, z);
            
            return flowerObject;
        }
        void SetFlowerTransform(Transform flowerTransform, int x, int z)
        {
            // plane position with random offset
            var gridPos = new Vector2(x - (GetWidth() - 1) / 2.0f, z - (GetDepth() - 1) / 2.0f);
            Vector2 randOffset = Random.insideUnitCircle / 2.0f;
            gridPos += new Vector2(randOffset.x,  randOffset.y);
            gridPos *= gridStep;
            
            SetFlowerTransform(flowerTransform, gridPos);
        }

        void SetFlowerTransform(Transform flowerTransform, Vector2 gridPos)
        {
            // Get the ground position at gridPos, with a certain layerMask.. Requires that the ground have a Collider.
            var worldPos = this.transform.TransformPoint(new Vector3(gridPos.x, 0f, gridPos.y));
            if (!flowerToGround)
            {
                flowerTransform.SetPositionAndRotation(worldPos, Quaternion.identity);
                return;
            }
            RaycastHit hit;
            bool hasHit = Physics.Raycast(
               worldPos,
               groundDir,
               out hit,
               Mathf.Infinity,
               groundMask
            );
            if (!hasHit)
            {
                // try the other way.
                hasHit = Physics.Raycast(
                   worldPos,
                   - groundDir,
                   out hit,
                   Mathf.Infinity,
                   groundMask
                );
            }
            if (!hasHit)
            {
                flowerTransform.SetPositionAndRotation(worldPos, Quaternion.identity);
                return;
            }
            // Set position from ground position.
            // Set rotation from normal at ground position.
            var pos = hit.point;
            var rot = Quaternion.LookRotation(flowerTransform.right, hit.normal);
            flowerTransform.SetPositionAndRotation(pos, rot);
        }

        void EnableAnimation(bool enable)
        {
            foreach (var flower in m_flowers)
            {
               flower.EnableAnimation(enable); 
            }
        }

        /// <summary>
        /// Get maximum distance from the box modifier centre.
        /// </summary>
        /// <returns></returns>
        float GetBoxMaxDistance()
        {
            return m_boxRangeModifier.GetBounds().size.magnitude * boxMultiplier * 0.5f;
        }

        #endregion

        #region Private

        BoxInteractiveRangeModifier m_boxRangeModifier;
        
        FitnessStrategy m_fitnessFunc;
        GenotypeStrategy m_genotypeFunc;
        
        // List<IA.Flower> List
        //2 dimensional array of flower
        GameObject[,] m_flowerObjects;
        Flower[,] m_flowers;
        
        FlowerData[,] m_currentFlowerData;
        FlowerData[,] m_previousFlowerData;

        Vector2Int m_gridSizeCached;

        FlowerData[,] m_flowerDataCache;
        bool[,] m_flowerDidReproduce;

        #endregion
    }

}
