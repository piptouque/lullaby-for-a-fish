﻿
using UnityEngine;
using UnityEngine.Assertions;

using Utility;

namespace AI.Behaviour
{
    public class Leaf : MonoBehaviour
    {

        public void EnableAnimation(bool enable)
        {
            m_animator.enabled = enable;
        }
        public void SetAnimationSpeed(float speed)
        {
            m_animator.speed = speed;
        }
        
        public void SetHeightAngle(float height, float angle)
        {
            transform.localPosition = Vector3.up * height;
            transform.localRotation = Quaternion.Euler(0f, angle, 0);
        }
        
        public void TriggerBorn()
        {
           m_animator.ResetTrigger(Animations.s_tDyingId);
           m_animator.SetTrigger(Animations.s_tBornId);
        }

        public void TriggerDying()
        {
           m_animator.ResetTrigger(Animations.s_tBornId);
           m_animator.SetTrigger(Animations.s_tDyingId);
        }

        void Awake()
        {
            m_animator = GetComponentInChildren<Animator>();
            Assert.IsNotNull(m_animator);
        }

        Animator m_animator;
    }
}
