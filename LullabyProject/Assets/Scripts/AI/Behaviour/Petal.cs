﻿
using UnityEngine;
using UnityEngine.Assertions;

using Utility;

namespace AI.Behaviour
{
    public class Petal : MonoBehaviour
    {
        // Start is called before the first frame update

        /// <summary>
        /// Sets colours of the petals.
        /// </summary>
        /// <param name="colour"></param>
        public void SetColour(Color colour)
        {
            // -> se conformer aux conventions d'écritures
            // -> si on écrit en anglais états-unis..; -> color
            var properties = new MaterialPropertyBlock();
            properties.SetColor(m_petalBaseColourShaderIndex,  colour);
            properties.SetColor(m_petalEmissionColourShaderIndex,  colour);
            m_petalRenderer.SetPropertyBlock(properties);
        }

        public void EnableAnimation(bool enable)
        {
            m_animator.enabled = enable;
        }

        public void SetAnimationSpeed(float speed)
        {
            m_animator.speed = speed;
        }
        
        public void TriggerBorn()
        {
           m_animator.ResetTrigger(Animations.s_tDyingId);
           m_animator.SetTrigger(Animations.s_tBornId);
        }

        public void TriggerDying()
        {
           m_animator.ResetTrigger(Animations.s_tBornId);
           m_animator.SetTrigger(Animations.s_tDyingId);
        }

        public void SetAngle(float angle)
        {
            // STAY IN LOCAL COORDINATES.
            transform.localRotation = Quaternion.Euler(0f, angle, 0f);
        }

        void Awake()
        {
            m_petalRenderer = GetComponentInChildren<Renderer>();
            m_animator = GetComponentInChildren<Animator>();
            Assert.IsNotNull(m_animator);
            Assert.IsNotNull(m_petalRenderer);
            
            m_petalBaseColourShaderIndex = Shader.PropertyToID("_BaseColor");
            m_petalEmissionColourShaderIndex = Shader.PropertyToID("_EmissionColor");
        }

        Animator m_animator;
        Renderer m_petalRenderer;
        
        int m_petalBaseColourShaderIndex;
        int m_petalEmissionColourShaderIndex;
        
        float m_angle;
    }
}
