﻿
using UnityEngine;

using Music;

namespace AI
{
    public class FitnessStrategy
    {   
        // pour chaque valeur -> une valeur
        public float GetValue(FlowerData flowerData, ENoteColour noteColour)
        {
            if (!flowerData.isAlive)
            {
                return 0f;
            }
            Color petalColour = flowerData.petalColour;
            // comparer petalColour à noteColour.
            Color colour = NoteColours.GetColour(noteColour);
            // ça doit me renvoyer une valeur dans [0; 1]
            float fitness;
            // if (noteColour == ENoteColour.eBlue)
            
            Color.RGBToHSV(petalColour, out float petalHue, out float s_, out float v_);
            Color.RGBToHSV(colour, out float noteHue, out s_, out v_);
            // Hue : [0, 1]
            // Résultat : [0, 1] -> [0, 2 * pi]
            float petalAngle = 2 * Mathf.PI * petalHue;
            float noteAngle = 2 * Mathf.PI * noteHue;
            // fitness = (1f + Mathf.Abs((petalHue - noteHue) % 1)) * 0.5f;
            // 
            Vector2 petalHueDir = new Vector2(Mathf.Cos(petalAngle), Mathf.Sin(petalAngle));
            Vector2 noteHueDir = new Vector2(Mathf.Cos(noteAngle), Mathf.Sin(noteAngle));
            float proj = Vector2.Dot(petalHueDir, noteHueDir); // In [-1, 1]
            fitness = (1f + proj) * 0.5f; // in [0, 1]
            
            return fitness;
        }
    }
}