﻿
using UnityEngine;

using Music;

namespace AI 
{
    public class FlowerData
    {
        public FlowerData(Vector2Int aPosition)
        {
            position = aPosition;
        }

        public void SetPetalColour(Color aPetalColour)
        {
            petalColour = aPetalColour;
        }

        public FlowerData(FlowerData other) : this(other.position)
        {
            numberPetals = other.numberPetals;
            numberLeaves = other.numberLeaves;
            stalkHeight = other.stalkHeight;
            fitness = other.fitness;
            canReproduce = other.canReproduce;
            isAlive = other.isAlive;
            
            petalColour = other.petalColour;
        }

        public readonly Vector2Int position;
        
        // phenotype
        public int numberPetals;
        public int numberLeaves;
        public double stalkHeight;
        public Color petalColour;
        
        // ...
        // fonction qui à partir des données de l'individu (la fleur),
        // décide de sa survie, et surtout du fait qu'elle se reproduise ou pas (prochaine itération)
        public float fitness;

        // ... data
        // todo: quand une fleur revient à la vie, elle doit pouvoir se reproduire à nouveau.
        public bool canReproduce;
        public bool isAlive;
        public bool hasChangedColour;
    }
}