
using System.Linq;
using System.Collections.Generic;

using UnityEngine;

using Music;

namespace AI
{
    public class GenotypeStrategy
    {

        [System.Serializable]
        public struct Distribution // sur la pile -> stack
        {
            public float mu;
            public float sigma;
        }
        
        public void SetPetalDistribution(
            Distribution petalColourDistribIdeal, 
            Distribution petalColourDistribChaotic)
        {
            m_petalColourDistribIdeal = petalColourDistribIdeal;
            m_petalColourDistribChaotic = petalColourDistribChaotic;
        }

        public void SetPetalLeafNumberRanges(Vector2Int petalNumberRange, Vector2Int leafNumberRange)
        {
            m_petalNumberRange = petalNumberRange;
            m_leafNumberRange = leafNumberRange;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="canReproduceFlowers">All flowers affecting the result (neighbours + original flower)</param>
        /// <returns></returns>
        public FlowerData ComputeGenotype(Vector2Int position, List<FlowerData> canReproduceFlowers, ENoteColour noteColour) 
        {
            //// COLOUR
            // La couleur du pétale, c'est la moyenne des couleurs des voisins -> ne peux pas fonctionner.
            // valeur_1 * 1/n + valeur_2 * 1/n + valeur_3 * 1/n
            // valeur_1 * b_1 + valeur_2 * b_2 + valeur_3 * b_3
            // b_i -> poids
            // si i ne participe pas à la reproduction (fitness nulle), alors b_i == 0.0
            // barycentre == moyenne pondérée (avec des poids)
            // b_i == fitness
            // valeur_finale = (valeur_1 * fitness_1 + ...) / somme_des_fitness
            var neighbourColourFitness = canReproduceFlowers.Select(data =>
            {
                // L'espace HSV est plus adapté à la moyenne entre couleurs.
                Color.RGBToHSV(data.petalColour, out float h, out float s, out float v);
                return new Vector4(h, s, v, data.fitness);
            });
            var hsvf = neighbourColourFitness.Aggregate((acc, hsvFitness) =>
                acc  + new Vector4(hsvFitness.x, hsvFitness.y, hsvFitness.z, 1f) * hsvFitness.w);
            // Le mélange de couleurs des fleurs parentes
            var petalColour = Color.HSVToRGB(hsvf.x / hsvf.w, hsvf.y / hsvf.w, hsvf.z / hsvf.w);
            // Une mutation aléatoire tendant à se rapprocher de la couleur << idéal >> -> couleur de la note jouée.
            // C'est un autre mélange
            // la couleur idéale est la couleur de la note!
            // MUTATION
            // ideal
            var idealColour = NoteColours.GetColour(noteColour);
            float factor = Random.Range(0, 1);
            float t = m_petalColourDistribIdeal.mu + m_petalColourDistribIdeal.sigma * factor; // in [0, 1]
            petalColour = Color.Lerp(petalColour, idealColour, t); // melange.
            // chaotic
            factor = Random.Range(0, 1);
            var chaoticColour = Random.ColorHSV();
            t = m_petalColourDistribChaotic.mu + m_petalColourDistribChaotic.sigma * factor; // in [0, 1]
            petalColour = Color.Lerp(petalColour, chaoticColour, t);
            
            //// Number of petals and leaves
            // The following is broken, it should return a barycentric result,
            // However it is highly unstable with repeated use,
            // so we only use a uniform distribution instead. whatever.
            /*
            Vector3 nf = canReproduceFlowers
                .Select(data => new Vector3(data.numberPetals, data.numberLeaves, data.fitness))
                .Aggregate((acc, numberFitness) => 
                    acc + new Vector3(numberFitness.x, numberFitness.y, 1f) * numberFitness.z);
            int petalNumber = Mathf.FloorToInt(nf.x / nf.z);
            int leafNumber = Mathf.FloorToInt(nf.y / nf.z);
            */
            int petalNumber = Random.Range(m_petalNumberRange.x, m_petalNumberRange.y);
            int leafNumber = Random.Range(m_leafNumberRange.x, m_leafNumberRange.y);

            var res = new FlowerData(position);
            // the newly-born flower can reproduce!
            res.isAlive = true;
            res.canReproduce = true;
            res.petalColour = petalColour;
            res.numberPetals = petalNumber;
            res.numberLeaves = leafNumber;
            res.hasChangedColour = true;
            
            return res;
        }

        Distribution m_petalColourDistribIdeal;
        Distribution m_petalColourDistribChaotic;
        Distribution m_petalNumberDistrib;
        Distribution m_leafNumberDistrib;

        Vector2Int m_petalNumberRange;
        Vector2Int m_leafNumberRange;
    }
}