﻿
using UnityEngine;
using UnityEngine.Assertions;

namespace Anim.Behaviour
{
    
    //This script manage if the torch is light on if the player goes on the right way 
    public class ActiveTorchLight : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            m_ps = GetComponent<ParticleSystem>();
            Assert.IsNotNull(m_ps);
            var emission = m_ps.emission;
            emission.enabled = false;
        }

        void OnTriggerEnter(Collider player) {
            //Switch on the light of torch (if it is on the path) in collision with the player
            if (player.CompareTag(Utility.Tags.c_playerTag)) {
                var emission =m_ps.emission;
                emission.enabled = true;
            }
        }

        ParticleSystem m_ps;

    }
}
