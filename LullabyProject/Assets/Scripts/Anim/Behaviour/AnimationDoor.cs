﻿
using UnityEngine;
using UnityEngine.Assertions;

namespace Anim.Behaviour
{
    public class AnimationDoor : MonoBehaviour
    {

    [SerializeField] bool isOpen;
    
    public void TriggerOpen()
    {
        SetOpen(!isOpen);
    }
    
    /// <summary>
    /// Change the value of IsOpen in the controller
    /// </summary>
    /// <param name="open"></param>
    void SetOpen(bool open) 
    {
        isOpen = open;
        m_animator.SetBool("isOpen", isOpen);
    }


    void Awake()
    {
        m_animator = GetComponent<Animator>();
        Assert.IsNotNull(m_animator);
    }

    void Start()
    {
        SetOpen(isOpen);
    }
    
    Animator m_animator;

    }
}
