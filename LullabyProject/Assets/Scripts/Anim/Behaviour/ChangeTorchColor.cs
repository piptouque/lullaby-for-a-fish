﻿
using UnityEngine;
using UnityEngine.Assertions;
using Interaction.Behaviour;

namespace Anim.Behaviour 
{
    public class ChangeTorchColor : MonoBehaviour
    {
        #region Serialised data

        public Color objColor;
        //Test before link with music mechanism

        public GameObject associatedObject;
        
        #endregion

        #region MonoBehaviour events
        void Awake() 
        {
            Assert.IsNotNull(associatedObject);
            m_interactive = associatedObject.GetComponent<AbstractSingleNoteInteractiveObject>();
            Assert.IsNotNull(m_interactive);
            m_ring = associatedObject.GetComponentInChildren<RingInteractiveObject>();
            Assert.IsNotNull(m_ring);

            m_torchParticles = GetComponentsInChildren<ParticleSystem>();
        }
        
        // Start is called before the first frame update
        void Start()
        {
            //Subscribe to object
            m_interactive.AddOnActivateListener((noteColour, musicalNote) => ChangeColor());
            activePath = false;
            m_ring.enabled = true;
        }
        
        #endregion

        #region Private utility

        void ChangeColor() 
        {
            if (activePath)
            {
                return;
            }

            foreach(var ps in m_torchParticles)
            {
                var emission = ps.emission;
                if(!emission.enabled)
                {
                    emission.enabled = true;
                }
                var main = ps.main;
                main.startColor = objColor;
            }
            m_ring.enabled = false;
            activePath = true;
        }
        
        #endregion

        #region Private data

        RingInteractiveObject m_ring;
        AbstractSingleNoteInteractiveObject m_interactive;
        
        ParticleSystem[] m_torchParticles;
        
        bool activePath;
        
        #endregion
    }
}
