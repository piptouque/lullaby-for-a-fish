﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

public class DisplayInstructionsManager : MonoBehaviour
{
    [SerializeField] Canvas instruction;
    private Transform _selection;
    
    void start() {
        Assert.IsNotNull(instruction);
    }

    void Update()
    {
        if (_selection != null)
        {
            instruction.enabled = false;
            _selection = null;
        }
        
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            //Get transform view by camera/mouse
            var selection = hit.transform;
            //Gameobjects which have instruction canavas
            if (selection == this.transform)
            {
                //active canvas
                instruction.enabled = true;
                _selection = selection;
            }
        }
    }
}