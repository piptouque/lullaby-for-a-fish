﻿
using UnityEngine;
using UnityEngine.Assertions;

using Interaction.Behaviour;

using MptUnity.Audio;

namespace Anim.Behaviour
{
    [RequireComponent(typeof(ViewInteractiveRangeModifier))]
    public class DoorTrigger : AbstractSingleNoteInteractiveObject
    {

        #region Serialised data

        // all the doors of the same color
        [SerializeField] GameObject[] doorObjects; 
        
        #endregion

        #region MonoBehaviour events

        protected override void Awake()
        {
            base.Awake();
            
            m_doors = new AnimationDoor[doorObjects.Length];
            int i = 0;
            foreach (var doorObject in doorObjects)
            {
                var door = doorObject.GetComponent<AnimationDoor>();
                Assert.IsNotNull(door);
                m_doors[i++] = door;
            }
        }

        protected override void OnActivate(MusicalNote note)
        {
            TriggerOpen();
        }

        void TriggerOpen()
        {
            foreach (var door in m_doors)
            {
                door.TriggerOpen();
            }
        }
        
        #endregion

        #region Private data

        AnimationDoor[] m_doors;
        
        #endregion
    }
}
