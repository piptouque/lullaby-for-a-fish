﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using IO.Behaviour;

namespace Anim.Behaviour
{
    public class HowToPlay : MonoBehaviour
    {

        bool activatedObj1;
        bool activatedObj2;

        public Canvas instStart;
        public Canvas instEnd;

        // Start is called before the first frame update
        void Start()
        {
            instStart.enabled = false;
            instEnd.enabled = false;

            Assert.IsNotNull(instStart);
            Assert.IsNotNull(instEnd);

            StartCoroutine(DisplayFirstInstructions(instStart));
            StartCoroutine(DisplayLastInstructions(instEnd));
        }

        // Update is called once per frame
        void Update()
        {       
        }

        IEnumerator DisplayFirstInstructions(Canvas canvas) {
            yield return new WaitForSeconds(5);
            canvas.enabled = true; // Enable the text so it shows
        }

        IEnumerator DisplayLastInstructions(Canvas canvas) {
            if(activatedObj1 && activatedObj2) 
            {
                canvas.enabled = true; // Enable the text so it shows
                yield return new WaitForSeconds(15);
                canvas.enabled = false; // Disable the text so it is hidden
                activatedObj1 = false;
                activatedObj2 = false;
            }
        }
    }
}
