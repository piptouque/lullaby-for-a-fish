

using UnityEngine;

using MptUnity.Audio.Behaviour;

using Utility;
using IO.Behaviour;
using Save.Behaviour;
using Environment.Behaviour;
using Music.Behaviour;
using GameScene.Behaviour;

namespace Common.Behaviour
{
    /// <summary>
    /// A MonoBehaviour with added functionalities specific to Lullaby For A Fish.
    /// IMPORTANT:
    /// A LullabyObject should NOT override Awake (or be very careful with this!)
    /// , since the Root Manager may not have been set at that point.
    /// All init should be done in Start!
    /// </summary>
    public abstract class LullabyBehaviour : MonoBehaviour
    {

        #region Public properties

        // These ones are needed by the LullabyStateBehaviours.
        public static GameAnimator GameAnimator => LullabyHandler.Instance.GameAnimatorHandle;
        
        #endregion
        #region Protected properties
        
        // Ended up being more macros than anything else,
        // but these should only be access by the LullabyBehaviours anyway.
        protected static Player Player => LullabyHandler.Instance.PlayerHandle;
        protected static PlayerState PlayerState => LullabyHandler.Instance.PlayerStateHandle;
        protected static LevelManager LevelManager => LullabyHandler.Instance.LevelManagerHandle;
        protected static LightManager LightManager => LullabyHandler.Instance.LightManagerHandle;
        protected static IJukebox BackgroundMusic => LullabyHandler.Instance.BackgroundMusicHandle;
        protected static IMusicSource AmbientNoise => LullabyHandler.Instance.AmbientNoiseHandle;
        protected static MoodController MoodController => LullabyHandler.Instance.MoodControllerHandle;
        protected static AmbianceController AmbianceController => LullabyHandler.Instance.AmbianceControllerHandle;

        #endregion

        #region Private Handler

        // We should not allow non-LullabyBehaviour to access this!
        // That's why it's internal.
        class LullabyHandler
        {

            #region Public data

            public static LullabyHandler Instance
            {
                get
                {
                    if (s_instance == null)
                    {
                        s_instance = new LullabyHandler();
                    }
                    return s_instance;
                }
            }
            
            // note: I'm aware that checking for null with MonoBehaviours is expansive.
            // I could not find any noticeable drawbacks while profiling,
            // So I'm keeping this the way it is (we could set a bool flag for every variable,
            // it's just tedious). I want some flexibility: this thing to work
            // even if not all of those are found, as it might in many valid use cases.
            // We will, however, assert if the member is requested but not readily available,
            // since it betrays a poor design choice somewhere.

            public Player PlayerHandle => m_player = UnityExtensions.FindComponentWithTagUnique(m_player, Tags.c_playerTag);
            public PlayerState PlayerStateHandle => m_playerState = UnityExtensions.FindComponentWithTagUnique(m_playerState, Tags.c_playerStateTag);
            public LevelManager LevelManagerHandle => m_levelManager = UnityExtensions.FindComponentWithTagUnique(m_levelManager, Tags.c_levelManagerTag);
            public LightManager LightManagerHandle => m_lightManager = UnityExtensions.FindComponentWithTagUnique(m_lightManager, Tags.c_lightManagerTag);
            public IJukebox BackgroundMusicHandle => m_backgroundMusic = UnityExtensions.FindComponentWithTagUnique(m_backgroundMusic, Tags.c_backgroundMusicTag);
            public IMusicSource AmbientNoiseHandle => m_ambientNoise = UnityExtensions.FindComponentWithTagUnique(m_ambientNoise, Tags.c_ambientNoiseTag);
            public MoodController MoodControllerHandle => m_moodController = UnityExtensions.FindComponentWithTagUnique(m_moodController, Tags.c_moodControllerTag);
            public AmbianceController AmbianceControllerHandle => m_ambianceController = UnityExtensions.FindComponentWithTagUnique(m_ambianceController, Tags.c_ambianceControllerTag);
            public GameAnimator GameAnimatorHandle => m_gameAnimator = UnityExtensions.FindComponentWithTagUnique(m_gameAnimator, Tags.c_gameAnimatorTag);


            #endregion


            #region Private data

            static LullabyHandler s_instance;

            GameAnimator m_gameAnimator;
            LoadingHandler m_loadingHandler;
            //
            Player m_player;
            PlayerState m_playerState;
            LevelManager m_levelManager;
            LightManager m_lightManager;
            IJukebox m_backgroundMusic;
            IMusicSource m_ambientNoise;
            MoodController m_moodController;
            AmbianceController m_ambianceController;

            #endregion
        }

        #endregion

    }
}
