
using System.Collections;

using UnityEngine;

using Utility;
using Common.Behaviour;

namespace Common.State
{
    public abstract class LullabyStateMachine : StateMachineBehaviour
    {
        #region Serialised data

        [SerializeField] bool shouldLoadFullscreen = true;
        
        #endregion
        
        #region To resolve

        protected virtual IEnumerator Load(Animator animator) { yield break; }
        
        protected virtual IEnumerator Unload(Animator animator) { yield break;}
        protected virtual void OnStateEnterBeforeLoading(Animator animator) { }
        protected virtual void OnStateEnterAfterLoading(Animator animator) { }

        protected virtual void OnStateExitBeforeUnloading(Animator animator) { }
        
        protected virtual void OnStateExitAfterUnloading(Animator animator) { }

        #endregion

        #region Protected utility

        protected Coroutine StartCoroutine(IEnumerator routine)
        {
            return LullabyBehaviour.GameAnimator.StartCoroutine(routine);
        }
        
        protected void StopCoroutine(Coroutine coroutine)
        {
            LullabyBehaviour.GameAnimator.StopCoroutine(coroutine);
        }


        protected IEnumerator LoadInternal(Animator animator)
        {
            // Should either be a whole loading screen if the previous scenes are unloaded,
            // or just an indicator if the previous scene is still visible.
            // show the loading indicator.
            ShowLoadingScreen(true);
            // Signal that this is currently loading.
            // We don't want sub-states loading at the same time,
            // because we need references from the parent state machine to be valid
            // when we are done loading a sub-state.
            // So we bridge each sub-state with this.
            // Kind of a mutex, really.
            animator.SetBool(Animations.s_isLoadingId, true);
            
            yield return Load(animator);
            
            OnStateEnterAfterLoading(animator);

            // Disable 'loading' indicator!
            ShowLoadingScreen(false);
            
            // Signal that this is done loading,
            // Another sub-state can then be loaded.
            animator.SetBool(Animations.s_isLoadingId, false);
        }

        protected IEnumerator UnloadInternal(Animator animator)
        {
            // Same thing as for Load,
            // We don't want multiple things being unloaded at once.
            animator.SetBool(Animations.s_isLoadingId, true);
            
            yield return Unload(animator);
            
            OnStateExitAfterUnloading(animator);
            
            animator.SetBool(Animations.s_isLoadingId, false);
        }
        
        #endregion

        #region Private data

        void ShowLoadingScreen(bool shouldShow)
        {
            if (shouldLoadFullscreen)
            {
                LullabyBehaviour.GameAnimator.LoadingHandler.RequestLoadingScreen(shouldShow);
            }
            else
            {
                LullabyBehaviour.GameAnimator.LoadingHandler.RequestLoadingIndicator(shouldShow);
            }
        }

        #endregion

        #region Private data

        Coroutine m_loadingCoroutine;

        #endregion
    }
}
