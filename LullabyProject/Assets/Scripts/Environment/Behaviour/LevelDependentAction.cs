﻿
using UnityEngine;
using UnityEngine.Assertions;

using Common.Behaviour;
using Utility;

namespace Environment.Behaviour
{
    public abstract class LevelDependentAction : LullabyBehaviour
    {
        #region Serialised data

        
        [SerializeField] public bool[] actionLevels = new bool[Levels.c_numberLevels];

        #endregion

        #region To resolve

        protected abstract void OnLevelSwitch(int level, bool isActive);

        #endregion
        
        #region MonoBehaviour events

        protected virtual void Awake()
        {
            Assert.IsTrue(actionLevels.Length == Levels.c_numberLevels);
        }

        protected virtual void Start()
        {
            Subscribe();
            m_isStarted = true;
        }

        void OnEnable()
        {
            if (m_isStarted)
            {
                Subscribe();
            }
        }

        void OnDisable()
        {
            UnSubscribe();
        }

        #endregion

        #region Private utility

        void OnLevelSwitch(int level)
        {
            OnLevelSwitch(level, actionLevels[level]);
        }

        void Subscribe()
        {
            if (m_isSubscribed)
            {
                return;
            }
            LevelManager.AddOnLevelSwitchListener(OnLevelSwitch);
            m_isSubscribed = true;
        }

        void UnSubscribe()
        {
            if (!m_isSubscribed)
            {
                return;
            }
            LevelManager.RemoveOnLevelSwitchListener(OnLevelSwitch);
            m_isSubscribed = false;
        }

        #endregion

        #region Private data

        bool m_isStarted;
        bool m_isSubscribed;

        #endregion
    }
}
