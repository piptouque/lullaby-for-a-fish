﻿
using UnityEngine;
using UnityEngine.Assertions;

using UI.Behaviour;

namespace Environment.Behaviour
{
    public class LevelDependentEnabled : LevelDependentAction
    {
        #region MonoBehaviour events

        #endregion

        #region Private utility

        protected override void OnLevelSwitch(int level, bool isActive)
        {
            gameObject.SetActive(isActive);
        }

        #endregion

        #region Private data

        #endregion

    }
}
