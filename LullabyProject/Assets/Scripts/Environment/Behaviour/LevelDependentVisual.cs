﻿
using UnityEngine;
using UnityEngine.Assertions;

using GameScene.Behaviour;
using Utility;

namespace Environment.Behaviour
{
    public class LevelDependentVisual : LevelDependentAction
    {
        #region MonoBehaviour events

        protected override void Awake()
        {
            base.Awake();
            
            m_renderers = GetComponentsInChildren<Renderer>();
            Assert.IsTrue(m_renderers.Length > 0);
        }
        
        #endregion

        #region Private utility

        protected override void OnLevelSwitch(int level, bool isActive)
        {
            foreach (var aRenderer in m_renderers)
            {
                aRenderer.enabled = isActive;
            }
        }

        #endregion

        #region Private data

        Renderer[] m_renderers;

        #endregion
    }
}
