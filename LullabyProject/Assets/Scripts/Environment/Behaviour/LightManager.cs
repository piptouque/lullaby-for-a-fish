﻿
using System.Linq;

using UnityEngine;
using UnityEngine.Assertions;

using Utility;

namespace Environment.Behaviour
{
    public class LightManager : MonoBehaviour
    {
        
        #region Serialised data
        
        
        [System.Serializable]
        public struct Sky
        {
            public Material skyboxMaterial; // HDR image for skybox that does the lighting
            public Color skyboxTint;
            public bool useFog;
            public FogMode fogMode;
            public Color fogColour;
            public float fogDensity;
            public Cubemap reflectionCubemap; // HDR image for reflections
            public float exposure; // Exposure of HDR used for lighting and GI
            public Vector3 sunDirection; // Directional Light (Sun) angle
            public float sunIntensity; // Intensity of directional light (sun)
            public Color sunColour; // Color of directional light (sun)
        }
        
        [SerializeField] ESky currentSky;

        [Header("Sun")] [SerializeField] GameObject sunPrefab;
        [Header("Skies")] 
        [SerializeField] Sky dawnSky;
        [SerializeField] Sky morningSky;
        [SerializeField] Sky noonSky;
        [SerializeField] Sky eveningSky;
        [SerializeField] Sky nightSky;
        
            
        #endregion

        #region Public
            
        
        public void SetSky(ESky sky)
        {
            SetSky(m_skies[(int)sky]);
            currentSky = sky;
        }
        
        #endregion

        #region MonoBehaviour events

        void Load()
        {
            
            // Sun
            Assert.IsNotNull(sunPrefab);

            if (m_sunLightObject == null)
            {
                m_sunLightObject = Instantiate(sunPrefab);
                m_sunLightObject.name += "_LIGHTMANAGER";
                m_sunLightObject.hideFlags = HideFlags.DontSave;
                m_sun = m_sunLightObject.GetComponent<Light>();
                Assert.IsNotNull(m_sun);
            }
            
            // Skies
            m_skies = new Sky[Skies.c_numberSkies] { dawnSky, morningSky, noonSky, eveningSky, nightSky };
            Assert.IsTrue(m_skies.All(sky => sky.reflectionCubemap && sky.skyboxMaterial));
        }
        
        void Awake()
        {
            Load();
            SetSky(currentSky);
        }

        void OnValidate()
        {
            // clamp current sky index.
            Load();
            SetSky(currentSky);
        }

        void OnDestroy()
        {
            Destroy(m_sunLightObject);
        }

        #endregion

        #region Private utility

        void SetSky(Sky sky)
        {
            // sky colour
            SetSkyColour(sky.skyboxMaterial, sky.exposure, sky.skyboxTint);
            RenderSettings.skybox = sky.skyboxMaterial;

            // reflection cubemap
            RenderSettings.defaultReflectionMode = UnityEngine.Rendering.DefaultReflectionMode.Custom;
            RenderSettings.customReflection = sky.reflectionCubemap;
            
            // fog
            RenderSettings.fog = sky.useFog;
            RenderSettings.fogMode = sky.fogMode;
            RenderSettings.fogDensity = sky.fogDensity;
            RenderSettings.fogColor = sky.fogColour;
            
            // sun
            RenderSettings.sun = m_sun;
            m_sun.transform.eulerAngles = sky.sunDirection;
            m_sun.intensity = sky.sunIntensity;
            m_sun.color = sky.sunColour;
        }

        static void SetSkyColour(Material skyboxMaterial, float exposure, Color tint)
        {
            // I'm not using PropertyBlocks here because RenderSettings.skybox does not have an accessible renderer.
            skyboxMaterial.SetFloat(s_exposure, exposure);
            skyboxMaterial.SetColor(s_tint, tint); 
        }
        
        #endregion

        #region Private data
            
        Light m_sun;
        GameObject m_sunLightObject;
        
        bool m_isSet;
        
        Sky[] m_skies = new Sky[Levels.c_numberLevels];
        
        static readonly int s_exposure = Shader.PropertyToID("_Exposure");
        static readonly int s_tint = Shader.PropertyToID("_Tint");

        #endregion
    }
}
