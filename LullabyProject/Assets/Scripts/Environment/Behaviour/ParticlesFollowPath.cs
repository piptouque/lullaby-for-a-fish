﻿
using System.Collections;
using UnityEngine;

namespace Environment.Behaviour
{
    /// Make particles follow a given iTween path
    public class ParticlesFollowPath : MonoBehaviour
    {
        public float time;
        public float particleSystemIndex;
        public string pathName;
        float yRotation;

        void Start()
        {
            // yRotation = -90;
            StartCoroutine(WaitCoroutine());
        }

        IEnumerator WaitCoroutine()
        {
            // time offset so that particles don't all leave at the same time
            yield return new WaitForSeconds(particleSystemIndex * 2f);
            // move particles along path (RiverPath0.... RiverPathN)
            iTween.MoveTo( gameObject, iTween.Hash(
                "path", 
                iTweenPath.GetPath(pathName), 
                "easetype", 
                iTween.EaseType.linear, 
                "time", 
                time, 
                "loopType", 
                "loop") );
            //ContinueRotation();
        }

    }
}
