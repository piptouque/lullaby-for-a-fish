
using UnityEngine;
using UnityEngine.Assertions;

using Utility;
using GameScene.Behaviour;
using Common.Behaviour;

namespace Environment.Behaviour
{
    /// <summary>
    /// Automatic entering / exiting zone when colliding with the collider.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class ZoneTrigger : LullabyBehaviour
    {
        #region Serialised data
        
        [SerializeField] EZone zone;
        [SerializeField] bool isEntrance;

        #endregion

        #region MonoBehaviour events

        
        void Start()
        {
            var managerObject = GameObject.FindWithTag(Zones.GetManagerTag(zone));
            Assert.IsNotNull(managerObject);
            m_manager = managerObject.GetComponent<ZoneManager>();
            Assert.IsNotNull(m_manager);

            m_collider = GetComponent<Collider>();
            Assert.IsNotNull(m_collider);
            m_collider.isTrigger = true;
        }

        void OnTriggerEnter(Collider other)
        {
            if (other == Player.GetCollider())
            {
                EZone currentZone = m_manager.GetCurrentZone();
                if (isEntrance && currentZone != zone)
                {
                    m_manager.EnterManagedZone();
                }
                else if (!isEntrance)
                {
                    m_manager.LeavePreviousZone();
                }
            }
        }

        #endregion

        #region Private data

        ZoneManager m_manager;
        Collider m_collider;

        #endregion
    }
}