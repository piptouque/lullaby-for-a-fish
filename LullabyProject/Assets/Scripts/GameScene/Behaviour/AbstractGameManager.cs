
using Common.Behaviour;

namespace GameScene.Behaviour
{
    public abstract class AbstractGameManager : LullabyBehaviour
    {
        #region To resolve

        public virtual void OnGameEnter() { }
        public virtual void OnGameExit() { }
        /// <summary>
        /// Beware, references might not be initialised yet.
        /// </summary>
        public virtual void OnGameLoading() { }
        /// <summary>
        /// Event worse, don't touch this method!!
        /// </summary>
        public virtual void OnGameUnloaded() { }

        #endregion
    }
}
