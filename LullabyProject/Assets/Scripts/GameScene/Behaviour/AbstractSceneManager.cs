
using UnityEngine;

using Utility;
using Common.Behaviour;

namespace GameScene.Behaviour
{
    public abstract class AbstractSceneManager : LullabyBehaviour
    {
        #region To resolve

        /// <summary>
        /// Is called when the scene is done loading.
        /// </summary>
        public virtual void OnSceneEnter() { }
        /// <summary>
        /// Is called before the scene is unloaded.
        /// </summary>
        public virtual void OnSceneExit() { }
        
        #endregion

    }
}
