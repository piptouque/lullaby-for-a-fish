﻿
using UnityEngine.Assertions;
using Utility;

namespace GameScene.Behaviour
{
    public class ConfigSceneManager : AbstractSceneManager
    {
        public void GoBack()
        {
            Scenes.EScene previous = GameAnimator.GetPreviousScene();
            switch (previous)
            {
               case Scenes.EScene.eMain: GameAnimator.FromConfigToMain(); break;
               case Scenes.EScene.ePause: GameAnimator.FromConfigToPause(); break;
               default: Assert.IsTrue(false); break;
            }
        }

        public override void OnSceneEnter()
        {
            // Player controls  should be disabled.
            Player.IsMovementEnabled = false;
            Player.IsViewEnabled = true;
            Player.IsAudioEnabled = true;
            Player.IsVisible = false;
            
            Player.SwitchActionMap(InputActions.c_configControlsActionMapName);
        }
    }
}
