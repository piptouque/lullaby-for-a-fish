﻿
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

using Utility;

namespace GameScene.Behaviour
{
    public class CreditsSceneManager : AbstractSceneManager
    {

        #region Public

        public void GoBackToMainMenu()
        {
            // we know that the first step is the intro cutscene, so we can go to it.
            GameAnimator.FromCreditsToMainMenu();
        }

        public void OnBack(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                GoBackToMainMenu();
            }
        }

        #endregion

        #region AbstractSceneManager resolution

        public override void OnSceneEnter()
        {
            // Player controls should be disabled.
            Player.IsMovementEnabled = false;
            Player.IsViewEnabled = true;
            Player.IsAudioEnabled = true;
            Player.IsVisible = false;

            Player.SwitchActionMap(InputActions.c_creditsControlsActionMapName);

            // set a random mood!
            MoodController.FillRandom();
            // play a random song, 
            // dirty trick: from 1 to avoid Main theme.
            int trackIndex = Random.Range(1, BackgroundMusics.GetNumber());
            if (trackIndex != BackgroundMusic.GetCurrentTrackIndex())
            {
                BackgroundMusic.Stop();
                BackgroundMusic.SwitchTrack(trackIndex);
                BackgroundMusic.Play();
            }
        }

        #endregion

        #region Monobehaviour events

        void Start()
        {
            Subscribe();
            m_isStarted = true;
        }
        
        void OnEnable()
        {
            if (m_isStarted)
            {
                Subscribe();
            }
        }

        void OnDisable()
        {
            if (m_backAction != null)
            {
                UnSubscribe();
            }
        }

        #endregion
        
        #region Private utility

        void Subscribe()
        {
            if (m_isSubscribed)
            {
                return;
            }
            // find unpause action! 
            m_backAction = Player.GetAction(InputActions.c_backCreditsActionName);
            Assert.IsNotNull(m_backAction);
            m_backAction.performed += OnBack;
            m_isSubscribed = true;
        }

        void UnSubscribe()
        {
            if (!m_isSubscribed)
            {
                return;
            }
            m_backAction.performed -= OnBack;
            m_isSubscribed = false;
        }
        

        #endregion

        #region Private data

        InputAction m_backAction;
        bool m_isSubscribed;
        bool m_isStarted;

        #endregion
    }
}
