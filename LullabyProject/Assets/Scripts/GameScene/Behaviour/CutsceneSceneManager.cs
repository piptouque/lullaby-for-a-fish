﻿
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Assertions;

using Utility;
using Music;

namespace GameScene.Behaviour
{
    /// Among other things:
    /// Play voice over during cutscenes, animates camera,
    /// And is called by the Animation to change scenes.
    /// Can also switch from play to cutscene and back.
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(Animation))]
    public class CutsceneSceneManager : AbstractSceneManager
    {
        #region Serialised data

        [Header("Config")] [SerializeField] bool shouldLaunchOnStartup = false;
        
        [Header("Cutscenes")]
        [SerializeField] Animation cameraAnimation;
        [SerializeField] Cutscene[] cutscenes;
        
        [Header("Ambient noise")]
        [SerializeField] AmbianceValue[] ambianceValues = new AmbianceValue[Ambiances.GetNumber()];
        
        [System.Serializable]
        struct Cutscene
        {
            public Scenes.EScene nextScene;
            public ECutscene nextCutscene; // will only be used if nextScene == eCutscene;
            
            [HideInInspector] public string cameraClipName;
            public AudioClip[] audioClips;
        }
        
        [Header("Sound effects")]
        public AudioClip[] soundClips;
        
        #endregion
        
        #region Public

        #region Launching

        public void Launch(int cutsceneIndex = 0, bool enablePlayerAudio = false)
        {
            Assert.IsTrue(cutsceneIndex >= 0 && cutsceneIndex < cutscenes.Length);
            
            // Disable view and audio of the player and enable Cutscene controls.
            // Player control should be disabled.
            // player.AreControlsEnabled = false;
            Player.IsMovementEnabled = false;
            Player.IsViewEnabled = false;
            Player.IsVisible = false;
            // If we are running without root, we use our own audio,
            // otherwise we that of the player.
            m_isPlayerAudio = enablePlayerAudio;
            // If we have Root, we take the listener of the player,
            // otherwise fall-back to child.
            Player.IsAudioEnabled = m_isPlayerAudio;
            m_audioListener.enabled= !m_isPlayerAudio;
            
            Player.SwitchActionMap(InputActions.c_cutsceneControlsActionMapName);
            
            // Stop background music, 
            BackgroundMusic.Stop();
            // Set ambient to appropriate mix
            foreach (var ambianceValue in ambianceValues)
            {
               AmbianceController.SetValue(ambianceValue.kind, ambianceValue.value); 
            }
            // disable ambiance update
            AmbianceController.IsUpdateEnabled = false;
            AmbientNoise.Play();
            
            // activate view and audio
            m_camera.enabled = true;
            m_audioSource.enabled = true;
            
            m_currentCutsceneIndex = cutsceneIndex;
            
            // play animation.
            cameraAnimation.Play(GetCurrentCutscene().cameraClipName);
        }

        #endregion

        #region Animation Event callbacks

        public void PlayAudio(int audioClipIndex)
        {
            var audioClips = GetCurrentCutscene().audioClips;
            Assert.IsTrue(audioClipIndex >= 0 && audioClipIndex < audioClips.Length);
            
            m_audioSource.clip = audioClips[audioClipIndex];
            m_audioSource.Play();
        }
        
        public void PlayBackgroundMusic(int trackIndex)
        {
            Assert.IsTrue(trackIndex >= 0 && trackIndex < BackgroundMusic.GetNumberTracks());
            
            BackgroundMusic.SwitchTrack(trackIndex);
            BackgroundMusic.Reset();
            BackgroundMusic.Play();
        }

        public void PlaySound(int soundClipIndex)
        {
            Assert.IsTrue(soundClipIndex >= 0 && soundClipIndex < soundClips.Length);
            // todo Set the appropriate mixer group
            // We can't do any better than sound effects, since we don't know the nature of the sound.
            m_audioSource.PlayOneShot(soundClips[soundClipIndex]);
        }
        
        public void SetSky(ESky sky)
        {
            LightManager.SetSky(sky);
        }
        
        #endregion

        #region InputAction events

        public void OnSkip(InputAction.CallbackContext context)
        {
            // Go to next scene / cutscene immediately.
            GoToNextScene();
        }

        #endregion

        #region Scene Switching (also callbacks)

        /// <summary>
        /// Go to next scene / cutscene after the current cutscene.
        /// </summary>
        public void GoToNextScene()
        {
            var cutscene = GetCurrentCutscene();
            switch (cutscene.nextScene)
            {
               case Scenes.EScene.ePlay: GoBackToPlay(); return; 
               case Scenes.EScene.eMain: GoBackToMain(); return;
               case Scenes.EScene.eCutscene: GoToCutscene(cutscene.nextCutscene);
                   return;
               default: Assert.IsTrue(false);
                   return;
            }
        }
        
        #endregion
        
        #endregion
        
        #region AbstractSceneManager resolution

        public override void OnSceneEnter()
        {
            // launch the cutscene, exit it when done.
            Launch();
        }
        
        public override void OnSceneExit()
        {
            // clean-up. Keeps the audio listener enabled if it is the player's.
            m_camera.enabled = false;
            m_audioSource.enabled = false;
            m_audioListener.enabled = false;

            // bring back the player's listener.
            Player.IsViewEnabled = true;
            Player.IsAudioEnabled = true;
            
            // re-enable ambiance update.
            AmbianceController.IsUpdateEnabled = true;
        }

        #endregion

        #region MonoBehaviour events

        protected virtual void Awake()
        {
            Assert.IsNotNull(cameraAnimation);
            
            Assert.IsTrue(cutscenes.All(cutscene => cutscene.audioClips.All(clip => clip != null)));
            
            Assert.IsTrue(cameraAnimation.GetClipCount() == cutscenes.Length);

            int i = 0;
            foreach (AnimationState clip in cameraAnimation)
            {
                Assert.IsNotNull(clip);
                cutscenes[i].cameraClipName = clip.name;
                ++i;
            }

            Assert.IsTrue(soundClips.All(clip => clip != null)); 
            
            Assert.IsTrue(ambianceValues.Length == Ambiances.GetNumber());

            m_camera = GetComponentInChildren<Camera>();
            Assert.IsNotNull(m_camera);
            
            m_audioListener = GetComponentInChildren<AudioListener>();
            Assert.IsNotNull(m_audioListener);
            
            m_audioSource = GetComponent<AudioSource>();
            Assert.IsNotNull(m_audioSource);
            
            m_camera.enabled = false;
            m_audioSource.enabled = false;
            m_audioListener.enabled = false;
        }

        void Start()
        {
            if (shouldLaunchOnStartup)
            {
               Launch();
            }
            Subscribe();
            m_isStarted = true;
        }

        protected virtual void OnEnable()
        {
            if (m_isStarted)
            {
                Subscribe();
            }
        }

        protected virtual void OnDisable()
        {
            UnSubscribe();
        }

        #endregion

        #region Private utility

        void Subscribe()
        {
            if (m_isSubscribed)
            {
                return;
            }

            // Find the actions!
            m_skipAction = Player.GetAction(InputActions.c_skipCutsceneActionName);
            Assert.IsNotNull(m_skipAction);
            m_skipAction.performed += OnSkip;
            m_isSubscribed = true;
        }

        void UnSubscribe()
        {
            if (!m_isSubscribed || m_skipAction == null)
            {
                return;
            }
            m_skipAction.performed -= OnSkip;
            m_isSubscribed = false;
        }

        Cutscene GetCurrentCutscene()
        {
           Assert.IsTrue(m_currentCutsceneIndex >= 0 && m_currentCutsceneIndex < cutscenes.Length);
           return cutscenes[m_currentCutsceneIndex];
        }
        
        void GoBackToPlay()
        {
            GameAnimator.FromCutsceneToPlay();
        }

        void GoBackToMain()
        {
            GameAnimator.FromCutsceneToMain();
        }

        static void GoToCutscene(ECutscene cutscene)
        {
            GameAnimator.FromPlayToCutscene(cutscene);
        }
        

        #endregion

        #region Private data

        AudioSource m_audioSource;
        string[] m_animationClipNames;
        Camera m_camera;
        AudioListener m_audioListener;

        InputAction m_skipAction;
        bool m_isPlayerAudio;

        int m_currentCutsceneIndex;
        bool m_isStarted;
        bool m_isSubscribed;

        #endregion
    }
}
