﻿
using UnityEngine;
using UnityEngine.Assertions;

using Utility;
using Music;
using Interaction.Behaviour;

namespace GameScene.Behaviour
{
    /// <summary>
    /// Used for the cutscenes where the old man goes back to the fish
    /// </summary>
    public class FishCutsceneSceneManager : CutsceneSceneManager
    {
        #region Serialised data

        [Header("Miscellaneous")]
        [SerializeField] GameObject fishObject;

        #endregion

        #region Fish events

        void OnActivateTrigger(Melody melody)
        {
            // set the state and save
            PlayerState.SetMelodyPlayed(melody.index);
            PlayerState.Save();

            // disable controls and display (handled by cutscene)
            Player.IsViewEnabled = false;
            
            // launch the cutscenes
            Launch(LevelManager.Level);
        }

        void OnLevelSwitch(int level)
        {

            // should only activate tutorial melody if we are in the tutorial (level == 0)
            // enable all the rest otherwise.
            // the enabled melodies are requested in Start elsewhere.
            for (int i = 0; i < Melodies.c_numberMelodies; ++i)
            {
                bool hasBeenPlayed = PlayerState.HasMelodyBeenPlayed(i);
                bool isTutorial = LevelManager.Level == 0;
                m_fish.EnableMelody(i, !hasBeenPlayed && (i != 0 ^ isTutorial));
            }
            // We do not allow any other melody to be played
            // but the 'easy' one during the tutorial.
        }

        #endregion

        #region MonoBehaviour events

        protected override void Awake()
        {
            base.Awake();
            
            m_fish = fishObject.GetComponent<FishInteractiveObject>();
            Assert.IsNotNull(m_fish);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            
            m_fish.AddOnMelodyTriggerListener(OnActivateTrigger);
            
            LevelManager.AddOnLevelSwitchListener(OnLevelSwitch);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            m_fish.RemoveOnMelodyTriggerListener(OnActivateTrigger);
            
            LevelManager.RemoveOnLevelSwitchListener(OnLevelSwitch);
        }

        #endregion
        
        #region Private data
        
        FishInteractiveObject m_fish;

        #endregion
    }
}
