

using UnityEngine;
using UnityEngine.Assertions;

using Utility;

namespace GameScene.Behaviour
{
    /// <summary>
    /// One Manager to rule them all. Singleton.
    /// Components other than LullabyBehaviour should not access it, so as to keep things clean and separate,
    /// but since there's no simple way to befriend classes in C#, whatever.
    /// Uses Animator to manage scenes, and gives access to some specific objects available throughout the game.
    /// Should not be directly accessed by anything other than LullabyBehaviours.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(LoadingHandler))]
    public class GameAnimator : MonoBehaviour
    {
        #region Serialised data

        [SerializeField] bool shouldStartGame = true;
        #endregion

        #region Public properties

        public LoadingHandler LoadingHandler => m_loadingHandler;

        #endregion
        
        #region Scene Management


        public void FromConfigToMain()
        {
            SetPreviousScene(Scenes.EScene.eConfig);
            
            m_animator.SetTrigger(Animations.s_tExitConfigMenuId);
            m_animator.SetTrigger(Animations.s_tEnterMainMenuId);
            
        }
        
        public void FromConfigToPause()
        {
            SetPreviousScene(Scenes.EScene.eConfig);
            
            m_animator.SetTrigger(Animations.s_tExitConfigMenuId);
            m_animator.SetTrigger(Animations.s_tEnterPauseMenuId);
        }
        
        /// <summary>
        /// Can be used to switch from Play (Game or Cutscene) to Pause menu.
        /// </summary>
        public void FromPlayToPause()
        {
            SetPreviousScene(Scenes.EScene.ePlay);
            
            m_animator.SetTrigger(Animations.s_tExitPlayId); 
            m_animator.SetTrigger(Animations.s_tEnterPauseId); 
            m_animator.SetTrigger(Animations.s_tEnterPauseMenuId); 
        }

        public void FromPlayToCutscene(ECutscene cutscene)
        {
            SetPreviousScene(Scenes.EScene.ePlay);
            
            m_animator.SetTrigger(Animations.s_tExitPlayId); 
            m_animator.SetTrigger(Animations.s_tExitGameCutsceneId);
            EnterCutscene(cutscene);
        }
        
        public void FromMainToPlay()
        {
            SetPreviousScene(Scenes.EScene.eMain);
            
            m_animator.SetTrigger(Animations.s_tExitMainMenuId);
            m_animator.SetTrigger(Animations.s_tExitMainId);
            m_animator.SetTrigger(Animations.s_tEnterGameId);
            m_animator.SetTrigger(Animations.s_tEnterPlayId);
        }

        public void FromMainMenuToCredits()
        {
            SetPreviousScene(Scenes.EScene.eMain);
            
            m_animator.SetTrigger(Animations.s_tExitMainMenuId);
            m_animator.SetTrigger(Animations.s_tEnterCreditsId);
        }


        public void FromMainToCutscene(ECutscene cutscene)
        {
            SetPreviousScene(Scenes.EScene.eMain);
            
            m_animator.SetTrigger(Animations.s_tExitMainMenuId);
            m_animator.SetTrigger(Animations.s_tExitMainId);
            EnterCutscene(cutscene);
        }


        public void FromMainToConfig()
        {
            SetPreviousScene(Scenes.EScene.eMain);
            
            m_animator.SetTrigger(Animations.s_tExitMainMenuId);
            m_animator.SetTrigger(Animations.s_tEnterConfigMenuId);
            
        }

        public void FromCreditsToMainMenu()
        {
            SetPreviousScene(Scenes.EScene.eCredits);
            
            m_animator.SetTrigger(Animations.s_tExitCreditsId);
            m_animator.SetTrigger(Animations.s_tEnterMainMenuId);
        }

        public void FromCutsceneToPlay()
        {
            // not necessarily true, but will not matter. (fixme?)
            SetPreviousScene(Scenes.EScene.eCutscene);
            
            // fixme: apparently we need first to set TExitPlay, then TEnterPlay
            // otherwise TExitPlayer gets consumed and not trigger anything??
            m_animator.SetTrigger(Animations.s_tExitCutscenePlayId);
            m_animator.SetTrigger(Animations.s_tExitGameCutsceneId);
            m_animator.SetTrigger(Animations.s_tEnterGameId);
            m_animator.SetTrigger(Animations.s_tEnterPlayId);
        }
        
        public void FromCutsceneToMain()
        {
            // not necessarily true, but will not matter. (fixme?)
            SetPreviousScene(Scenes.EScene.eCutscene);
            
            m_animator.SetTrigger(Animations.s_tExitCutscenePlayId);
            m_animator.SetTrigger(Animations.s_tExitGameCutsceneId);
            m_animator.SetTrigger(Animations.s_tEnterMainId);
            m_animator.SetTrigger(Animations.s_tEnterMainMenuId);
        }
        
        public void FromPauseToPlay()
        {
            SetPreviousScene(Scenes.EScene.ePause);
            
            m_animator.SetTrigger(Animations.s_tExitPauseMenuId);
            m_animator.SetTrigger(Animations.s_tExitPauseId);
            m_animator.SetTrigger(Animations.s_tEnterPlayId);
        }

        public void FromPauseToMain()
        {
            SetPreviousScene(Scenes.EScene.ePause);
            
            m_animator.SetTrigger(Animations.s_tExitPauseMenuId);
            m_animator.SetTrigger(Animations.s_tExitPauseId);
            m_animator.SetTrigger(Animations.s_tExitGameCutsceneId);
            m_animator.SetTrigger(Animations.s_tEnterMainId);
            m_animator.SetTrigger(Animations.s_tEnterMainMenuId);
        }
        
        public void FromPauseToConfig()
        {
            SetPreviousScene(Scenes.EScene.ePause);
            
            m_animator.SetTrigger(Animations.s_tExitPauseMenuId);
            m_animator.SetTrigger(Animations.s_tEnterConfigMenuId);
        }

        
        public Scenes.EScene GetPreviousScene()
        {
            return (Scenes.EScene) m_animator.GetInteger(Animations.s_previousSceneId);
        }

        public static void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.ExitPlaymode();
#else 
            Application.Quit();
#endif
        }
        
        #endregion
        
        #region Unity Monobehaviour events

        void Awake()
        {
            m_animator = GetComponent<Animator>();
            Assert.IsNotNull(m_animator);

            m_loadingHandler = GetComponent<LoadingHandler>();
            Assert.IsNotNull(m_loadingHandler);
        }

        void Start()
        {
            if (shouldStartGame)
            {
                // start the game by triggering the state machine.
                m_animator.SetTrigger(Animations.s_tEnterMainMenuId);
                m_animator.SetTrigger(Animations.s_tEnterMainId);
            }
        }

        /*
        todo
        void OnDestroy()
        {
           // should find a way to call all other destructors?
           // so that the instance is valid throughout the game..
           // For now, only destroy the LullabyObjects, as a safety.
           // Extremely expansive, and probably very long and unoptimised,
           // but is called only once at exit, so whatever.
           // Get the *distinct* objects, so that we do not try to delete the same object twice.
           // Still not exception proof, but since the RootManager should only be destroyed when the game exits,
           // this should not be much of an issue (everything is still .
           var lullabyObjects = FindObjectsOfType<LullabyBehaviour>()
               .Where(component => component != null)
               .Select(component => component.gameObject)
               .Distinct();
           foreach (var go in lullabyObjects)
           {
               // immediately, so that our RootManager is still valid!
               if (go != null)
               {
                   DestroyImmediate(go);
               }
           }
           // todo: destroy the static Managers (Light, Level) last so that
           //       references to them remain valid.
           //  
           //
           HasInstance = false;
        }
        */
        #endregion
        
        #region Private utility

        void SetPreviousScene(Scenes.EScene scene)
        {
            m_animator.SetInteger(Animations.s_previousSceneId, (int)scene);
        }
        
        void EnterCutscene(ECutscene cutscene)
        {
            m_animator.SetInteger(Animations.s_tCutsceneId, (int)cutscene);
            m_animator.SetTrigger(Animations.s_tEnterCutsceneId);
            m_animator.SetTrigger(Animations.s_tEnterCutscenePlayId);
        }

        #endregion

        #region Private data

        Animator m_animator;
        LoadingHandler m_loadingHandler;

        #endregion

    }
}
