
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

using Utility;

namespace GameScene.Behaviour
{
    public class GameSceneManager : AbstractSceneManager
    {
        #region Serialised data

        [SerializeField] GameObject spawnObject;

        #endregion

        #region Public

        #endregion

        #region AbstractSceneManager resolution

        public override void OnSceneEnter()
        {
            // Start the appropriate zone trigger.
            // Unnecessarily expensive, but I'd rather reuse code here.
            ZoneManager zoneManager = UnityExtensions.FindComponentWithTagUnique<ZoneManager>(null, Zones.GetManagerTag(m_spawn.zone));
            zoneManager.EnterManagedZone();
            
            // Set the level according to the number of played melodies!
            LevelManager.Level = PlayerState.GetNumberMelodyPlayed();
            // also change the skybox
            LightManager.SetSky(Skies.s_levelToSky[LevelManager.Level]);
            
            // Unfreeze mood and ambiance update
            AmbianceController.IsUpdateEnabled = true;
            MoodController.IsUpdateEnabled = true;
            
            // Play ambient noise
            AmbientNoise.Play();
            
            // Finally, move the player to the spawn location
            Player.SetPosition(spawnObject.transform.position);
            Player.SetRotation(spawnObject.transform.rotation);
        }
        

        public override void OnSceneExit()
        {
            // Stop updating mood and ambiance
            AmbianceController.IsUpdateEnabled = false;
            MoodController.IsUpdateEnabled = false;
            
            // Stop ambient noise!
            AmbientNoise.Stop();
            // Warp the player back to spawn location as a safety measure:
            // Fixes some OnTriggerExit that are not triggered when the player is warped on next OnSceneEnter
            // if they were inside a trigger collider when they exited.
            Player.SetPosition(spawnObject.transform.position);
        }

        #endregion
        
        #region MonoBehaviour stuff
        
        void Awake()
        {
            Assert.IsNotNull(spawnObject);
            m_spawn = spawnObject.GetComponent<Spawn>();
            Assert.IsNotNull(m_spawn);
        }

        #endregion
        
        #region Private data

        Spawn m_spawn;
        
        #endregion
    }
}