
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Assertions;

using Utility;

using Common.Behaviour;

namespace GameScene.Behaviour
{
    public class LevelSwitchEvent : UnityEvent<int> { }
    /// <summary>
    /// A manager that handles level switching
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class LevelManager : LullabyBehaviour
    {
        #region Serialised data

        [UnityEngine.Serialization.FormerlySerializedAsAttribute("m_level")]
        [Header("Level switching")]
        [Range(0, Levels.c_numberLevels - 1)]
        [SerializeField] int level;
        
        [Header("Audio")] 
        [SerializeField] AudioClip levelSwitchClip;
        [SerializeField] AudioClip endGameClip;
        
        
        #endregion

        #region Public

        public int Level
        {
            get => level;
            set
            {
                Assert.IsTrue(value >= 0 && value < Levels.c_numberLevels);
                level = value;
                // play a sound depending on whether the game is finished
                m_audioSource.PlayOneShot(level == Levels.c_numberLevels - 1 ? endGameClip : levelSwitchClip);
                m_events.onLevelSwitch.Invoke(level);
            }
        }

        /// <summary>
        /// Resets level and clears player save data.
        /// </summary>
        public void Reset()
        {
            PlayerState.ResetData();
            // 
            Level = 0;
        }
        
        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            Assert.IsNotNull(levelSwitchClip);
            Assert.IsNotNull(endGameClip);

            m_audioSource = GetComponent<AudioSource>();
            Assert.IsNotNull(m_audioSource);
        }

        void OnValidate()
        {
            if (m_audioSource != null)
            {
                Level = level;
            }
        }

        #endregion

        #region Events

        class Events
        {
            public readonly LevelSwitchEvent onLevelSwitch = new LevelSwitchEvent();
        }

        public void AddOnLevelSwitchListener(UnityAction<int> onLevelSwitch)
        {
           m_events.onLevelSwitch.AddListener(onLevelSwitch); 
        }
        
        public void RemoveOnLevelSwitchListener(UnityAction<int> onLevelSwitch)
        {
           m_events.onLevelSwitch.RemoveListener(onLevelSwitch); 
        }

        #endregion

        #region Private data

        AudioSource m_audioSource;
        
        readonly Events m_events = new Events();

        #endregion
    }
}