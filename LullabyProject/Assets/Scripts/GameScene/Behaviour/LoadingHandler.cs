
using System;

using UnityEngine;
using UnityEngine.Assertions;

namespace GameScene.Behaviour
{
    public class LoadingHandler : MonoBehaviour
    {
        #region Serialised data

        [SerializeField] GameObject loadingScreenPrefab;
        [SerializeField] GameObject loadingIndicatorPrefab;

        #endregion

        #region Public utility

        public void RequestLoadingScreen(bool isShowing)
        {
            // Is called in state machine, which can apparently be entered before a call to Awake here,
            // so we need to Set() before.
            Set();
            //

            // this is awful, but it works, so...
            if (isShowing)
            {
                if (m_numberLoadingScreenRequested == 0)
                {
                    ShowLoadingScreen(true);
                    if (m_numberIndicatorRequested > 0)
                    {
                        ShowLoadingIndicator(false);
                    }
                }
                ++m_numberLoadingScreenRequested;
            }
            else
            {
                m_numberLoadingScreenRequested = Math.Max(m_numberLoadingScreenRequested - 1, 0);
                if (m_numberLoadingScreenRequested == 0)
                {
                    if (m_numberIndicatorRequested > 0)
                    {
                        ShowLoadingIndicator(true);
                    }
                    ShowLoadingScreen(false);
                }
            }
        }
        
        public void RequestLoadingIndicator(bool isShowing)
        {
            // Is called in state machine, which can apparently be entered before a call to Awake here,
            // so we need to Set() before.
            Set();
            //

            // this is awful, but it works, so...
            if (isShowing)
            {
                if (m_numberIndicatorRequested == 0 && m_numberLoadingScreenRequested == 0)
                {
                    // if no indicator OR screen is already requested
                    ShowLoadingIndicator(true);
                }
                ++m_numberIndicatorRequested;
            }
            else
            {
                m_numberIndicatorRequested = Math.Max(m_numberIndicatorRequested - 1, 0);
                if (m_numberIndicatorRequested == 0)
                {
                    ShowLoadingIndicator(false);
                }
            }
        }
        
        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            Set();
        }

        void Set()
        {
            if (m_isSet)
            {
                return;
            }
            Assert.IsNotNull(loadingScreenPrefab);
            m_screenObject = Instantiate(loadingScreenPrefab);
            Assert.IsNotNull(loadingIndicatorPrefab);
            m_indicatorObject = Instantiate(loadingIndicatorPrefab);

            m_screenCanvas = m_screenObject.GetComponent<Canvas>();
            Assert.IsNotNull(m_screenCanvas);
            m_indicatorCanvas = m_indicatorObject.GetComponent<Canvas>();
            Assert.IsNotNull(m_indicatorCanvas);

            ShowLoadingScreen(false);
            ShowLoadingIndicator(false);

            m_isSet = true;
        }

        void OnDestroy()
        {
            Destroy(m_indicatorObject);
            Destroy(m_screenObject);
        }

        #endregion

        #region Private utility

        void ShowLoadingScreen(bool isShowing)
        {
            m_screenCanvas.enabled = isShowing;
        }

        void ShowLoadingIndicator(bool isShowing)
        {
            m_indicatorCanvas.enabled = isShowing;
        }
        

        #endregion

        #region Private data

        GameObject m_screenObject;
        GameObject m_indicatorObject;

        Canvas m_screenCanvas;
        Canvas m_indicatorCanvas;

        // Number of states currently loading with screen, indicator,
        // Able to only show the screen if the indicator is also requested.
        int m_numberIndicatorRequested;
        int m_numberLoadingScreenRequested;
        
        bool m_isSet;

        #endregion
    }
}