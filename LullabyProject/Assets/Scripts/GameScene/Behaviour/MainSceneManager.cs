﻿
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Assertions;

using Utility;
using UI.Behaviour;

namespace GameScene.Behaviour
{
    public class MainSceneManager : AbstractSceneManager
    {
        [SerializeField] GameObject continueGameButtonObject;

        #region Public

        public void GoToNewPlay()
        {
            // we should reset the game
            LevelManager.Reset();
            
            // we know that the first step is the intro cutscene, so we can go to it.
            GameAnimator.FromMainToCutscene(ECutscene.eIntroCutscene);
        }

        public void GoToPlay()
        {
            // player save data should already be loaded byt the Main Menu state.
        
            // go straight to play.
            GameAnimator.FromMainToPlay();
        }
        
        public void GoToCredits()
        {
            // go straight to play.
            GameAnimator.FromMainMenuToCredits();
        }


        public void QuitGame()
        {
            GameAnimator.ExitGame();
        }

        public void GoToConfig()
        {
            GameAnimator.FromMainToConfig();
        }

        public void OnQuit(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                QuitGame();
            }
        }

        #endregion

        #region AbstractSceneManager resolution

        public override void OnSceneEnter()
        {
            //load the player data
            // here, save data is created if it did not already exist.
            PlayerState.Load();
            
            // Continue button should only be enabled if the save file is not new.
            m_continueGameButton.enabled = !PlayerState.IsNew();
            
            // Player movement should be disabled. 
            Player.IsMovementEnabled = false;
            Player.IsViewEnabled = true;
            Player.IsAudioEnabled = true;
            Player.IsVisible = false;
            
            
            Player.SwitchActionMap(InputActions.c_mainControlsActionMapName);
            
            // Freeze Mood Update, normal mood
            MoodController.IsUpdateEnabled = false;
            MoodController.FillValues(1f);
            
            // play Menu theme!
            BackgroundMusic.SwitchTrack((int)EBackgroundMusic.eMenu);
            BackgroundMusic.Play();
            BackgroundMusic.Play();
        }

        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            Assert.IsNotNull(continueGameButtonObject);
            m_continueGameButton = continueGameButtonObject.GetComponent<LullabyButton>();
            Assert.IsNotNull(m_continueGameButton);

        }

        void Start()
        {
            Subscribe();
            m_isStarted = true;
        }

        void OnEnable()
        {
            if (m_isStarted)
            {
                Subscribe();
            }
        }

        void OnDisable()
        {
            if (m_quitAction != null)
            {
                UnSubscribe();
            }
        }
        
        #endregion

        #region Private utility

        void Subscribe()
        {
            if (m_isSubscribed)
            {
                return;
            }
            // find unpause action! 
            m_quitAction = Player.GetAction(InputActions.c_quitGameActionName);
            Assert.IsNotNull(m_quitAction);
            m_quitAction.performed += OnQuit;
            m_isSubscribed = true;
        }

        void UnSubscribe()
        {
            if (!m_isSubscribed)
            {
                return;
            }
            m_quitAction.performed -= OnQuit;
            m_isSubscribed = false;
        }

        #endregion

        #region Private data

        LullabyButton m_continueGameButton;
        InputAction m_quitAction;
        bool m_isSubscribed;
        bool m_isStarted;

        #endregion
    }
}
