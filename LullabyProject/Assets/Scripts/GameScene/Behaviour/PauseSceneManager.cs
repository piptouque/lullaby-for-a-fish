﻿
using UnityEngine.InputSystem;
using UnityEngine.Assertions;

using Utility;

namespace GameScene.Behaviour
{
    public class PauseSceneManager : AbstractSceneManager
    {
        #region Public

        #region InputAction events

        public void OnUnpause(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                GoBackToPlay();
            }
        }

        #endregion

        #region Scene Switching

        public void GoBackToPlay()
        {
            GameAnimator.FromPauseToPlay();
        }

        public void GoBackToMain()
        {
            GameAnimator.FromPauseToMain();
        }
        
        public void GoToConfig()
        {
            GameAnimator.FromPauseToConfig();
        }
        
        #endregion
        
        #endregion

        #region AbstractSceneManager resolution

        public override void OnSceneEnter()
        {
            // Player control should be disabled, but not the view and audio.
            // we can keep the camera, though
            Player.IsMovementEnabled = false;
            Player.IsViewEnabled = true;

            // no simulation
            UnityEngine.Physics.autoSimulation = false;

            Player.SwitchActionMap(InputActions.c_pauseControlsActionMapName);
            // todo: freeze game and rendering somehow.
            
            // stop ambient noise, why not.
            AmbientNoise.Pause();
        }

        public override void OnSceneExit()
        {
            // todo: un-freeze
            // yes simulation
            UnityEngine.Physics.autoSimulation = true;
            
            // Bring back the ambient.
            AmbientNoise.Play();
        }

        #endregion

        #region MonoBehaviour events

        void Start()
        {
           Subscribe();
           m_isStarted = true;
        }
        
        void OnEnable()
        {
            if (m_isStarted)
            {
                Subscribe();
            }
        }

        void OnDisable()
        {
            if (m_unpauseAction != null)
            {
                UnSubscribe();
            }
        }

        #endregion

        #region Private utility

        void Subscribe()
        {
            if (m_isSubscribed)
            {
                return;
            }
            // find unpause action! 
            m_unpauseAction = Player.GetAction(InputActions.c_unpauseActionName);
            Assert.IsNotNull(m_unpauseAction);
            m_unpauseAction.performed += OnUnpause;
            m_isSubscribed = true;
        }

        void UnSubscribe()
        {
            if (!m_isSubscribed)
            {
                return;
            }
            m_unpauseAction.performed -= OnUnpause;
            m_isSubscribed = false;
        }
        

        #endregion

        #region Private data

        InputAction m_unpauseAction;
        bool m_isSubscribed;
        bool m_isStarted;

        #endregion
    }
}
