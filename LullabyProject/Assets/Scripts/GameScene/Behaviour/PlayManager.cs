
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

using Utility;

namespace GameScene.Behaviour
{
    /// <summary>
    /// Contrarily to the others, this one does not load a scene,
    /// but should be in charge of changes specific to switching from PauseMenu to in-game play.
    /// Such as, freezing rendering of some such.
    /// </summary>
    public class PlayManager : AbstractGameManager
    {

        #region Input events

        public void OnPause(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                GameAnimator.FromPlayToPause();
            }
        }
        
        #endregion

        #region AbstractGameManager resolution

        public override void OnGameEnter()
        {
            // controls and camera should be enabled here.
            Player.IsMovementEnabled = true;
            Player.IsViewEnabled = true;
            Player.IsAudioEnabled = true;
            Player.IsVisible = true;

            Player.SwitchActionMap(InputActions.c_gameControlsActionMapName);
        }
        
        #endregion

        #region MonoBehaviour events

        
        void Start()
        {
            Subscribe();
            m_isStarted = true;
        }

        void OnEnable()
        {
            if (m_isStarted)
            {
                Subscribe();
            }
        }

        void OnDisable()
        {
            if (m_pauseAction != null)
            {
                UnSubscribe();
            }
        }

        #endregion

        #region Private utility

        void Subscribe()
        {
            if (m_isSubscribed)
            {
                return;
            }
            // find unpause action! 
            m_pauseAction = Player.GetAction(InputActions.c_pauseActionName);
            Assert.IsNotNull(m_pauseAction);
            m_pauseAction.performed += OnPause;
            m_isSubscribed = true;
        }

        void UnSubscribe()
        {
            if (!m_isSubscribed)
            {
                return;
            }
            m_pauseAction.performed -= OnPause;
            m_isSubscribed = false;
        }
        

        #endregion

        #region Private data
        
        InputAction m_pauseAction;
        bool m_isSubscribed;
        bool m_isStarted;

        #endregion
    }
}