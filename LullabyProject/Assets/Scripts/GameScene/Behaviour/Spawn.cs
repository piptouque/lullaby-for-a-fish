
using UnityEngine;

using Utility;

namespace GameScene.Behaviour
{
    /// <summary>
    /// Holds data on how to handle the Player at start-up.
    /// </summary>
    public class Spawn : MonoBehaviour
    {
        #region Serialised data

        /// <summary>
        /// The Zone in which the player spawns. Does not change their location.
        /// </summary>
        public EZone zone; 

        #endregion

        #region MonoBehaviour events

        void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(transform.position, 5f);
        }

        #endregion
    }
}