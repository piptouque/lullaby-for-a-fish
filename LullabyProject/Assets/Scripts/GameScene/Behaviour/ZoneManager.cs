
using UnityEngine;
using UnityEngine.Assertions;

using Utility;

namespace GameScene.Behaviour
{
    /// <summary>
    /// A GameManager for each zone!
    /// note: Does not have to be put in the same scene as the zone.
    /// </summary>
    public class ZoneManager : AbstractGameManager
    {
        #region Serialised data
        
        [Header("Settings")]
        public EZone zone;
        public EBackgroundMusic track;

        #endregion

        #region Public

        public EZone GetCurrentZone()
        {
            return (EZone)m_zoneAnimator.GetInteger(Animations.s_eZoneId);
        }
        
        public void EnterManagedZone()
        {
            EnterZone(zone);
        }
        
        public void LeavePreviousZone()
        {
           m_zoneAnimator.SetInteger(Animations.s_eZoneId, (int)EZone.eNone);
           m_zoneAnimator.SetTrigger(Animations.s_tZoneExitId); 
           
        }

        #endregion

        #region AbstractGameManager resolution

        public override void OnGameEnter()
        {
            // play the appropriate track!
            BackgroundMusic.Stop();
            BackgroundMusic.SwitchTrack((int)track);
            BackgroundMusic.Play();
            
            // Unfreeze update of Mood!
            MoodController.IsUpdateEnabled = true;
        }
        
        public override void OnGameExit()
        {
           // Zero-out mood and stop audio update!
           // This is done, in part, to get smooth audio transitions between zones.
           MoodController.FillValues(0f);
           MoodController.IsUpdateEnabled = false;
        }
        
        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            m_zoneAnimator = UnityExtensions.FindComponentWithTagUnique(m_zoneAnimator, Tags.c_zoneAnimatorTag);
            Assert.IsNotNull(m_zoneAnimator);
        }
        
        #endregion

        #region Private utility
        
        void EnterZone(EZone aZone)
        {
            m_zoneAnimator.SetInteger(Animations.s_eZoneId, (int)aZone);
            m_zoneAnimator.SetTrigger(Animations.s_tZoneEnterId);
        }

        #endregion

        #region Private data

        Animator m_zoneAnimator;

        #endregion
    }
}