
using System.Collections;

using UnityEngine;

using Common.State;
using GameScene.Behaviour;

namespace GameScene.State
{
    public abstract class AbstractGameState : AbstractState
    {
        #region AbstractGameStateBehaviour partial resolution

        protected sealed override void OnStateEnterAfterLoading(Animator animator)
        {
            GetGameManager().OnGameEnter();
        }

        protected sealed override void OnStateExitBeforeUnloading(Animator animator)
        {
            GetGameManager().OnGameExit();
        }

        protected sealed override void OnStateEnterBeforeLoading(Animator animator)
        {
            base.OnStateEnterBeforeLoading(animator);
        }

        protected sealed override void OnStateExitAfterUnloading(Animator animator)
        {
            base.OnStateExitAfterUnloading(animator);
        }

        protected override IEnumerator Load(Animator animator)
        {
            yield break;
        }

        protected sealed override IEnumerator Unload(Animator animator)
        {
            yield break;
        }

        #endregion

        #region To resolve

        /// <summary>
        /// Get the tag of the manager that should be linked to this state.
        /// This is done to use multiple GameManager of the same sub-class at once.
        /// </summary>
        /// <returns></returns>
        protected abstract AbstractGameManager GetGameManager();

        #endregion

        #region Private utility

        #endregion
    }
}
