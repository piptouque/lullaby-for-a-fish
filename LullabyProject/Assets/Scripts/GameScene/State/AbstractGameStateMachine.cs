
using System.Collections;

using UnityEngine;

using GameScene.Behaviour;
using Common.State;

namespace GameScene.State
{
    public abstract class AbstractGameStateMachine : AbstractStateMachine
    {
        #region AbstractGameStateBehaviour partial resolution

        protected sealed override void OnStateEnterAfterLoading(Animator animator)
        {
            GetGameManager().OnGameEnter();
        }

        protected sealed override void OnStateExitBeforeUnloading(Animator animator)
        {
            GetGameManager().OnGameExit();
        }

        protected sealed override void OnStateEnterBeforeLoading(Animator animator)
        {
            GetGameManager().OnGameLoading();
        }

        protected sealed override void OnStateExitAfterUnloading(Animator animator)
        {
            GetGameManager().OnGameUnloaded();
        }

        protected override IEnumerator Load(Animator animator)
        {
            yield break;
        }

        protected sealed override IEnumerator Unload(Animator animator)
        {
            yield break;
        }

        #endregion

        #region To resolve

        protected abstract AbstractGameManager GetGameManager();
        
        #endregion

    }
}
