
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

using Utility;
using GameScene.Behaviour;

namespace GameScene.State
{
    /// <summary>
    /// Is templated in order to automate look-up of TSceneManager according to its sub-class.
    /// </summary>
    /// <typeparam name="TSceneManager"></typeparam>
    public abstract class AbstractSceneStateMachine<TSceneManager> : AbstractStateMachine
        where TSceneManager : AbstractSceneManager
    {
        #region AbstractGameStateBehaviour partial resolution

        protected sealed override void OnStateEnterAfterLoading(Animator animator)
        {
            GetSceneManager().OnSceneEnter();
        }

        protected sealed override void OnStateExitBeforeUnloading(Animator animator)
        {
            GetSceneManager().OnSceneExit();
        }

        protected sealed override void OnStateEnterBeforeLoading(Animator animator)
        {
        }

        protected sealed override void OnStateExitAfterUnloading(Animator animator)
        {
        }

        protected sealed override IEnumerator Load(Animator animator)
        {
            yield return EnterSceneBatch(GetBatchName(animator));
        }

        protected sealed override IEnumerator Unload(Animator animator)
        {
            yield return ExitSceneBatch(GetBatchName(animator));
        }

        #endregion

        #region To resolve

        /// <summary>
        /// Name batch of the scene in the scene batch list in Utility.Scenes.c_gameSceneLoadingBatches.
        /// </summary>
        protected abstract string GetBatchName(Animator animator);

        #endregion

        #region Private utility

        IEnumerator EnterSceneBatch(string batchName)
        {
            var sceneNames = GetSceneBatchNames(batchName);

            var batchScene = SceneManager.CreateScene(batchName);
            
            var asyncOps = new AsyncOperation[sceneNames.Count];

            for (int i = 0; i < asyncOps.Length; ++i)
            {
                asyncOps[i] = SceneManager.LoadSceneAsync(sceneNames[i], LoadSceneMode.Additive);
            }
            
            // Wait for loading to complete.
            while (!asyncOps.All(op => op.isDone))
            {
                yield return null;
            }

            // then merge all scenes in the batch in one.
            // todo: pretty sure this is expensive as heck, on top of not being asynchronous.
            //       so far it seems to be okay.
            for (int i = 0; i < sceneNames.Count; ++i)
            {
                SceneManager.MergeScenes(
                    SceneManager.GetSceneByName(sceneNames[i]), 
                    batchScene
                    );
            }
        }

        IEnumerator ExitSceneBatch(string batchName)
        {
            var asyncOp = SceneManager.UnloadSceneAsync(batchName);
            
            // Wait for unloading to complete.
            while (!asyncOp.isDone)
            {
                yield return null;
            }
            
        }

        List<string> GetSceneBatchNames(string batchName)
        {
            bool found = Scenes.s_batchToSceneNames.TryGetValue(batchName, out List<string> sceneNames);
            Assert.IsTrue(found);
            return sceneNames;
        }

        TSceneManager GetSceneManager()
        {
            // Expensive, but is not called often, so it's all right.
            // Could be optimised by using tags, but that would take away the generic aspect of this.
            return FindObjectOfType<TSceneManager>();
        }

        
        #endregion
    }
}
