

using UnityEngine;

using Common.State;

namespace GameScene.State
{
    public abstract class AbstractState : LullabyStateMachine
    {
        #region StateMachineBehaviour stuff

        // IMPORTANT:
        // Here using OnStateEnter in order to use with simple states (and not state machines)
        public sealed override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            
            // Important:
            // Resetting the triggers should be
            // the responsibility of the derived class.
            OnStateEnterBeforeLoading(animator);
            
            // loading the scenes.
            StartCoroutine(LoadInternal(animator));
        }

        // IMPORTANT:
        // same as OnStateEnter.
        public sealed override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            OnStateExitBeforeUnloading(animator);

            // this corresponds to the list of scenes which must be loaded.
            
            // should not show anything during unloading. 
            // it ought to be done by the next scene anyway.
            StartCoroutine(UnloadInternal(animator));
        }

        #endregion
    }
}
