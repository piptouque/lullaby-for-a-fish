

using UnityEngine;

using Common.State;

namespace GameScene.State
{
    public abstract class AbstractStateMachine : LullabyStateMachine
    {
        #region StateMachineBehaviour stuff

        // IMPORTANT:
        // Using OnStateMachineEnter here instead of OnStateEnter
        // in order not to call it when entering sub-state machines.
        public sealed override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
        {
            
            // Important:
            // Resetting the triggers should be
            // the responsibility of the derived class.
            OnStateEnterBeforeLoading(animator);
            
            
            // loading the scenes.
            StartCoroutine(LoadInternal(animator));
        }

        // IMPORTANT:
        // same as OnStateMachineEnter, we don't want to call it when exiting a sub-state machine.
        public sealed override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
        {
            OnStateExitBeforeUnloading(animator);

            // should not show anything during unloading. 
            // it ought to be done by the next scene anyway.
            StartCoroutine(UnloadInternal(animator));
        }
        
        #endregion
    }
}
