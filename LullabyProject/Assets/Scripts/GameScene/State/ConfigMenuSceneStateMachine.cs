﻿
using UnityEngine;

using Utility;
using GameScene.Behaviour;

namespace GameScene.State
{
    public class ConfigMenuSceneStateMachine : AbstractSceneStateMachine<ConfigSceneManager>
    {
            #region StateMachineBehaviour stuff

            protected override string GetBatchName(Animator animator)
            {
                return Scenes.c_configMenuBatchName;
            }

            #endregion
    }
}
