﻿
using UnityEngine;

using Utility;
using GameScene.Behaviour;

namespace GameScene.State
{
    public class CreditsSceneStateMachine : AbstractSceneStateMachine<CreditsSceneManager>
    {
            #region StateMachineBehaviour stuff

            protected override string GetBatchName(Animator animator)
            {
                return Scenes.c_creditsBatchName;
            }

            #endregion
    }
}
