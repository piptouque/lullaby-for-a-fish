
using UnityEngine;

using Utility;
using GameScene.Behaviour;

namespace GameScene.State
{
    public class CutsceneSceneStateMachine : AbstractSceneStateMachine<CutsceneSceneManager>
    {
            #region StateMachineBehaviour stuff

            protected override string GetBatchName(Animator animator)
            {
                ECutscene cutscene = (ECutscene) animator.GetInteger("CutsceneIndex");
                switch (cutscene)
                {
                    case ECutscene.eIntroCutscene: return Scenes.c_introCutsceneBatchName;
                    case ECutscene.eEndIntroCutscene: return Scenes.c_endIntroCutsceneBatchName;
                    case ECutscene.eEndFirstLevelCutscene: return Scenes.c_endFirstLevelCutsceneBatchName;
                    case ECutscene.eEndSecondLevelCutscene: return Scenes.c_endSecondLevelCutsceneBatchName;
                    case ECutscene.eEndThirdLevelCutscene: return Scenes.c_endThirdLevelCutsceneBatchName;
                    default: return null;
                }
            }

            #endregion
    }
}
