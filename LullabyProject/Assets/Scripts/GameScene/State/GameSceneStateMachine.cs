
using UnityEngine;

using Utility;
using GameScene.Behaviour;

namespace GameScene.State
{
    public class GameSceneStateMachine : AbstractSceneStateMachine<GameSceneManager>
    {
            #region StateMachineBehaviour stuff

            protected override string GetBatchName(Animator animator)
            {
                return Scenes.c_gameBatchName;
            }

            #endregion
    }
}
