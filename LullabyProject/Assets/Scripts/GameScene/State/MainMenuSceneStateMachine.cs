using UnityEngine;

using Utility;
using GameScene.Behaviour;

namespace GameScene.State
{
    public class MainMenuSceneStateMachine : AbstractSceneStateMachine<MainSceneManager>
    {
            #region StateMachineBehaviour stuff
            
            protected override string GetBatchName(Animator animator)
            {
                return Scenes.c_mainMenuBatchName;
            }

            #endregion
    }
}
