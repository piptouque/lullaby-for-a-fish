﻿

using UnityEngine;

using Utility;
using GameScene.Behaviour;

namespace GameScene.State
{
    public class PauseMenuSceneStateMachine : AbstractSceneStateMachine<PauseSceneManager>
    {
            #region StateMachineBehaviour stuff

            protected override string GetBatchName(Animator animator)
            {
                return Scenes.c_pauseMenuBatchName;
            }

            #endregion
    }
}
