
using UnityEngine;
using UnityEngine.Assertions;

using Utility;
using GameScene.Behaviour;

namespace GameScene.State
{
    /// <summary>
    /// The state that corresponds to 'Play' / 'Paused"
    /// </summary>
    public class PlayState : AbstractGameState
    {
        protected override AbstractGameManager GetGameManager()
        {
            // Find it with its tag!
            var gameManagerObject = GameObject.FindWithTag(Tags.c_playManagerTag);
            var gameManager = gameManagerObject.GetComponent<PlayManager>();
            Assert.IsNotNull(gameManager);
            return gameManager;
        }
    }
}
