
using UnityEngine;
using UnityEngine.Assertions;

using Utility;
using GameScene.Behaviour;

namespace GameScene.State
{
    /// <summary>
    /// A Game Manager for the 'Play' state (as opposed to 'Paused') in game animator.
    /// </summary>
    public class ZoneState : AbstractGameState
    {
        #region Serialised data

        [SerializeField] EZone zone;

        #endregion
        protected override AbstractGameManager GetGameManager()
        {
            // Find it with its tag!
            var zoneManager = UnityExtensions.FindComponentWithTagUnique<ZoneManager>(null, Zones.GetManagerTag(zone));
            Assert.IsNotNull(zoneManager);
            Assert.IsTrue(zoneManager.zone == zone);
            return zoneManager;
        }
    }
}
