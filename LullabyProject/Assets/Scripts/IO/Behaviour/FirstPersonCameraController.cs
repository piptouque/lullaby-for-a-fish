
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

using Utility;

namespace IO.Behaviour
{
    public class FirstPersonCameraController : MonoBehaviour
    {

        #region Serialised data

        [Header("Sensitivity")]
        [Range(5f, 20f)]
        [SerializeField]
        float lookSensitivity = 10f;

        [Header("Look angles")]
        [Range(10f, 90f)]
        [SerializeField] float maxUpAngle = 40f; // in degrees!
        [Range(-90f, -10f)]
        [SerializeField] float minUpAngle = -30f; // in degrees!

        #endregion

        #region Public utility

        public void SetInputLookDirection(Vector2 inputLookDir)
        {
            m_inputLookDir = inputLookDir;
        }

        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            m_movementController = GetComponentInParent<MovementController>();
            Assert.IsNotNull(m_movementController);
            m_isCursorVisible = Cursor.visible;
        }

        void Start()
        {
            m_eulerAngles = transform.localEulerAngles;
        }

        void FixedUpdate()
        {
            var look = m_inputLookDir * (lookSensitivity * Time.fixedDeltaTime);

            // Clamping here is to prevent the player from turning their head over their shoulders or under their legs
            m_eulerAngles[0] = Mathf.Clamp( m_eulerAngles[0] - look.y, minUpAngle, maxUpAngle);
            m_eulerAngles[1] += look.x;

            // Rotate the camera around the x-axis
            //transform.localEulerAngles = m_eulerAngles;
            transform.localRotation = Quaternion.Euler(m_eulerAngles[0], 0f, 0f);
            m_movementController.transform.Rotate(Vector3.up * look.x);
        }

        void OnEnable()
        {
            // Hide mouse cursor.
            HideCursor();
        }

        void OnDisable()
        {
            // Hide mouse cursor.
            UnHideCursor();
        }

        void OnApplicationFocus(bool hasFocus)
        {
            UpdateCursorVisibility();
        }

        #endregion

        #region Private utility
        
        // Save the previous state of the cursor
        void HideCursor()
        {
            m_isCursorVisible = false;
            UpdateCursorVisibility();
        }

        void UnHideCursor()
        {
            m_isCursorVisible = true;
            UpdateCursorVisibility();
        }

        void UpdateCursorVisibility()
        {
            /*
            if (m_isCursorVisible && !Cursor.visible)
            {
                // pop back to the centre of the screen, so as to emulate CursorLockMode.Locked state.
                var disp = Display.main;
                float w = disp.renderingWidth;
                float h = disp.renderingHeight;
                Mouse.current.WarpCursorPosition(new Vector2(w, h) * 0.5f);
            }
            Cursor.visible = m_isCursorVisible;
            */
            Cursor.lockState = m_isCursorVisible ? CursorLockMode.None : CursorLockMode.Locked;
        }

        #endregion

        #region Private data

        MovementController m_movementController;
        bool m_isCursorVisible;
        
        Vector3 m_eulerAngles;
        Vector2 m_inputLookDir;

        #endregion

    }
}