﻿
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

using MptUnity.Audio.Behaviour;
using MptUnity.Audio;

using Utility;
using Music;
using IO.State;
using Interaction.Behaviour;

namespace IO.Behaviour
{
    /// <summary>
    /// Event which gets called whenever the FlutePlayer starts playing a MusicalNote.
    /// First argument is index of the tone in the FlutePlayer's range of notes. 
    /// </summary>
    public class OnFluteNoteStartEvent : UnityEvent<ENoteColour, MusicalNote> { }
    /// <summary>
    /// Event which gets called whenever the FlutePlayer stops playing a MusicalNote.
    /// First argument is index of the tone in the FlutePlayer's range of notes.
    /// </summary>
    public class OnFluteNoteStopEvent : UnityEvent<ENoteColour , MusicalNote> { }
    
    /// <summary>
    ///Can be used to animate objects based on the Flute.
    /// </summary>
    public class OnFluteStateEnterEvent : UnityEvent<AbstractFluteState> { }
    /// <summary>
    ///Can be used to animate objects based on the Flute.
    /// </summary>
    public class OnFluteStateExitEvent : UnityEvent<AbstractFluteState> { }

    /// <summary>
    /// A class for the Instrument playing component of the Player.
    /// Watch for notes playing with OnPlayerNoteStartEvent and OnPLayerNoteStopEvent.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class Flute : MonoBehaviour
    {
        #region Serialised data 

        [Range(0L, 1L)] 
        public double volume = 1L;

        #endregion

        #region Public
        
        public void SetAllInteractiveObjects()
        {
            var interactiveModules = FindObjectsOfType<MusicInteractiveModule>();
            
            foreach (var mod in interactiveModules)
            {
               mod.SetFlute(this); 
            }
        }

        public Player GetPlayer()
        {
            return m_player;
        }

        public void PlayNote(ENoteColour noteColour)
        {
            TriggerNoteStart(noteColour, new MusicalNote(NoteColours.GetTone(noteColour), volume));
        }

        public void StopNote(ENoteColour noteColour)
        {
            TriggerNoteStop(noteColour);
        }

        public void SetPosition(float distanceNote)
        {
            transform.localPosition = new Vector3(distanceNote, -0.4f, 0.1f);
        }

        #endregion

        #region Unity MonoBehaviour events

        void Awake()
        {
            m_player = GetComponentInParent<Player>();
            Assert.IsNotNull(m_player);
            
            m_instrumentSource = GetComponentInChildren<IInstrumentSource>();
            Assert.IsNotNull(m_instrumentSource);
            
            m_animator = GetComponent<Animator>();
            Assert.IsNotNull(m_animator);

        }

        void Start()
        {
            Set();
            SetAllInteractiveObjects();
            SetPosition(1.5f);
        }

        void OnEnable()
        {
            if (m_isSet)
            {
                m_instrumentSource.Play();
            }
        }

        void OnDisable()
        {
            if (m_isSet)
            {
                m_instrumentSource.Stop();
            }
        }

        #endregion

        #region StateMachine routine

        void SetupState()
        {
            var allStates = m_animator.GetBehaviours<AbstractFluteState>();
            foreach (var state in allStates)
            {
               state.SetOwner(this);
            }
        }

        public void SignalStateEnterEvent(AbstractFluteState state)
        {
            m_events.fluteStateEnterEvent.Invoke(state);
        }

        public void SignalStateExitEvent(AbstractFluteState state)
        {
           m_events.fluteStateExitEvent.Invoke(state); 
        }

        void TriggerNoteStart(ENoteColour colour, MusicalNote note)
        {
            // Pass info to the StateMachine through the Animator.
            m_animator.SetInteger(Animations.s_eNoteColourId, (int)colour);
            m_animator.SetInteger(Animations.s_noteToneId, note.tone);
            m_animator.SetFloat(Animations.s_noteVolumeId, (float)note.volume);
            m_animator.SetFloat(Animations.s_notePanningId, (float)note.panning);
            // Triggering the transition to the Playing state!
            m_animator.ResetTrigger(Animations.s_tNoteStoppedId);
            m_animator.SetTrigger(Animations.s_tNoteStartedId);
            
            m_pressedColours.Add(colour);
        }

        void TriggerNoteStop(ENoteColour colour)
        {
            m_animator.SetInteger(Animations.s_eNoteColourId, (int)colour);
            // Triggering the transition to the Stopped state!
            m_animator.ResetTrigger(Animations.s_tNoteStartedId);
            m_animator.SetTrigger(Animations.s_tNoteStoppedId);

            m_pressedColours.Remove(colour);
        }
        
        #endregion

        #region Private utility

        void Set()
        {
            if (!m_isSet)
            {
                SetupAudio();
                SetupState();
            }

            m_isSet = true;
        }

        
        void SetupAudio()
        {
            m_playingVoices = new int[NoteColours.GetNumber()];
            m_playingColours = new List<ENoteColour>();
            m_pressedColours = new List<ENoteColour>();
            for (int i = 0; i < m_playingVoices.Length; ++i)
            {
                m_playingVoices[i] = -1;
            }
            // Force a number of voices
            // Two might yield better results than only one (hopefully).
            if (m_instrumentSource.NumberVoices < 2)
            {
                m_instrumentSource.NumberVoices = 2; 
            }
        }

        int GetPlayingVoice(ENoteColour colour)
        {
            return m_playingVoices[(int) colour];
        }

        void SetPlayingVoice(ENoteColour colour, int voice)
        {
            m_playingVoices[(int) colour] = voice;
            m_playingColours.Add(colour);
        }

        void UnsetPlayingVoice(ENoteColour colour)
        {
            
            m_playingVoices[(int) colour] = -1;
            m_playingColours.Remove(colour);
        }


        #region Public but should only be used by the state machines

        /// <summary>
        /// 
        /// </summary>
        /// <returns>true on success, false on failure.</returns>
        public bool StopNoteInternal(ENoteColour colour)
        {
            int voice = GetPlayingVoice(colour);
            Assert.IsTrue(voice >= 0 && voice < m_instrumentSource.GetNumberVoices());
            MusicalNote note = m_instrumentSource.GetNote(voice);
            bool success = m_instrumentSource.StopNote(voice);
            if (success)
            {
                UnsetPlayingVoice(colour);
                m_events.fluteNoteStopEvent.Invoke(colour, note);
            }
            return success;
        }

        public bool PlayNoteInternal(ENoteColour colour, MusicalNote note)
        {
            int voice = m_instrumentSource.StartNote(note);
            bool success = voice != -1;
            if (success)
            {
                SetPlayingVoice(colour, voice);
                m_events.fluteNoteStartEvent.Invoke(colour, note);
            }
            return success;
        }

        public void StopAllPlayingNotPressedNotes()
        {
            for (int i = 0; i < m_playingColours.Count; ++i)
            {
                ENoteColour colour = m_playingColours[i];
                if (!m_pressedColours.Contains(colour) && StopNoteInternal(colour))
                {
                    --i;
                }
            }
        }

        public void StopAllPlayingNotes()
        {
            for (int i = 0; i < m_playingColours.Count; ++i)
            {
                ENoteColour colour = m_playingColours[i];
                if (!StopNoteInternal(colour))
                {
                    --i;
                }
            }
        }
        
        #endregion

        #endregion

        #region Events

        class FluteEvents
        {
            public readonly OnFluteNoteStartEvent fluteNoteStartEvent;
            public readonly OnFluteNoteStopEvent fluteNoteStopEvent;
            public readonly OnFluteStateEnterEvent fluteStateEnterEvent;
            public readonly OnFluteStateExitEvent fluteStateExitEvent;

            public FluteEvents()
            {
                fluteNoteStartEvent = new OnFluteNoteStartEvent();
                fluteNoteStopEvent  = new OnFluteNoteStopEvent();
                fluteStateEnterEvent = new OnFluteStateEnterEvent();
                fluteStateExitEvent = new OnFluteStateExitEvent();
            }
        }

        public void AddOnNoteStartListener(UnityAction<ENoteColour, MusicalNote> onNoteStart)
        {
            m_events.fluteNoteStartEvent.AddListener(onNoteStart);
        }

        public void RemoveOnNoteStartListener(UnityAction<ENoteColour, MusicalNote> onNoteStart)
        {
            m_events.fluteNoteStartEvent.RemoveListener(onNoteStart);
        }

        public void AddOnNoteStopListener(UnityAction<ENoteColour, MusicalNote> onNoteStop)
        {
            m_events.fluteNoteStopEvent.AddListener(onNoteStop);
        }

        public void RemoveOnNoteStopListener(UnityAction<ENoteColour, MusicalNote> onNoteStop)
        {
            m_events.fluteNoteStopEvent.RemoveListener(onNoteStop);
        }
        
        public void AddOnStateEnterListener(UnityAction<AbstractFluteState> onStateEnter)
        {
            m_events.fluteStateEnterEvent.AddListener(onStateEnter);
        }
        
        public void RemoveOnStateEnterListener(UnityAction<AbstractFluteState> onStateEnter)
        {
            m_events.fluteStateEnterEvent.RemoveListener(onStateEnter);
        }
        
        public void AddOnStateLeaveListener(UnityAction<AbstractFluteState> onStateLeave)
        {
            m_events.fluteStateExitEvent.AddListener(onStateLeave);
        }
        
        public void RemoveOnStateLeaveListener(UnityAction<AbstractFluteState> onStateLeave)
        {
            m_events.fluteStateExitEvent.RemoveListener(onStateLeave);
        }
        
        #endregion

        #region Private data

        Player m_player;
        
        IInstrumentSource m_instrumentSource;

        AbstractFluteState m_state;
        
        int[] m_playingVoices;
        List<ENoteColour> m_playingColours;
        List<ENoteColour> m_pressedColours;

        Animator m_animator;

        bool m_isSet;
        bool m_isAnimated;
        
        readonly FluteEvents m_events = new FluteEvents();
        

        #endregion
    }
}