﻿
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Audio;

using MptUnity.Utility;


using Utility;

namespace IO.Behaviour
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(AudioSource))]
    public class FootstepController : MonoBehaviour
    {
        #region Serialised data

        [Header("Animation")]
        [Range(0.5f, 2f)]
        [SerializeField] float animationSpeed = 1f;
        [Range(1f, 2f)]
        [SerializeField] float runningSpeedModifier = 1.3f;

        [Header("Audio")] 
        [SerializeField]
        [Range(0f, 1f)] 
        float footstepPanAmount = 0.5f;

        [System.Serializable]
        struct Ground
        {
            public EGround kind;
            public AudioClip[] clips;
        }
        [SerializeField] Ground[] grounds = new Ground[Grounds.GetNumberNonDefault()];

        #endregion

        #region Public

        public float GetSpeed()
        {
            return animationSpeed * m_speedModifier;
        }

        public bool IsRunning
        {
            get => m_isRunning;
            set
            {
                m_isRunning = value;
                SpeedModifer = m_isRunning ? runningSpeedModifier : 1f;
            }
        }
        
        public float SpeedModifer
        {
            get => m_speedModifier;
            set
            {
                m_speedModifier = value;
                UpdateSpeed();
            }
        }

        public float BaseSpeed
        {
            get => animationSpeed;
            set
            {
               animationSpeed = value;
               UpdateSpeed();
            }
        }

        public void PlayFootstep(int footIndex)
        {
            Assert.IsTrue(footIndex < m_footAudioSources.Length);
            // depends on the current ground!
            EGround groundKind = m_movement.GetLastGroundKind();
            var ground = grounds[(int) groundKind];
            
            int footstepIndex = Random.Range(0, ground.clips.Length);
            m_footAudioSources[footIndex].PlayOneShot(ground.clips[footstepIndex]);
            
        }
        
        #endregion
        
        #region Monobehaviour events

        void Awake()
        {
            m_animator = GetComponent<Animator>();
            Assert.IsNotNull(m_animator);
            
            m_player = GetComponentInParent<Player>();
            Assert.IsNotNull(m_player);
            
            m_movement = GetComponentInParent<MovementController>();
            Assert.IsNotNull(m_movement);
            
            Assert.IsTrue(grounds.Length == Grounds.GetNumberNonDefault());
            foreach (var ground in grounds)
            {
                Assert.IsTrue(ground.clips.Length >= 1);
                foreach (var clip in ground.clips)
                {
                    Assert.IsNotNull(clip);
                }
            }
            
            // Duplicate the audio source and set the pans.
            // In order to have two audio sources with the same overall settings.
            var leftFootAudioSource = GetComponent<AudioSource>();
            Assert.IsNotNull(leftFootAudioSource);
            var rightFootAudioSource = gameObject.AddComponent<AudioSource>();
            UnityAudioUtility.CopyAudioSourceSettings(leftFootAudioSource, rightFootAudioSource);

            leftFootAudioSource.panStereo = footstepPanAmount;
            rightFootAudioSource.panStereo = -footstepPanAmount;

            m_footAudioSources = new[] { leftFootAudioSource, rightFootAudioSource };
            
            //
            // It does not seem like I have access to System.IO.Path, whatever.
            string footstepPath = Audio.c_audioMixerDirectoryPath + Audio.c_mainAudioMixerName;
            m_footstepGroup = Resources.Load<UnityEngine.Audio.AudioMixerGroup>(footstepPath);
            Assert.IsNotNull(m_footstepGroup);

        }
        
        void Start()
        {
            m_movement.AddOnStepListener(OnStep);
            m_speedModifier = 1f;
            
            UpdateSpeed();
        }
        
        void OnValidate()
        {
            if (m_animator != null)
            {
                UpdateSpeed();
            }
        }

        #endregion

        #region Private utility
    
        void OnStep(Vector3 step)
        {
            m_animator.SetTrigger(Animations.s_tStepId);     
        }
        
        void UpdateSpeed()
        {
            SetSpeed(GetSpeed());
        }
        
        void SetSpeed(float speed)
        {
            m_animator.speed = speed;
        
            UnityAudioUtility.StretchAudioGroup(
                m_footstepGroup, 
                m_footAudioSources, 
                speed,
                Audio.c_footstepPitchBendPropertyName
            );
        }
    
        #endregion

        #region Private data
    
        Player m_player;
        MovementController m_movement;
        Animator m_animator;
        AudioSource[] m_footAudioSources;
        AudioMixerGroup m_footstepGroup;
        
        float m_speedModifier;
        
        bool m_isRunning;
        
        #endregion
    }
}
