
using UnityEngine;

using Utility;

namespace IO.Behaviour
{
    public class GroundCheck : MonoBehaviour
    {
        #region Serialised data

        [SerializeField] float groundDistance = 0.05f;
        [SerializeField] float groundCheckRadius = 0.1f;

        /// <summary>
        /// Should ground checking be limited to ground layers?
        /// Should only be checked if certain that ground is labelled everywhere,
        /// So we won't.
        /// </summary>
        [SerializeField] bool checkGroundOnly = false;
        // instead, we use this
        [SerializeField] LayerMask checkMask;

        #endregion

        #region Public

        /// <summary>
        /// Is the player currently touching the ground? Might be 
        /// </summary>
        /// <returns></returns>
        public bool IsGrounded()
        {
            if (m_currentPhysicsStep != m_lastCheckedPhysicStep)
            {
                // Physics.OverlapSphereNonAlloc could also be used.
                // see: https://docs.unity3d.com/ScriptReference/Physics.OverlapSphereNonAlloc.html
                // But we use SphereCast, it's better since we know the direction of the ground.

                m_lastWasGrounded = Physics.SphereCast(
                    transform.position,
                    groundCheckRadius,
                    Physics.gravity,
                    out var hit,
                    groundDistance,
                    checkGroundOnly ? Grounds.GetGroundLayerMask() : checkMask
                );
                if (m_lastWasGrounded)
                {
                    m_lastGroundKind = Grounds.GetGroundKind(hit.collider.gameObject.layer);
                }
                m_lastCheckedPhysicStep = m_currentPhysicsStep;
            }
            return m_lastWasGrounded;
        }

        /// <summary>
        /// Get the kind of the ground the Player was last grounded on.
        /// </summary>
        /// <returns></returns>
        public EGround GetLastGroundKind()
        {
            return m_lastGroundKind;
        }

        #endregion

        #region MonoBehaviour events

        void FixedUpdate()
        {
            ++m_currentPhysicsStep;
        }

        void OnDrawGizmos()
        {
            var pos = transform.position;
            var offset = Physics.gravity.normalized * groundDistance;
            Gizmos.color = IsGrounded() ? Color.blue : Color.red;
            Gizmos.DrawWireSphere(pos, groundCheckRadius);
            Gizmos.DrawWireSphere(pos + offset, groundCheckRadius);
        }
        
        #endregion

        #region Private data

        int m_currentPhysicsStep;
        int m_lastCheckedPhysicStep;
        bool m_lastWasGrounded;
        
        EGround m_lastGroundKind;

        #endregion

    }
}