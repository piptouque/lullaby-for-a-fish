﻿
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Assertions;

using Utility;

namespace IO.Behaviour
{
    
    public class OnStepEvent : UnityEvent<Vector3> { }
    
    [RequireComponent(typeof(CharacterController))]
    public class MovementController : MonoBehaviour
    {
        #region Serialised data

        [Header("Speed")]
        [Range(0.5f, 3f)] public float runningSpeedModifier = 1.8f;
        [Range(0.5f, 4f)] public float maxSpeed = 0.8f;
        // Speed according to movement direction.
        [Range(0.1f, 1f)]
        public float forwardSpeed      = 1.0f;
        [Range(0.1f, 1f)]
        public float backwardSpeed     = 0.5f;
        [Range(0.1f, 1f)]
        public float lateralSpeed      = 0.3f;
        
        [Header("Step")]
        [Range(1f, 50f)]
        public float smoothness  = 6f;
        [Range(0.1f, 5f)]
        public float stepSize   = 1f;

        #endregion

        #region Public utility

        public bool IsGrounded()
        {
            return m_groundCheck.IsGrounded();
        }

        public EGround GetLastGroundKind()
        {
            return m_groundCheck.GetLastGroundKind();
        }
        
        public bool IsRunning { 
            get; 
            set; 
        }

        public void SetInputMoveDir(Vector2 inputMoveDir)
        {
            // Player input
            // int lateralDir = (Input.GetKey(input) ? 1 : 0) - (Input.GetKey(left) ? 1 : 0);
            // int longitudinalDir = (Input.GetKey(forward) ? 1 : 0) - (Input.GetKey(backward) ? 1 : 0);

            // m_dir = new Vector2Int(lateralDir, longitudinalDir);
            m_inputMoveDir = smoothness * new Vector2(inputMoveDir.x, inputMoveDir.y);
        }

        public void SetPosition(Vector3 position)
        {
            // see: https://forum.unity.com/threads/does-transform-position-work-on-a-charactercontroller.36149/#post-4132021
            bool wasEnabled = m_playerController.enabled;
            m_playerController.enabled = false;
            transform.position = position;
            m_playerController.enabled = wasEnabled;
        }

        public void SetRotation(Quaternion rot)
        {
            // see: https://forum.unity.com/threads/does-transform-position-work-on-a-charactercontroller.36149/#post-4132021
            bool wasEnabled = m_playerController.enabled;
            m_playerController.enabled = false;
            transform.rotation = rot;
            m_playerController.enabled = wasEnabled;
        }
        
        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            m_cameraController = GetComponentInChildren<FirstPersonCameraController>();
            Assert.IsNotNull(m_cameraController);
            
            m_playerController = GetComponent<CharacterController>();
            Assert.IsNotNull(m_playerController);

            m_groundCheck = GetComponentInChildren<GroundCheck>();
            Assert.IsNotNull(m_groundCheck);
        }

        void FixedUpdate()
        {
            // part of the velocity that is taken from gravity
            Vector3 gravityDir = Physics.gravity.normalized;
            Vector3 previousFallVelocity = gravityDir * Vector3.Dot(m_velocity, gravityDir);
            Vector3 previousMovingVelocity = m_velocity - previousFallVelocity;
            
            // applying 'drag', only to the moving part.
            float movingDrag = previousMovingVelocity.magnitude / maxSpeed;
            m_velocity = previousMovingVelocity * Mathf.Pow(1 - movingDrag * Time.fixedDeltaTime, 2);

            bool isGrounded = IsGrounded();
            // Gravity
            // do not add back fall velocity if it is already grounded.
            if (!isGrounded)
            {
                // acceleration times time is a velocity.
                m_velocity += previousFallVelocity + Physics.gravity * Time.fixedDeltaTime;
            }
            
            // IMPORTANT:
            // WE SHOULD NOT STOP THE PLAYER FROM MOVING WHEN THEY ARE FALLING,
            // THEY WILL GET STUCK
            // THE BEST WE CAN DO IS NOT TRIGGER THE 'STEP' EVENT WHEN THEY DO.
            
            // Velocity from input
            m_inputMoveAcc += m_inputMoveDir * Time.fixedDeltaTime;

            bool isStepTaken = m_inputMoveAcc.sqrMagnitude >= stepSize * stepSize;
            if (isStepTaken)
            {
                var move = GetDirectionalMovement(m_inputMoveAcc);
                //
                if (IsRunning)
                {
                    move *= runningSpeedModifier;
                }
                
                // instant acceleration!
                m_velocity += move;

                if (isGrounded)
                {
                    // todo: not sure invoking an event from the physics thread is a good idea..
                    m_events.onStepEvent.Invoke(m_inputMoveAcc);
                }
                // remember to reset the accumulator.
                m_inputMoveAcc = Vector3.zero;
            }
            
            // 
            //
            m_playerController.Move(m_velocity * Time.fixedDeltaTime);
        }
        
        #endregion

        #region Private utility

        /// <summary>
        /// Get movement factor depending on direction.
        /// </summary>
        /// <returns></returns>
        Vector3 GetDirectionalMovement(Vector2 inputMoveAcc)
        {
            var move = new Vector3(inputMoveAcc.x, 0f, inputMoveAcc.y); 
            float longitudinalSpeed = move.z > 0 ? forwardSpeed : backwardSpeed;
            move.Scale(new Vector3(lateralSpeed, 1f, longitudinalSpeed));
            
            // rotate towards look direction
            move = m_cameraController.transform.TransformDirection(move);
            // project on (x, z) plane.
            // kinda bad? I don't know.
            move.y = 0f;
            return move;
        }

        #endregion

        #region Events


        class MovementEvents
        {
            public readonly OnStepEvent onStepEvent;

            public MovementEvents()
            {
                onStepEvent = new OnStepEvent();
            }
        }

        public void AddOnStepListener(UnityAction<Vector3> onStep)
        {
            m_events.onStepEvent.AddListener(onStep);
        }
        
        public void RemoveOnStepListener(UnityAction<Vector3> onStep)
        {
            m_events.onStepEvent.RemoveListener(onStep);
        }


        #endregion

        #region Private data

        FirstPersonCameraController m_cameraController;
        CharacterController m_playerController;
        GroundCheck m_groundCheck;

        Vector3 m_velocity;
        
        Vector2 m_inputMoveDir;
        Vector2 m_inputMoveAcc;

        MovementEvents m_events = new MovementEvents();

        #endregion
    }

}
