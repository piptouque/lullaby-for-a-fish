
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

using Music;
using GameScene.Behaviour;

namespace IO.Behaviour
{
    [RequireComponent(typeof(MovementController))]
    [RequireComponent(typeof(PlayerInput))]
    public class Player : MonoBehaviour
    {
        #region Serialise data

        [Header("Component settings")]
        [SerializeField] bool isVisible = true;
        [SerializeField] bool enableMovement = true;
        [SerializeField] bool enableView = true;
        [SerializeField] bool enableAudio = true;

        #endregion
        
        #region Public utility

        public Flute GetFlute()
        {
            return m_flute;
        }

        public Camera GetCamera()
        {
            return m_camera;
        }

        public AudioListener GetListener()
        {
            return m_audioListener;
        }

        public void SwitchActionMap(string mapName)
        {
            m_playerInput.SwitchCurrentActionMap(mapName);
        }

        public InputActionMap GetCurrentActionMap()
        {
            return m_playerInput.currentActionMap;
        }

        public InputAction GetAction(string actionName, string actionMapName = null)
        {
            string str = actionMapName == null ? actionName : actionMapName + '/' + actionName;
            InputAction action =  m_playerInput.actions.FindAction(str);
            Debug.Assert(action != null, 
                $"Action {str} was not found in the InputActionAsset {m_playerInput.name}.");
            Assert.IsNotNull(action);
            return action;
        }

        public bool IsVisible
        {
            get => m_isVisible;
            set
            {
                if (m_isVisible == value)
                {
                    return;
                }
                m_isVisible = value;
                foreach (var aRenderer in m_allRenderers)
                {
                    aRenderer.enabled = m_isVisible;
                }

            }
        }
        public bool IsMovementEnabled
        {
            get => m_isMovementEnabled;
            set
            {
                if (m_isMovementEnabled == value)
                {
                    return;
                }
                m_isMovementEnabled = value;
                m_cameraController.enabled = m_isMovementEnabled;
                m_movementController.enabled = m_isMovementEnabled;
            }
        }

        public bool IsViewEnabled
        {
            get => m_isViewEnabled;
            set
            {
                if (m_isViewEnabled == value)
                {
                    return;
                }
                m_isViewEnabled = value;
                m_camera.enabled = m_isViewEnabled;
            }
        }

        public bool IsAudioEnabled
        {
            get => m_isAudioEnabled;
            set
            {
                if (m_isAudioEnabled == value)
                {
                    return;
                }
                m_isAudioEnabled = value;
                m_audioListener.enabled = m_isAudioEnabled;
            }
        }

        /// <summary>
        /// Set world coordinates position of the player.
        /// Use this instead of manually changing the transform.
        /// </summary>
        /// <param name="position"></param>
        public void SetPosition(Vector3 position)
        {
            m_movementController.SetPosition(position);
        }
        
        public void SetRotation(Quaternion rot)
        {
            m_movementController.SetRotation(rot);
        }
        
        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public Collider GetCollider()
        {
            return m_collider;
        }

        #region Input system Unity event callbacks.
        
        public void OnMove(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                SetInputMoveDir(context.ReadValue<Vector2>());
            }
            else
            {
                SetInputMoveDir(new Vector2());
            }
        }

        public void OnRun(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                // todo: for some reason the resulting animation speed is way higher than expected.
                m_footstepController.IsRunning = true;
                m_movementController.IsRunning = true;
            }
            else if (context.canceled)
            {
                m_footstepController.IsRunning = false;
                m_movementController.IsRunning = false;
            }
        }

        public void OnLook(InputAction.CallbackContext context)
        {
            SetInputLookDir(context.ReadValue<Vector2>());
        }

        public void OnNote(InputAction.CallbackContext context)
        {
            if (context.started || context.canceled)
            {
                ENoteColour noteColour = NoteColours.FromName(context.action.name);
                if (context.started)
                {
                    m_flute.PlayNote(noteColour);
                }
                else
                {
                    m_flute.StopNote(noteColour);
                }
            }
        }
        
        #endregion

        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            m_playerInput = GetComponent<PlayerInput>();
            Assert.IsNotNull(m_playerInput);
            m_camera = GetComponentInChildren<Camera>();
            Assert.IsNotNull(m_camera);
            m_audioListener = GetComponentInChildren<AudioListener>();
            Assert.IsNotNull(m_audioListener);
            m_collider = GetComponentInChildren<Collider>();
            Assert.IsNotNull(m_collider);
                
            m_movementController = GetComponent<MovementController>();
            Assert.IsNotNull(m_movementController);
            m_cameraController = GetComponentInChildren<FirstPersonCameraController>();
            Assert.IsNotNull(m_cameraController);
            m_footstepController = GetComponentInChildren<FootstepController>();
            Assert.IsNotNull(m_footstepController);
            m_flute = GetComponentInChildren<Flute>();
            Assert.IsNotNull(m_flute);

            m_allRenderers = GetComponentsInChildren<Renderer>();
        }

        void Start()
        {
            ResetMovementViewAudio();
        }

        void OnValidate()
        {
            //
            if (m_camera != null)
            {
                ResetMovementViewAudio();
                SetPosition(transform.position);
            }
        }

        #endregion

        #region Private utility

        void SetInputLookDir(Vector2 inputLookDir)
        {
            m_cameraController.SetInputLookDirection(inputLookDir);
        }

        void SetInputMoveDir(Vector2 inputMoveDir)
        {
            m_movementController.SetInputMoveDir(inputMoveDir);
        }

        void ResetMovementViewAudio()
        {
            // hack to force enabling / disabling.
            m_isMovementEnabled = !enableMovement;
            m_isViewEnabled = !enableView;
            m_isAudioEnabled = !enableAudio;
            m_isVisible = !isVisible;
            //
            IsViewEnabled = enableView;
            IsMovementEnabled = enableMovement;
            IsAudioEnabled = enableAudio;
            IsVisible = isVisible;
        }

        #endregion

        #region Private data
        
        Flute m_flute;

        PlayerInput m_playerInput; 
        Camera m_camera;
        AudioListener m_audioListener;
        Collider m_collider;
        
        FirstPersonCameraController m_cameraController;
        FootstepController m_footstepController;
        MovementController m_movementController;
        
        
        Renderer[] m_allRenderers;

        bool m_isVisible;
        bool m_isMovementEnabled;
        bool m_isViewEnabled;
        bool m_isAudioEnabled;

        #endregion
    }
}