﻿using UnityEngine;

using MptUnity.Audio;
using Music;
using Utility;

namespace IO.State
{
    public class FluteStatePlaying : AbstractFluteState
    {
        #region AbstractFlutePlayerState resolution

        protected override void OnStateEnterInternal(Animator animator)
        {
            // Resetting the corresponding trigger.
            animator.ResetTrigger(Animations.s_tNoteStartedId);
            
            owner.StopAllPlayingNotes();

            int colourIndex = animator.GetInteger(Animations.s_eNoteColourId);
            ENoteColour colour = (ENoteColour) colourIndex;
            
            m_note = new MusicalNote(
                animator.GetInteger(Animations.s_noteToneId),
                animator.GetFloat(Animations.s_noteVolumeId)
            );
            owner.PlayNoteInternal(colour, m_note);
            
            float positionNote;
            switch (colour)
            {
                case ENoteColour.eBlue: positionNote = 0.5f; break;
                case ENoteColour.eGreen: positionNote = 0.4f; break;
                case ENoteColour.eYellow: positionNote = 0.3f; break;
                case ENoteColour.eOrange: positionNote = 0.2f; break;
                case ENoteColour.eRed: positionNote = 0.1f; break;
                default: positionNote = 2.0f; break;
            }
            owner.SetPosition(positionNote);
            // bleu : 0.5f
            // vert : 0.4f
            // jaune : 0.3f
            // orange : 0.2f
            // rouge : 0.1f
        }
        
        #endregion

        #region Private data

        MusicalNote m_note;

        #endregion
    }
}
