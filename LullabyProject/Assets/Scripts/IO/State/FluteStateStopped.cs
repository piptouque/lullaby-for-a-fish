﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IO.State
{
    public class FluteStateStopped : AbstractFluteState
    {
        #region AbstractFlutePlayerState resolution

        protected override void OnStateEnterInternal(Animator animator)
        {
            owner.SetPosition(1.5f);
        }

        #endregion
    }
}

