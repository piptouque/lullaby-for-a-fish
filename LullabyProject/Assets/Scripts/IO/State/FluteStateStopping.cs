﻿using UnityEngine;

using Utility;

namespace IO.State
{

    public class FluteStateStopping : AbstractFluteState
    {

        #region AbstractFlutePlayerState resolution

        protected override void OnStateEnterInternal(Animator animator)
        {
            // Resetting the corresponding trigger.
            animator.ResetTrigger(Animations.s_tNoteStoppedId);

            // Debug.Log($"Stop {colour}");
            owner.StopAllPlayingNotPressedNotes();
        }

        #endregion
    }

}