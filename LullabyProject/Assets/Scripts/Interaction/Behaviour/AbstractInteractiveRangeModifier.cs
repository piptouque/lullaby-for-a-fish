using IO.Behaviour;

namespace Interaction.Behaviour
{
    /// <summary>
    /// Strategies for filtering interactive objects when playing according to range.
    /// Usage: Attach to an object using an InteractiveMusicModule.
    /// </summary>
    public abstract class AbstractInteractiveRangeModifier : UnityEngine.MonoBehaviour
    {
        #region To resolve

        public abstract bool IsInRange(Flute flute);

        #endregion
    }
}