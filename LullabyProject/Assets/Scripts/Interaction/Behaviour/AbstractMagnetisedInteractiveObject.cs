
using UnityEngine;
using UnityEngine.Assertions;

namespace Interaction.Behaviour
{
    /// <summary>
    /// An interactive object that gets attracted to a position relative to the player when activated.
    /// Uses physics to animate. 
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public abstract class AbstractMagnetisedInteractiveObject : AbstractSingleNoteInteractiveObject
    {
        #region Serialised data
        
        [Range(20f, 200f)] public float strength = 80f;

        #endregion
        
        #region Unity Monobehaviour events

        protected override void Awake()
        {
            base.Awake();
            
            m_rigidbody = GetComponent<Rigidbody>();
            Assert.IsNotNull(m_rigidbody);
        }
        #endregion

        #region To resolve

        /// <summary>
        /// Vector from the object to the magnet in World coordinates.
        /// </summary>
        /// <returns></returns>
        protected abstract Vector3 GetMagnetVector();

        #endregion
        
        #region AbstractSingleNoteInteractiveObject resolution

        protected override void OnActivate(MptUnity.Audio.MusicalNote note)
        {
            m_previousUseGravity = m_rigidbody.useGravity;
            
            m_rigidbody.useGravity = false;
        }

        protected override void OnDeactivate(MptUnity.Audio.MusicalNote note)
        {
            m_rigidbody.useGravity = m_previousUseGravity;
        }

        protected override void FixedUpdateActive()
        {
            ApplyMagnet(GetMagnetVector());
        }

        #endregion
        
        #region Private utility

        void ApplyMagnet(Vector3 magnetVec)
        {
            float r = magnetVec.magnitude;
            float amp = strength;
            // smoothing things over, so that the magnet's position is a resting position.
            amp *= r >= 1f
                ? 1f / (r * r)
                : r;
            m_rigidbody.AddForce(magnetVec.normalized * (Time.smoothDeltaTime * amp), ForceMode.Impulse);
        }

        #endregion
        
        #region Private data

        Rigidbody m_rigidbody;

        bool m_previousUseGravity;
        
        #endregion
    }
}