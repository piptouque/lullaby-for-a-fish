
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Assertions;

using Music;
using MptUnity.Audio;
using Utility;

namespace Interaction.Behaviour
{
    // The int component of the pair will only have meaning for leaves,
    // where it will give use the index of the melody of this path.
    using IMelodyTreeNode = ITreeNode<ENoteColour, List<int>>;
    using MelodyTreeNode = TreeNode<ENoteColour, List<int>>;
    
    
    /// <summary>
    /// </summary>
    public class OnMelodyTriggerEvent : UnityEvent<Melody> { }
    public class OnMelodyEnableEvent : UnityEvent<int, bool> { }

    
    public abstract class AbstractMelodyInteractiveObject : AbstractMusicInteractiveObject
    {
        #region Serialised data
        
        [Header("Melody")]
        [SerializeField] Melody[] melodies;
        [Range(2f, 10f)]
        [SerializeField] float idleTimeOut = 4f;

        #endregion
        
        #region Public utility

        public void EnableMelody(int melodyIndex, bool enable)
        {
            Assert.IsTrue(melodyIndex >= 0 && melodyIndex < melodies.Length);
            m_areMelodyEnabled[melodyIndex] = enable;

            if (m_isSet)
            {
                m_melodyEvents.melodyEnableEvent.Invoke(melodyIndex, enable);
            }
        }

        public int GetDepthMaxEnabled()
        {
            // Compute AltitudVe only from enabled melodies!
            return m_melodyTree.GetAltitude(node =>
                node.Weight.Any(melodyIndex => 
                    m_areMelodyEnabled[melodyIndex]));
        }


        #endregion

        #region Unity MonoBehaviour events

        protected override void Awake()
        {
            base.Awake();
            
            m_areMelodyEnabled = new bool[Levels.c_numberLevels];
            for (int i = 0; i < m_areMelodyEnabled.Length; ++i)
            {
                m_areMelodyEnabled[i] = true;
            }
            
            var melodyArrays = melodies
                .Select(melody => melody.noteColours.ToList())
                .ToList();

            m_melodyTree = MelodyTreeNode.FromArrays(
                melodyArrays,
                (previousWeight, depth, i) => 
                { 
                    previousWeight.Add(melodies[i].index);
                    return previousWeight; 
                });
            
            // current node is root.
            m_currentMelodyNode = m_melodyTree;
        }

        #endregion
        
        #region AbstractMusicInteractiveObjectResolution
        
        protected sealed override void OnNoteStart(ENoteColour aNoteColour, MusicalNote note)
        {
            // have to look for the child node whose value is aNoteColour
            var nextNode = m_currentMelodyNode.FindChild(node =>
            {
                bool hasSameColour = node.Value == aNoteColour;
                bool isPathActive = node.Weight.Any(melodyIndex => m_areMelodyEnabled[melodyIndex]);
                return hasSameColour && isPathActive;
            });
            if (nextNode == null)
            {
                // The note does not continue any melody, we stop the activation process.
                OnActivateStopInternal();
            }
            else
            {
                // cancelling activation time-out.
                CancelTimeout();
                
                m_currentMelodyNode = nextNode;
                
                // We will call OnActivatePartial at each new note start!
                // should cache depth of node, but whatever.
                OnActivatePartial(m_currentMelodyNode.GetDepth() - 1, aNoteColour, note);    
            }
        }

        protected sealed override void OnNoteStop(ENoteColour aNoteColour, MusicalNote note)
        {
            if (m_currentMelodyNode.IsLeaf())
            {
                // We'll call OnActivateTrigger when the last note of the melody stopped playing!
                OnActivateTriggerInternal(ComputeMelody(m_currentMelodyNode));
            }
            else
            {
                // Otherwise we set up a time-out for the activation of the object.
                ResumeTimeout();
            }
        }

        protected override void OnRangeEnter()
        {
            if (!m_isSet)
            {
                for (int i = 0; i < m_areMelodyEnabled.Length; ++i)
                {
                    m_melodyEvents.melodyEnableEvent.Invoke(i, m_areMelodyEnabled[i]);
                }
                m_isSet = true;
            }
        }

        protected override void OnRangeExit()
        {
            // Stopping the attempt to activate the object when the player gets out of range.
            ResumeTimeout();
        }

        #endregion

        #region To resolve

        protected virtual void OnActivateTrigger(Melody melody) { }
        protected virtual void OnActivatePartial(int noteIndex, ENoteColour noteColour, MusicalNote note) { }
        protected virtual void OnActivateStop() { }

        #endregion

        #region Private utility

        void OnActivateStopInternal()
        {
            OnActivateStop();
            ClearMelodyBuffer();
        }
        
        void OnActivateTriggerInternal(Melody melody)
        {
            OnActivateTrigger(melody);
            m_melodyEvents.melodyTriggerEvent.Invoke(melody);
            ClearMelodyBuffer();
        }

        void ClearMelodyBuffer()
        {
            m_currentMelodyNode = m_melodyTree;
        }

        IEnumerator OnActivateStopTimeOut()
        {
            yield return new WaitForSeconds(idleTimeOut);
            OnActivateStopInternal();
        }

        static Melody ComputeMelody(IMelodyTreeNode melodyLeaf)
        {
            Assert.IsTrue(melodyLeaf.IsLeaf());

            List<int> melodyIndices = melodyLeaf.Weight;
            Assert.IsTrue(melodyIndices.Count == 1);
            int melodyIndex = melodyIndices.First();
              
            ENoteColour[] melodyArray = melodyLeaf
                .GetPathFromRoot()
                .Where(node => !node.IsRoot())
                .Select(melodyNode => melodyNode.Value)
                .ToArray();

            return new Melody(melodyIndex, melodyArray);
        }

        void CancelTimeout()
        {
            if (m_timeOutCoroutine != null)
            {
                StopCoroutine(m_timeOutCoroutine);
                m_timeOutCoroutine = null;
            }
        }
        
        void ResumeTimeout()
        {
            if (isActiveAndEnabled && gameObject.activeSelf && m_timeOutCoroutine == null)
            {
                m_timeOutCoroutine = StartCoroutine(OnActivateStopTimeOut());
            }
        }

        #endregion

        #region Events
        
        class MelodyEvents
        {
            public readonly OnMelodyTriggerEvent melodyTriggerEvent = new OnMelodyTriggerEvent();
            public readonly OnMelodyEnableEvent melodyEnableEvent = new OnMelodyEnableEvent();
        }

        public void AddOnMelodyTriggerListener(UnityAction<Melody> onMelodyTrigger)
        {
            m_melodyEvents.melodyTriggerEvent.AddListener(onMelodyTrigger);
        }
        public void RemoveOnMelodyTriggerListener(UnityAction<Melody> onMelodyTrigger)
        {
            m_melodyEvents.melodyTriggerEvent.RemoveListener(onMelodyTrigger);
        }
        public void AddMelodyEnableListener(UnityAction<int, bool> onMelodyEnable)
        {
            m_melodyEvents.melodyEnableEvent.AddListener(onMelodyEnable);
        }
        public void RemoveMelodyEnableListener(UnityAction<int, bool> onMelodyEnable)
        {
            m_melodyEvents.melodyEnableEvent.RemoveListener(onMelodyEnable);
        }

        #endregion
        
        #region Private data

        IMelodyTreeNode m_melodyTree;
        IMelodyTreeNode m_currentMelodyNode;

        bool[] m_areMelodyEnabled;

        Coroutine m_timeOutCoroutine;

        bool m_isSet;

        readonly MelodyEvents m_melodyEvents = new MelodyEvents();

        #endregion
    }
}