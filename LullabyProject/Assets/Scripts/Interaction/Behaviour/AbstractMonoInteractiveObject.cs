
namespace Interaction.Behaviour
{
    public abstract class AbstractMonoInteractiveObject : AbstractMusicInteractiveObject
    {
        #region To resolve

        protected virtual void UpdateActive() { }
        protected virtual void FixedUpdateActive() { }

        #endregion

        #region MonoBehaviour events

        void Update()
        {
            if (m_isActive)
            {
                UpdateActive();
            }
        }

        void FixedUpdate()
        {
            if (m_isActive)
            {
                FixedUpdateActive();
            }
        }

        #endregion
        
        #region Protected utility

        protected void SignalActivate(bool activate)
        {
            m_isActive = activate;
        }

        #endregion
        
        #region Private data

        bool m_isActive;

        #endregion
    }
}