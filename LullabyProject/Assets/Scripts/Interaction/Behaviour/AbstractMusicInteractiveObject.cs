
using UnityEngine;
using UnityEngine.Assertions;

using IO.Behaviour;

using MptUnity.Audio;
using Music;

namespace Interaction.Behaviour
{
    /// <summary>
    /// An abstract class for objects which should react to the flute playing notes.
    /// </summary>
    [RequireComponent(typeof(MusicInteractiveModule))]
    public abstract class AbstractMusicInteractiveObject : MonoBehaviour
    {
        #region Public

        #endregion

        #region MonoBehaviour events

        protected virtual void Awake()
        {
            m_module = GetComponent<MusicInteractiveModule>();
            Assert.IsNotNull(m_module);
        }

        protected virtual void Start()
        {
           Subscribe();
           m_isStarted = true;
        }
            
        protected virtual void OnEnable()
        {
            if (m_isStarted)
            {
                Subscribe();
            }
        }
        
        /// <remarks>Using polymorphism with MonoBehaviour events requires that they be declared virtual,
        /// since they do not use inheritance.
        /// see: https://stackoverflow.com/a/53085621
        /// </remarks>
        protected virtual void OnDisable()
        {
            if (m_module.IsSubscribedInRange())
            {
                OnRangeExit();
                OnStopInteraction();
            }
            Unsubscribe();
        }

        #endregion
        
        #region To resolve
        
        /// <summary>
        /// Called when the FlutePlayer starts playing a note while in range,
        /// Or when it enters the range of the object while playing a note.
        /// </summary>
        /// <param name="aNoteColour"></param>
        /// <param name="note"></param>
        protected virtual void OnNoteStart(ENoteColour aNoteColour, MusicalNote note) { }
        /// <summary>
        /// Called when the FlutePlayer stops playing a note,
        /// Or when it exits the range of the object.
        /// </summary>
        /// <param name="noteColour"></param>
        /// <param name="note"></param>
        protected virtual void OnNoteStop(ENoteColour noteColour, MusicalNote note) { }

        /// <summary>
        /// Called when the FlutePlayer enters the range of the object.
        /// </summary>
        protected virtual void OnRangeEnter() { }
        /// <summary>
        /// Called when the FlutePlayer exits the range of the object.
        /// </summary>
        protected virtual void OnRangeExit() { }
        
        /// <summary>
        /// Called when the interaction is stopped (disabled, destroyed, etc.)
        /// Intended for memory freeing or process cleaning.
        /// </summary>
        protected virtual void OnStopInteraction() { }

        #endregion

        #region Private utility

        void Subscribe()
        {
            if (m_isSubscribed)
            {
                return;
            }
            m_module.AddOnNoteStartListener(OnNoteStart);
            m_module.AddOnNoteStopListener(OnNoteStop);
            m_module.AddOnRangeEnterListener(OnRangeEnterInternal);
            m_module.AddOnRangeExitListener(OnRangeExitInternal);
            m_module.AddOnStopInteractionListener(OnStopInteractionInternal);

            m_isSubscribed = true;
        }

        void Unsubscribe()
        {
            if (!m_isSubscribed)
            {
                return;
            }
            m_module.RemoveOnNoteStartListener(OnNoteStart);
            m_module.RemoveOnNoteStopListener(OnNoteStop);
            m_module.RemoveOnRangeEnterListener(OnRangeEnterInternal);
            m_module.RemoveOnRangeExitListener(OnRangeExitInternal);
            m_module.RemoveOnStopInteractionListener(OnStopInteractionInternal);

            m_isSubscribed = false;
        }

        // Using anonymous delegate seemed to work,
        // but I'm not taking any chances.
        // see: https://stackoverflow.com/a/41199095
        void OnRangeEnterInternal(int _) => OnRangeEnter();
        void OnRangeExitInternal(int _) => OnRangeExit();
        void OnStopInteractionInternal(int _) => OnStopInteraction();
        
        #endregion

        #region Protected data

        protected Flute GetFlute()
        {
            return m_module.GetFlute();
        }

        #endregion
        
        #region Private data

        MusicInteractiveModule m_module;
        bool m_isSubscribed;
        bool m_isStarted;

        #endregion
    }
}