
using UnityEngine.Events;

using MptUnity.Audio;
using Music;

namespace Interaction.Behaviour
{
    /// <summary>
    /// </summary>
    public class OnActivateEvent : UnityEvent<ENoteColour, MusicalNote> { }
    /// <summary>
    /// </summary>
    public class OnDeactivateEvent : UnityEvent<ENoteColour, MusicalNote> { }
    
    /// <summary>
    /// An interactive object which reacts to only one note.
    /// </summary>
    public abstract class AbstractSingleNoteInteractiveObject : AbstractMonoInteractiveObject
    {
        #region Serialised data

        public ENoteColour noteColour;

        #endregion

        #region To resolve

        protected virtual void OnActivate(MusicalNote note) { }
        protected virtual void OnDeactivate(MusicalNote note) { }

        #endregion
        
        #region AbstractMusicInteractiveObject resolution

        protected sealed override void OnNoteStart(ENoteColour aNoteColour, MusicalNote note)
        {
            if (aNoteColour == noteColour)
            {
                OnActivate(note);
                SignalActivate(true);
                singleNoteEvents.onActivateEvent.Invoke(aNoteColour, note);
            }
        }

        protected sealed override void OnNoteStop(ENoteColour aNoteColour, MusicalNote note)
        {
            if (aNoteColour == noteColour)
            {
                OnDeactivate(note);
                SignalActivate(false);
                singleNoteEvents.onDeactivateEvent.Invoke(aNoteColour, note);
            }
        }

        #endregion

        #region Events

        class SingleNoteEvents
        {
            public readonly OnActivateEvent onActivateEvent;
            public readonly OnDeactivateEvent onDeactivateEvent;

            public SingleNoteEvents()
            {
                onActivateEvent = new OnActivateEvent();
                onDeactivateEvent = new OnDeactivateEvent();
            }
        }
        
        public void AddOnActivateListener(UnityAction<ENoteColour, MusicalNote> onActivate)
        {
            singleNoteEvents.onActivateEvent.AddListener(onActivate);
        }

        public void RemoveOnActivateListener(UnityAction<ENoteColour, MusicalNote> onActivate)
        {
            singleNoteEvents.onActivateEvent.RemoveListener(onActivate);
        }
        
        public void AddOnDeactivateListener(UnityAction<ENoteColour, MusicalNote> onDeactivate)
        {
            singleNoteEvents.onDeactivateEvent.AddListener(onDeactivate);
        }

        public void RemoveOnDeactivateListener(UnityAction<ENoteColour, MusicalNote> onDeactivate)
        {
            singleNoteEvents.onDeactivateEvent.RemoveListener(onDeactivate);
        }

        #endregion

        #region Private

        SingleNoteEvents singleNoteEvents = new SingleNoteEvents();

        #endregion
    }
}