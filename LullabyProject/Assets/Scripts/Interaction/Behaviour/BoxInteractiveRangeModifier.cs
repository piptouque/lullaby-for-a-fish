
using UnityEngine;
using UnityEngine.Assertions;

using IO.Behaviour;

namespace Interaction.Behaviour
{
    /// <summary>
    /// Is in range when the player is inside the AABB of the collider.
    /// </summary>
    public class BoxInteractiveRangeModifier : AbstractInteractiveRangeModifier
    {

        #region Serialised data

        [SerializeField] BoxCollider boundsCollider;
        
        #endregion

        #region Public

        public void SetBox(Vector3 size, Quaternion rotation)
        {
            boundsCollider.size = size;
            boundsCollider.transform.rotation = rotation;
        }
        
        public Bounds GetBounds()
        {
            return boundsCollider.bounds;
        }
       
        #endregion
        
        #region AbstractInteractiveRangeModifier resolution

        public override bool IsInRange(Flute flute)
        {
            return GetBounds().Contains(flute.GetPlayer().GetPosition());
        }
        
        #endregion

        #region MonoBehaviourEvents

        void Awake()
        {
            if (boundsCollider == null)
            {
                boundsCollider = GetComponent<BoxCollider>();
            }
            Assert.IsNotNull(boundsCollider);
            boundsCollider.isTrigger = true;
        }

        #endregion

        #region Private utility

        #endregion
    }
}