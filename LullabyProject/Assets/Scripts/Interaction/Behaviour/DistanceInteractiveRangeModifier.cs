
using UnityEngine;

using IO.Behaviour;

namespace Interaction.Behaviour
{
    public class DistanceInteractiveRangeModifier : AbstractInteractiveRangeModifier
    {
        #region Serialised data

        [Range(0f, 30)]
        public float maxDistance = 8f;
        
        #endregion
        
        #region AbstractInteractiveRangeModifier resolution

        public override bool IsInRange(Flute flute)
        {
            float dist = Vector3.Distance(flute.transform.position, transform.position);
            bool isInRange = dist <= maxDistance;

            m_fluteCached = flute;

            return isInRange;
        }
        
        void OnDrawGizmosSelected()
        {
            if (m_fluteCached == null)
            {
                return;
            }
            
            Gizmos.color = IsInRange(m_fluteCached) ? Color.green : Color.red; 
            Gizmos.DrawWireSphere(transform.position, maxDistance);
        }

        #endregion

        #region Private data

        Flute m_fluteCached;

        #endregion
    }
}