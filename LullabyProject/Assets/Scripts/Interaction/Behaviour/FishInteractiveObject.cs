
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

using Music;
using MptUnity.Audio;
using MptUnity.Audio.Behaviour;

using UI.Behaviour;

namespace Interaction.Behaviour
{
    /// <summary>
    /// The fish behaviour combines instrument playing and melody with UI display (via MelodyCanvas)
    /// </summary>
    [RequireComponent(typeof(IInstrumentSource))]
    public class FishInteractiveObject : AbstractMelodyInteractiveObject
    {
        #region Serialised data
        
        [Header("Note repeat")] 
        [SerializeField]
        [Range(0f, 2f)]
        float repeatDelay = 0.5f;

        [Header("Sound effects")] [SerializeField]
        AudioClip[] triggerClips;

        #endregion


        #region MonoBehaviour events

        protected override void Awake()
        {
            base.Awake();

            m_instrumentSource = GetComponent<IInstrumentSource>();
            Assert.IsNotNull(m_instrumentSource);

            m_audioSource = GetComponent<AudioSource>();
            Assert.IsNotNull(m_audioSource);
            
            m_melodyCanvas = GetComponentInChildren<MelodyCanvas>();
            Assert.IsNotNull(m_melodyCanvas);

            Assert.IsTrue(triggerClips.Length > 0);
            Assert.IsTrue(triggerClips.All(clip => clip != null));

        }

        protected override void Start()
        {
            base.Start();
            
            m_instrumentSource.NumberVoices = 2;
            // m_instrumentSource.Stop();
        }
        
        protected override void OnDisable()
        {
            base.OnDisable();
            
            Stop();
        }
        
        #endregion

        #region AbstractMelodyInteractiveObject events


        protected override void OnRangeEnter()
        {
            base.OnRangeEnter();

            m_melodyCanvas.SetVisible(true);
            // m_instrumentSource.Play();
        }

        protected override void OnRangeExit()
        {
            base.OnRangeExit();

            m_melodyCanvas.SetVisible(false);
            
            // fix: use both isActiveAndEnabled and gameObject.activeSelf
            // isActiveAndEnabled is true when the component or go is destroyed, somehow?
            if (isActiveAndEnabled && gameObject.activeSelf)
            {
                StartCoroutine(StopAllNotesDelayed());
            }
            else
            {
                m_instrumentSource.StopAllNotes();
            }
        }

        protected override void OnActivateTrigger(Melody melody)
        {
            m_melodyCanvas.ResetMelody();
            m_melodyCanvas.SetVisible(false);

            // play a 'success' sound effect
            Stop();
            int i = Random.Range(0, triggerClips.Length);
            m_audioSource.PlayOneShot(triggerClips[i]);
            
            StartCoroutine(StopAllNotesDelayed());
        }

        protected override void OnActivatePartial(int noteIndex, ENoteColour noteColour, MusicalNote note)
        {
            m_melodyCanvas.SetMelodyNote(noteIndex, noteColour);

            StartCoroutine(RepeatNoteDelayed(note));
        }

        protected override void OnActivateStop()
        {
            StartCoroutine(StopAllNotesDelayed());
            
            m_melodyCanvas.ResetMelody();
        }

        #endregion


        #region Private utility

        void Stop()
        {
            StopAllCoroutines(); 
            m_instrumentSource.StopAllNotes();
        }

        IEnumerator RepeatNoteDelayed(MusicalNote note)
        {
            yield return new WaitForSeconds(repeatDelay);
            // the fish repeats the note!
            if (m_voice != -1)
            {
                m_instrumentSource.StopNote(m_voice);
            }
            m_voice = m_instrumentSource.StartNote(note);
        }
        
        IEnumerator StopAllNotesDelayed()
        {
            yield return new WaitForSeconds(repeatDelay);
            // the fish repeats the note!
            m_instrumentSource.StopAllNotes();
        }
        
        #endregion

        #region Private data

        MelodyCanvas m_melodyCanvas;
        IInstrumentSource m_instrumentSource;
        AudioSource m_audioSource;

        int m_voice = -1;
        
        #endregion
    }
}