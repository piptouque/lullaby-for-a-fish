
using UnityEngine;

namespace Interaction.Behaviour
{
    public class FluteAttractedInteractiveObject : AbstractMagnetisedInteractiveObject
    {
        #region Serialised data

        [SerializeField] Vector3 relativePos = new Vector3(0f, 0.8f, 2f);
        
        #endregion
        
        #region AbstractMagnetisedInteractiveObject resolution

        protected override Vector3 GetMagnetVector()
        {
            // Vector from magnet to object.
            Transform fluteTransform = GetFlute().transform;

            Vector3 localMagnetPos = fluteTransform.localPosition + relativePos;
            return fluteTransform.TransformPoint(localMagnetPos) - transform.position;
        }

        #endregion
    }
}