using UnityEngine;

using Music;

namespace Interaction.Behaviour
{
    /// <summary>
    /// An interactive object that lights up when activated.
    /// Uses physics to animate. 
    /// </summary>
    public class GlimmerInteractiveObject : AbstractSingleNoteInteractiveObject
    {
        #region Serialised data

        [Range(0.1f, 10.0f)]
        public float activeIntensity = 3.0f;

        #endregion
        
        #region Unity Monobehaviour events
        
        protected override void Awake()
        {
            base.Awake();
            
            m_lightObject = new GameObject();
            m_lightObject.transform.SetParent(transform, false);
            
            lightComponent = m_lightObject.AddComponent<Light>();
            lightComponent.color = NoteColours.GetColour(noteColour);

            lightComponent.intensity = 0.0f;
        }

        #endregion
        
        #region AbstractSingleNoteInteractiveObject

        protected override void OnActivate(MptUnity.Audio.MusicalNote note)
        {
            lightComponent.intensity = activeIntensity;
        }

        protected override void OnDeactivate(MptUnity.Audio.MusicalNote note)
        {
            lightComponent.intensity = 0.0f;
        }
        
        #endregion
        
        #region Protected data

        GameObject m_lightObject;
        protected Light lightComponent;

        #endregion
    }
}