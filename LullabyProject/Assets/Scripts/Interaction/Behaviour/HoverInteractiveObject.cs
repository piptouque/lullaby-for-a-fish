
using UnityEngine;
using UnityEngine.Assertions;

namespace Interaction.Behaviour
{
    /// <summary>
    /// An interactive object that hovers when activated.
    /// Uses physics to animate. 
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class HoverInteractiveObject : AbstractMagnetisedInteractiveObject
    {
        #region Serialised data
        
        [SerializeField] Vector3 hoverDir = Vector3.up;
        [Range(0.1f, 100f)]
        [SerializeField] float maxGroundDistance = 20f;

        [SerializeField] LayerMask raycastLayerMask;
        
        #endregion

        #region AbstractMagnetisedInteractiveObject

        protected override Vector3 GetMagnetVector()
        {
            Vector3 pos = m_collider.ClosestPoint(transform.position);
            
            m_groundFound = GetHighGroundPosition(pos, out m_ground);
            if (!m_groundFound)
            {
                return Vector3.zero;
            }
            // it's over, Anakin!

            float height = Vector3.Distance(pos, m_ground);

            return hoverDir.normalized * (hoverDir.magnitude - height);
        }

        #endregion

        #region Gizmo drawing

        void OnDrawGizmosSelected()
        {
            var pos = m_collider == null
                ? transform.position
                :  m_collider.ClosestPoint(transform.position);
            if (m_groundFound)
            {
                Gizmos.color =  Color.cyan; 
                Gizmos.DrawLine(pos, m_ground);
            }
            else
            {
                Gizmos.color =  Color.magenta; 
                Gizmos.DrawLine(pos, pos - hoverDir * maxGroundDistance);
            }
        }

        #endregion

        #region Private utility

        bool GetHighGroundPosition(Vector3 pos, out Vector3 groundPos)
        {
            RaycastHit hit;
            bool hasHit = Physics.Raycast(
                pos,
                -hoverDir,
                out hit,
                maxGroundDistance,
                raycastLayerMask.value
            );

            groundPos = hasHit ? hit.point : Vector3.zero;
            return hasHit;
        }

        #endregion

        #region MonoBehaviour events

        protected override void Awake()
        {
            base.Awake();

            m_collider = GetComponent<Collider>();
            Assert.IsNotNull(m_collider);
        }

        #endregion

        #region Private data
        
        Collider m_collider;

        Vector3 m_ground;
        bool m_groundFound;

        #endregion
    }
}