
using System;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Assertions;

using IO.Behaviour;

using MptUnity.Audio;
using Music;

namespace Interaction.Behaviour
{
    
    public class OnNoteStartEvent : UnityEvent<ENoteColour, MusicalNote> { }
    public class OnNoteStopEvent : UnityEvent<ENoteColour , MusicalNote> { }
    public class OnRangeEnterEvent : UnityEvent<int> { }
    public class OnRangeExitEvent : UnityEvent<int> { }
    public class OnStopInteraction : UnityEvent<int> { }
    
    
    public class MusicInteractiveModule : MonoBehaviour
    {
        #region Serialised data

        /// <summary>
        /// Time step at which the ranges are updated.
        /// </summary>
        [Range(0f, 1f)] [SerializeField] float updateStep = 0.3f;

        #endregion
        
        #region Public

        public void SetFlute(Flute aFlute)
        {
            if (m_isSet)
            {
                return;
            }
            Assert.IsNotNull(aFlute);
            m_flute = aFlute;
            
            m_isSubscribedInRange = false;
            m_isSubscribedOutOfRange = false;
            
            m_isSet = true;
        }

        public Flute GetFlute()
        {
            Assert.IsNotNull(m_flute);
            return m_flute;
        }

        public bool IsSubscribedInRange()
        {
            return m_isSubscribedInRange;
        }

        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            // Does not support adding modifiers at run-time.
            m_rangeModifiers = GetComponents<AbstractInteractiveRangeModifier>();
        }

        void Start()
        {
            FindFlute();
            
            m_outOfRangePlayingNotes = new List<Tuple<ENoteColour, MusicalNote>>();
            m_inRangePlayingNotes = new List<Tuple<ENoteColour, MusicalNote>>();
        }

        void Update()
        {
            // early exit if the flute has not been found.
            if (!m_isSet)
            {
                return;
            }

            m_updateStepAcc += Time.deltaTime;
            if (m_updateStepAcc >= updateStep)
            {
                m_updateStepAcc = 0f;
                UpdateRange();
            }
        }

        void OnDisable()
        {
            if (m_isSet)
            {
                StopInteraction();
            }
        }

        void OnEnable()
        {
            // will be called before Start. Will have to check that the object is set.
            if (m_isSet)
            {
                SubscribeOutOfRange();
            }
        }

        #endregion
        
        #region Private utility

        void EndSubscriptions()
        {
            UnsubscribeInRange();
            UnsubscribeOutOfRange();
        }
        void SubscribeInRange()
        {
            if (!m_isSubscribedInRange)
            {
                m_flute.AddOnNoteStartListener(OnNoteStartInRange);
                m_flute.AddOnNoteStopListener(OnNoteStopInRange);
            }
            m_isSubscribedInRange = true;
        }

        void UnsubscribeInRange()
        {
            if (m_isSubscribedInRange)
            {
                m_flute.RemoveOnNoteStartListener(OnNoteStartInRange);
                m_flute.RemoveOnNoteStopListener(OnNoteStopInRange);
            }
            m_isSubscribedInRange = false;
        }

        void SubscribeOutOfRange()
        {
            if (!m_isSubscribedOutOfRange)
            {
                m_flute.AddOnNoteStartListener(OnNoteStartOutOfRange);
                m_flute.AddOnNoteStopListener(OnNoteStopOutOfRange);
            }
            m_isSubscribedOutOfRange = true;
        }

        void UnsubscribeOutOfRange()
        {
            if (m_isSubscribedOutOfRange)
            {
                m_flute.RemoveOnNoteStartListener(OnNoteStartOutOfRange);
                m_flute.RemoveOnNoteStopListener(OnNoteStopOutOfRange);
            }
            m_isSubscribedOutOfRange = false;
        }
        
        void OnNoteStartInRange(ENoteColour noteColour, MusicalNote note)
        {
            m_inRangePlayingNotes.Add(new Tuple<ENoteColour, MusicalNote>(noteColour, note));
            m_events.onNoteStartEvent.Invoke(noteColour, note);
        }
        
        void OnNoteStopInRange(ENoteColour noteColour, MusicalNote note)
        {
            m_inRangePlayingNotes.RemoveAll(el => el.Item1 == noteColour);
            m_events.onNoteStopEvent.Invoke(noteColour, note);
        }

        void OnNoteStartOutOfRange(ENoteColour noteColour, MusicalNote note)
        {
            m_outOfRangePlayingNotes.Add(new Tuple<ENoteColour, MusicalNote>(noteColour, note));
        }
        
        void OnNoteStopOutOfRange(ENoteColour noteColour, MusicalNote note)
        {
            m_outOfRangePlayingNotes.RemoveAll(el => el.Item1 == noteColour);
        }
        
        bool IsInRange()
        {
            return m_rangeModifiers.All(modifier => modifier.IsInRange(m_flute));
        }

        bool FindFlute()
        {
            var aFlute = FindObjectOfType<Flute>();
            bool found = aFlute != null;
            if (found)
            {
                SetFlute(aFlute);
            }

            return found;
        }

        void UpdateRange()
        {
            bool isInRange = IsInRange();
            if (!m_isSubscribedOutOfRange && !isInRange)
            {
                if (m_isSubscribedInRange)
                {
                    m_events.onRangeExitEvent.Invoke(-1);
                }
                
                m_inRangePlayingNotes.ForEach(el => 
                    m_events.onNoteStopEvent.Invoke(el.Item1, el.Item2));
                m_outOfRangePlayingNotes.AddRange(m_inRangePlayingNotes);
                m_inRangePlayingNotes.Clear();
                
                UnsubscribeInRange();
                SubscribeOutOfRange();
            }
            else if (!m_isSubscribedInRange && isInRange)
            {
                
                if (m_isSubscribedOutOfRange)
                {
                    m_events.onRangeEnterEvent.Invoke(-1);
                }
                m_outOfRangePlayingNotes.ForEach(el => 
                    m_events.onNoteStartEvent.Invoke(el.Item1, el.Item2));
                m_inRangePlayingNotes.AddRange(m_outOfRangePlayingNotes);
                m_outOfRangePlayingNotes.Clear();

                UnsubscribeOutOfRange();
                SubscribeInRange();
            }
        }
        
        void StopInteraction()
        {
            if (m_isSet)
            {
                if (m_isSubscribedInRange)
                {
                    m_events.onRangeExitEvent.Invoke(-1);
                }
                m_events.onStopInteractionEvent.Invoke(-1);
                EndSubscriptions();
            }
        }

        #endregion

        #region Events

        class InteractiveEvents
        {
            public readonly OnNoteStartEvent onNoteStartEvent;
            public readonly OnNoteStopEvent onNoteStopEvent;
            public readonly OnRangeEnterEvent onRangeEnterEvent;
            public readonly OnRangeExitEvent onRangeExitEvent;
            public readonly OnStopInteraction onStopInteractionEvent;

            public InteractiveEvents()
            {
                onNoteStartEvent = new OnNoteStartEvent();
                onNoteStopEvent  = new OnNoteStopEvent();
                onRangeEnterEvent = new OnRangeEnterEvent();
                onRangeExitEvent = new OnRangeExitEvent();
                onStopInteractionEvent = new OnStopInteraction();
            }
        }
        
        public void AddOnNoteStartListener(UnityAction<ENoteColour, MusicalNote> onNoteStart)
        {
            m_events.onNoteStartEvent.AddListener(onNoteStart);
        }

        public void RemoveOnNoteStartListener(UnityAction<ENoteColour, MusicalNote> onNoteStart)
        {
            m_events.onNoteStartEvent.RemoveListener(onNoteStart);
        }
        
        public void AddOnNoteStopListener(UnityAction<ENoteColour, MusicalNote> onNoteStop)
        {
            m_events.onNoteStopEvent.AddListener(onNoteStop);
        }

        public void RemoveOnNoteStopListener(UnityAction<ENoteColour, MusicalNote> onNoteStop)
        {
            m_events.onNoteStopEvent.RemoveListener(onNoteStop);
        }
        
        public void AddOnRangeEnterListener(UnityAction<int> onRangeEnter)
        {
            m_events.onRangeEnterEvent.AddListener(onRangeEnter);
        }

        public void RemoveOnRangeEnterListener(UnityAction<int> onRangeEnter)
        {
            m_events.onRangeEnterEvent.RemoveListener(onRangeEnter);
        }
        
        public void AddOnRangeExitListener(UnityAction<int> onRangeExit)
        {
            m_events.onRangeExitEvent.AddListener(onRangeExit);
        }

        public void RemoveOnRangeExitListener(UnityAction<int> onRangeExit)
        {
            m_events.onRangeExitEvent.RemoveListener(onRangeExit);
        }
        
        public void AddOnStopInteractionListener(UnityAction<int> onStopInteraction)
        {
            // m_events.onStopInteractionEvent.AddListener(onStopInteraction);
            m_events.onStopInteractionEvent.RemoveListener(onStopInteraction);
        }

        public void RemoveOnStopInteractionListener(UnityAction<int> onStopInteraction)
        {
            m_events.onStopInteractionEvent.RemoveListener(onStopInteraction);
        }


        #endregion
    
        #region Private data
        
        Flute m_flute;
        
        List<Tuple<ENoteColour, MusicalNote>> m_outOfRangePlayingNotes;
        List<Tuple<ENoteColour, MusicalNote>> m_inRangePlayingNotes;

        AbstractInteractiveRangeModifier[] m_rangeModifiers;
        
        bool m_isSet;
        bool m_isSubscribedInRange;
        bool m_isSubscribedOutOfRange;

        readonly InteractiveEvents m_events = new InteractiveEvents();

        float m_updateStepAcc;

        #endregion
    }
}