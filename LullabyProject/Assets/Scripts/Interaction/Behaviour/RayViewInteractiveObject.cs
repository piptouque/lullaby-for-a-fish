
using UnityEngine;
using UnityEngine.Assertions;

namespace Interaction.Behaviour
{
    /// <summary>
    /// An interactive object that creates a Spot Light from the player to it when activated.
    /// Uses physics to animate.
    /// Currently unusable.
    /// </summary>
    public class RayViewInteractiveObject : GlimmerInteractiveObject
    {
        #region Serialised data

        public float shadowRadius = 1f;
        [Range(1f, 90f)]
        public float spotBreadth = 1f; // in degrees
        public float innerSpotAngle = 30f; // in degrees

        #endregion
        
        #region Unity Monobehaviour events

        protected override void Awake()
        {
            base.Awake();
            
            // For info on the Light component,
            // see: https://docs.unity3d.com/ScriptReference/Light.html
            
            // we want the spot light to do ...?
            // todo, maybe??
            
            // for some kind of ray shape.
            lightComponent.type = LightType.Spot;
            lightComponent.shape = LightShape.Box;
            lightComponent.innerSpotAngle = innerSpotAngle;

        }

        protected override void UpdateActive()
        {
            base.UpdateActive();
            
            var player = GetFlute().GetPlayer();
            Assert.IsNotNull(player);
            Camera cam = player.GetCamera();
            Assert.IsNotNull(cam);

            Vector3 vec = cam.transform.position - transform.position;
            // lightComponent.innerSpotAngle =
        }

        #endregion

        #region Private data

        #endregion
    }
}