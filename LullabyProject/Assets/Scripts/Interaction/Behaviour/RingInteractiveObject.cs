﻿
using UnityEngine;
using MptUnity.Audio;
using MptUnity.Audio.Behaviour;
using Music;
using UnityEngine.Assertions;

namespace Interaction.Behaviour
{
    [RequireComponent(typeof(IInstrumentSource))]
    public class RingInteractiveObject : AbstractMusicInteractiveObject
    {

        #region Serialised data

        public ENoteColour noteColour;
        [Range(1f,10f)]
        public float timeToLoop = 5f;
        [Range(0.1f,5f)]
        public float timeToRing = 1f;
        
        #endregion

        #region AbstractMusicInteractiveObject resolution

        protected override void OnRangeEnter()
        {
            InvokeRepeating(nameof(Ring), 0.0f, timeToLoop);
            InvokeRepeating(nameof(UnRing), timeToRing, timeToLoop);
        }

        protected override void OnRangeExit()
        {
            StopRinging();
        }

        protected override void OnStopInteraction()
        {
            StopRinging();
        }
        
        #endregion

        #region Private utility

        void Ring() 
        {
            /*
             * if (!enabled)
             * {
             *     StopRinging();
             * }
             */
            var note = new MusicalNote(NoteColours.GetTone(noteColour), 1f);
            m_voice = m_instrumentSource.StartNote(note);
            if (m_voice != 1)
            {
                m_isRinging = true;
            }
        }

        void UnRing()
        {
            if (m_voice != -1)
            {
                m_instrumentSource.StopNote(m_voice);
                m_voice = -1;
            }
            m_isRinging = false;
        }

        void StopRinging()
        {
            if (m_isRinging)
            {
                UnRing();
            }
            CancelInvoke();
        }
        
        #endregion
        
        #region MonoBehaviour events

        protected override void Awake() 
        {
            base.Awake();
            
            m_instrumentSource = GetComponent<IInstrumentSource>();
            Assert.IsNotNull(m_instrumentSource);        
        }
        
        #endregion

        #region Private data
        
        int m_voice = -1;
        bool m_isRinging;
        
        IInstrumentSource m_instrumentSource;
        
        #endregion
    }
}