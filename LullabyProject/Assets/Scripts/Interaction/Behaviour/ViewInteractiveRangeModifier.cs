
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

using IO.Behaviour;

namespace Interaction.Behaviour
{
    /// <summary>
    /// Get the 
    /// </summary>
    public class ViewInteractiveRangeModifier : AbstractInteractiveRangeModifier
    {
        #region Serialised data

        [Range(0.0f, 0.5f)] 
        public float maxOffset = 0.4f;
        [SerializeField] LayerMask raycastLayerMask;

        [SerializeField] Collider targetCollider;
        
        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            m_colliders = targetCollider == null ? GetComponentsInChildren<Collider>() : new Collider[] { targetCollider };
            Assert.IsTrue(m_colliders.All(el => el != null));
        }


        void OnDrawGizmos()
        {
            Vector3 target = m_hasHit ? m_hit.point : (transform.position - m_camPos) * Mathf.Infinity;
            Gizmos.color = m_isInRange ? Color.green : m_hasHit ? Color.yellow : Color.red;
            
            Gizmos.DrawLine(m_camPos, target);
        }

        #endregion

        #region AbstractInteractiveRangeModifier resolution

        public override bool IsInRange(Flute flute)
        {
            Camera cam = flute.GetPlayer().GetCamera();
            Transform camTrans = cam.transform;
            
            m_camPos = camTrans.position;
            Vector3 pos = transform.position;

            Vector3 vec = pos - m_camPos;
            
            // first check that it is in bounds
            // The offset is the projection of the vector on the camera surface
            // normalise it wrt. screen dimension
            Vector3 proj = cam.WorldToViewportPoint(pos);
            Vector2 screenVecProj = new Vector2(proj.x - 0.5f, proj.y - 0.5f);
            
            if (proj.z < 0 || screenVecProj.magnitude > maxOffset)
            {
                // Not a candidate. Return early, 
                m_isInRange = false;
                return m_isInRange;
            }

            m_hasHit = Physics.Raycast(
                m_camPos,
                vec.normalized,
                out m_hit,
                Mathf.Infinity,
                raycastLayerMask.value
            );
            
            // If the ray did not hit anything, or if the collider is this object's own.
            // This is done so that we don't require the interactive object to have a collider.
            m_isInRange = m_hasHit && m_colliders.Any(el => el == m_hit.collider);
            return m_isInRange;
        }

        #endregion

        #region Private data

        Collider[] m_colliders;

        Vector3 m_camPos;
        bool m_isInRange;
        RaycastHit m_hit;
        bool m_hasHit;

        #endregion

    }
}