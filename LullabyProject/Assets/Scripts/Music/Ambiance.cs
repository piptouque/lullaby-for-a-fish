
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Assertions;

namespace Music
{
    public enum EAmbiance
    {
        eFireplace = 0,
        eForest = 1,
        eInterior = 2,
        eNight = 3,
        eWaterfall = 4,
    }

    public static class Ambiances
    {
        public static int GetNumber()
        {
            return System.Enum.GetValues(typeof(EAmbiance)).Length; 
        }
    }
}