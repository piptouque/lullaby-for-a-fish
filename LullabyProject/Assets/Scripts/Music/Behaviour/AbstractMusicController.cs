
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

using FullSerializer;

using MptUnity.Audio.Behaviour;
using MptUnity.Utility;
using Common.Behaviour;

namespace Music.Behaviour
{
    using MusicValueDictionary = Dictionary<string, float[]>;
    public class OnMusicControllerEnableUpdate : UnityEvent<bool> { }
    
    public abstract class AbstractMusicController<E> : LullabyBehaviour
    where E : struct, System.Enum
    {
        
        #region Serialised data

        [Header("Music values")] [SerializeField]
        TextAsset musicValueFile;
        [Range(0f, 1f)]
        public float defaultMusicValue = 1f;

        [Header("Update")]
        [SerializeField] bool shouldUpdate = true;
        /// <summary>
        /// Mood is updated at MonoBehaviour update step,
        /// any time step below that will in effect be capped to that.
        /// </summary>
        [Range(0, 3)]
        public double updateTimeStep = 1.0;
        
        [Header("Apply")]
        [Range(0, 3)]
        public double applyTimeStep = .1;
        [SerializeField] [Range(0.01f, 1f)] float slidingSpeed = 0.2f;
        
        #endregion

        #region Public utility

        public bool IsUpdateEnabled
        {
            get => shouldUpdate;
            set
            {
                shouldUpdate = value;
                m_events.onEnableUpdateEvent.Invoke(shouldUpdate);
            }
        }

        public void AddModifier(AbstractMusicModifier<E> modifier)
        {
            var prev = m_modifiers.Find(el => el == modifier);
            if (prev == null)
            {
                m_modifiers.Add(modifier);
            }
        }

        public void RemoveModifier(AbstractMusicModifier<E> modifier)
        {
            m_modifiers.Remove(modifier);
        }

        public float GetValue(E e)
        {
            return m_currentMusicSliders[System.Convert.ToInt32(e)].GetValue();
        }

        public void SetValue(E e, float value)
        {
            m_currentMusicSliders[System.Convert.ToInt32(e)].SetTarget(value);
        }

        /// <summary>
        /// Set all slider target to fill value.
        /// </summary>
        public void FillValues(float value)
        {
            var values = new float[s_valueNumber];
            for (int i = 0; i < s_valueNumber; ++i)
            {
                values[i] = value;
            }
            SetValues(values);
        }
        

        public void SetValues(float[] values)
        {
            Assert.IsTrue(values.Length == s_valueNumber);
            for (int i = 0; i < s_valueNumber; ++i)
            {
               m_currentMusicSliders[i].SetTarget(values[i]); 
            }
        }

        public void FillRandom()
        {
            var values = new float[s_valueNumber];
            for (int i = 0; i < s_valueNumber; ++i)
            {
                values[i] = Random.value;
            }
            SetValues(values);
        }

        #endregion

        #region To resolve

        protected abstract IMusicSource GetMusicSource();
        
        #endregion
        
        #region MonoBehaviour events

        void Awake()
        {
            // be careful about what you put in Awake!
            // References are not initialised!
            m_modifiers = new List<AbstractMusicModifier<E>>();
            m_currentMusicSliders = new List<Slider>(s_valueNumber);
            for (int i = 0; i < s_valueNumber; ++i)
            {
                // linear interpolation is enough for now.
                m_currentMusicSliders.Add(new Slider(dt => dt * slidingSpeed));
            }
            
            m_musicSource = GetMusicSource();
            Assert.IsNotNull(m_musicSource);
            
            Assert.IsNotNull(musicValueFile);
            m_dict = ParseMusicValueDictionary(musicValueFile.text);
        }


        void Update()
        {
            // we update this here because it is not as important as apply the changes.
            // so that we do not clog the physics thread.
            m_updateTimeAcc += Time.deltaTime;

            if (!IsUpdateEnabled || m_updateTimeAcc < updateTimeStep)
            {
                return;
            }

            m_updateTimeAcc = 0.0;
            UpdateMusicValues();
        }
        
        void FixedUpdate()
        {
            // Update the sliders
            foreach (var slider in m_currentMusicSliders)
            {
               slider.UpdateDelta(Time.deltaTime); 
            }
            
            // Applying the values is less expensive, and also more important to keep calling regularly.
            m_applyTimeAcc += Time.fixedDeltaTime;

            if (m_applyTimeAcc < applyTimeStep)
            {
                return;
            }

            m_applyTimeAcc = 0.0;
            ApplyMusicValues();
        }

        void OnValidate()
        {
            IsUpdateEnabled = shouldUpdate;
        }

        #endregion

        #region Gizmo drawing

        void OnDrawGizmosSelected()
        {
            // We also draw all modifiers if the controller is selected.
            if (m_modifiers != null)
            {
                foreach (var moodModifier in m_modifiers)
                {
                    moodModifier.DrawGizmoInternal();
                }
            }
        }

        #endregion

        #region Private utility

        void UpdateMusicValues()
        {
            if (!m_modifiers.Any())
            {
                // do not update!
                foreach (var slider in m_currentMusicSliders)
                {
                    slider.SetTarget(defaultMusicValue);
                }
                return;
            }
            // IMPORTANT:
            // Here, baseMusicValues's order of music kind must match the order of the enum! todo
            float[] musicValues = new float[s_valueNumber];
            // we don't care about the scales for now,
            // we'll simply normalise the mood vector after this.
            foreach (var musicModifier in m_modifiers)
            {
                // get the weight of this modifier wrt. the player.
                float weightModifer = musicModifier.GetWeightModifier(Player);
                var musicBaseValues = musicModifier.GetBaseMusicValues();
                for (int j = 0; j < s_valueNumber; ++j)
                {
                    musicValues[j] += musicModifier.baseWeight * weightModifer * musicBaseValues[j];
                }
            }
            // normalise it so that the sum is equal to one.
            // does not guarantee equal volume or power,
            // but that's better than nothing.
            float sumBaseWeights = m_modifiers.Select(modifier => modifier.baseWeight).Sum();
            if (sumBaseWeights > 0f)
            {
                for (int j = 0; j < s_valueNumber; ++j)
                {
                    musicValues[j] /= sumBaseWeights;
                }
            }
            float sumValues = musicValues.Sum(); 
            if (sumValues > 0f)
            {
                for (int j = 0; j < s_valueNumber; ++j)
                {
                    musicValues[j] /= sumValues;
                }
            }
            for (int j = 0; j < s_valueNumber; ++j)
            {
                m_currentMusicSliders[j].SetTarget(musicValues[j]);
            }
            //
        }

        void ApplyMusicValues()
        {
            foreach (var pair in m_dict)
            {
                int sectionIndex = m_musicSource.GetSectionIndex(pair.Key);
                if (sectionIndex != -1)
                {
                    double volume = 0.0;
                    for (int j = 0; j < m_currentMusicSliders.Count; ++j)
                    {
                        volume += s_valueNumber * m_currentMusicSliders[j].GetValue() * pair.Value[j];
                    }
                    // no need to clamp, it's done by the music source.
                    m_musicSource.SetSectionVolume(sectionIndex, volume);
                }
            }
            // todo:
            // also multiply the *GAIN*
            // the idea is to get something that is of the same total volume
            // as 1, 1, ... number of instruments]
            // this won't work, for instance:
            // m_backgroundJukebox.volume = Moods.GetNumber();
        }

        static MusicValueDictionary ParseMusicValueDictionary(string musicValueString)
        {
            fsData data = fsJsonParser.Parse(musicValueString);
            object dict = null;
            fsResult result = s_serialiser.TryDeserialize(
                data,
                typeof(MusicValueDictionary),
                ref dict
            );
            return result.Failed ? null : (MusicValueDictionary) dict;
        }

        #endregion

        #region Events

        class ControllerEvents
        {
            public OnMusicControllerEnableUpdate onEnableUpdateEvent = new OnMusicControllerEnableUpdate();
        }

        public void AddOnEnableUpdateListener(UnityAction<bool> onEnableUpdate)
        {
            m_events.onEnableUpdateEvent.AddListener(onEnableUpdate);
        }
        
        public void RemoveOnEnableUpdateListener(UnityAction<bool> onEnableUpdate)
        {
            m_events.onEnableUpdateEvent.RemoveListener(onEnableUpdate);
        }

        #endregion

        #region Private data

        IMusicSource m_musicSource;

        List<AbstractMusicModifier<E>> m_modifiers;

        List<Slider> m_currentMusicSliders;
        
        double m_updateTimeAcc;
        double m_applyTimeAcc;

        MusicValueDictionary m_dict;

        readonly ControllerEvents m_events = new ControllerEvents();
        
        static readonly int s_valueNumber = System.Enum.GetValues(typeof(E)).Length;
        static readonly fsSerializer s_serialiser = new fsSerializer();

        #endregion
    }
}