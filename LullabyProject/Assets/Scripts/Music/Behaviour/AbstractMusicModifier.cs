
using System.Linq;
using UnityEngine;

using IO.Behaviour;
using Common.Behaviour;

namespace Music.Behaviour
{
    public abstract class AbstractMusicModifier<E> : LullabyBehaviour
        where E : System.Enum
    {

        #region Serialised data

        [Range(0f, 50f)]
        public float baseWeight = 1.0f;

        #endregion
        
        #region Public utility

        public void DrawGizmoInternal()
        {
            var values = GetBaseMusicValues();
            Color colour =  new Color(values[0], values[1], values[1], values.Average());// E.GetColour(GetBaseMusicValues());
            colour.a = baseWeight;
            Gizmos.color = colour;
            
            DrawGizmo();
        }
        
        #endregion
        
        #region To resolve

        /// <summary>
        /// Get a value between 0 and 1 depending on the player's state.
        /// which should be applied to the mood modifier's base values.
        /// </summary>
        /// <param name="aPlayer"></param>
        /// <returns></returns>
        public abstract float GetWeightModifier(Player aPlayer);

        public abstract float[] GetBaseMusicValues();

        protected abstract void Subscribe();
        protected abstract void UnSubscribe();

        protected virtual void DrawGizmo() { }
        
        #endregion

        #region MonoBehaviour events

        void Start()
        {
            SubscribeInternal();
            m_isStarted = true;
        }

        void OnEnable()
        {
            // add it to the appropriate music controller.
            if (m_isStarted)
            {
                SubscribeInternal();
            }
        }

        void OnDisable()
        {
            UnSubscribeInternal();
        }
        
        void OnDrawGizmosSelected()
        {
            DrawGizmoInternal();
        }


        #endregion

        #region Private  utility

        void SubscribeInternal()
        {
            if (!m_isSubscribed)
            {
                Subscribe();
            }

            m_isSubscribed = true;
        }

        void UnSubscribeInternal()
        {
            if (m_isSubscribed)
            {
                UnSubscribe();
            }

            m_isSubscribed = false;
        }

        #endregion

        #region Private data

        float m_cachedWeight;
        bool m_isStarted;
        bool m_isSubscribed;

        static readonly int s_valueNumber = System.Enum.GetValues(typeof(E)).Length;
        
        #endregion

    }
}