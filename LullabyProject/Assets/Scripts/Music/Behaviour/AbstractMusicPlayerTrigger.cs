
using UnityEngine;
using UnityEngine.Assertions;

using Common.Behaviour;

namespace Music.Behaviour
{
    [RequireComponent(typeof(Collider))]
    public class AbstractMusicPlayerTrigger : LullabyBehaviour
    {
        #region Serialised data 
        
        #endregion

        #region Monobehaviour events

        void Awake()
        {
            thisCollider = GetComponent<Collider>();
            Assert.IsNotNull(thisCollider);
            // activate trigger behaviour if it was not already set.
            thisCollider.isTrigger = true;
        }


        #endregion

        #region To resolve

        protected virtual void OnTriggerEnter(Collider other) { }
        protected virtual void OnTriggerExit(Collider other) { }

        #endregion

        #region Protected data
        
        protected Collider thisCollider;

        #endregion
    }
}