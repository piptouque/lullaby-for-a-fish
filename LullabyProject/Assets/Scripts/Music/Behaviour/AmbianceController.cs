
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

using MptUnity.Audio.Behaviour;

namespace Music.Behaviour
{
    public class AmbianceController : AbstractMusicController<EAmbiance>
    {
        #region AbstractMusicController resolution

        protected override IMusicSource GetMusicSource()
        {
            var sourceObject = GameObject.FindGameObjectWithTag(Utility.Tags.c_ambientNoiseTag);
            Assert.IsNotNull(sourceObject);
            return  sourceObject.GetComponent<IMusicSource>();
        }
        
        #endregion
    }
}