
using System.Linq;

using UnityEngine.Assertions;

namespace Music.Behaviour
{
    public class DistanceAmbianceModifier : DistanceMusicModifier<EAmbiance>
    {
        #region Serialised data

        public AmbianceValue[] baseMusicValues = new AmbianceValue[Ambiances.GetNumber()];

       #endregion
        
        public override float[] GetBaseMusicValues()
        {
            return baseMusicValues.Select(musicValue => musicValue.value).ToArray();
        }

        protected override void Subscribe()
        {
            AmbianceController.AddModifier(this);
        }

        protected override void UnSubscribe()
        {
            AmbianceController.RemoveModifier(this);
        }

        void Awake()
        {
            Assert.IsTrue(GetBaseMusicValues().Length == Ambiances.GetNumber());
        }
    }
}