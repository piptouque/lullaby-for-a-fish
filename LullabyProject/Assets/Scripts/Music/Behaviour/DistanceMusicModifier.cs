
using UnityEngine;
using UnityEngine.Assertions;

using IO.Behaviour;

namespace Music.Behaviour
{
    /// <summary>
    /// Mood modifier that uses the player's distance to its position.
    /// </summary>
    public abstract class DistanceMusicModifier<E> : AbstractMusicModifier<E>
        where E : System.Enum
    {
        #region Serialised data

        [Range(10f, 200f)]
        public float maxDistance = 50f;
        
        #endregion
        
        #region AbstractMoodModifier resolution

        public override float GetWeightModifier(Player aPlayer)
        {
            float dist = Vector3.Distance(transform.position, aPlayer.GetPosition());
            return 1f - Mathf.InverseLerp(0f, maxDistance, dist);
        }
        
        protected override void DrawGizmo()
        {
            Gizmos.DrawWireSphere(transform.position, maxDistance);
        }

        #endregion
    }
}