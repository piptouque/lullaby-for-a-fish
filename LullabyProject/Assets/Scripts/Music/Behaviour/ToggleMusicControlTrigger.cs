
using UnityEngine;
using UnityEngine.Assertions;

using MptUnity.Audio.Behaviour;
using Utility;
using IO.Behaviour;

namespace Music.Behaviour
{
    public class ToggleMusicControlTrigger : AbstractMusicPlayerTrigger
    {
        #region AbstractPlayerMusicTriggerCollider

        protected override void OnTriggerEnter(Collider other)
        {
            if (other == Player.GetCollider())
            {
                MoodController.IsUpdateEnabled = !AmbianceController.IsUpdateEnabled;
                AmbianceController.IsUpdateEnabled = !AmbianceController.IsUpdateEnabled;
            }
        }
        #endregion
    }
}