
using UnityEngine;

namespace Music.Behaviour
{
    public class ToggleMusicPauseTrigger : AbstractMusicPlayerTrigger
    {
        #region AbstractPlayerMusicTriggerCollider

        protected override void OnTriggerEnter(Collider other)
        {
            if (other == Player.GetCollider())
            {
                BackgroundMusic.TogglePlay();
                AmbientNoise.TogglePlay();
            }
        }
        #endregion
    }
}