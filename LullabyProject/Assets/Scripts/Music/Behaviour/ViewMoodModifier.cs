
using System.Linq;

using UnityEngine.Assertions;

namespace Music.Behaviour
{
    public class ViewMoodModifier : ViewMusicModifier<EMood>
    {
        #region Serialised data

        public MoodValue[] baseMusicValues = new MoodValue[Moods.GetNumber()];

        #endregion
        
        public override float[] GetBaseMusicValues()
        {
            return baseMusicValues.Select(musicValue => musicValue.value).ToArray();
        }
        
        protected override void Subscribe()
        {
            MoodController.AddModifier(this);
        }
        
        protected override void UnSubscribe()
        {
            MoodController.RemoveModifier(this);
        }

        protected override void Awake()
        {
            base.Awake();
            
            Assert.IsTrue(GetBaseMusicValues().Length == Moods.GetNumber());
        }
        
    }
}