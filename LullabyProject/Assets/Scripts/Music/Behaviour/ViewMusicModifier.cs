
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

using IO.Behaviour;

namespace Music.Behaviour
{
    /// <summary>
    /// Will be non-null when this object is in view of the player.
    /// Uses physics to cast a ray from the camera to the object.
    /// </summary>
    /// <typeparam name="E"></typeparam>
    public abstract class ViewMusicModifier<E> : AbstractMusicModifier<E>
        where E : System.Enum
    {
        #region Serialised data

        [SerializeField] LayerMask raycastLayerMask;
        
        #endregion

        #region MonoBehaviour events

        protected virtual void Awake()
        {
            m_colliders = GetComponentsInChildren<Collider>();
            Assert.IsNotNull(m_colliders);
            Assert.IsTrue(m_colliders.All(col => col != null));
        }

        #endregion

        #region Gizmo drawing

        protected override void DrawGizmo()
        {
            Vector3 target = m_hasHit ? m_hit.point : (transform.position - m_camPos) * Mathf.Infinity;
            
            Gizmos.DrawLine(m_camPos, target);

        }

        #endregion

        #region AbstractMoodRangeModifier resolution

        public override float GetWeightModifier(Player aPlayer)
        {
            Camera cam = aPlayer.GetCamera();
            Transform camTrans = cam.transform;
            
            m_camPos = camTrans.position;
            Vector3 pos = transform.position;

            Vector3 vec = pos - m_camPos;
            
            // We get the projection of the centre of the object on the screen.
            Vector3 viewPoint = cam.WorldToViewportPoint(pos);
            // this one is in [-1, 1] in a valid config.
            Vector2 screenPointNormalised = new Vector2(viewPoint.x - 0.5f, viewPoint.y - 0.5f) * 2f;
            // L_0 norm => square.
            float distFromCentre = Mathf.Max(Mathf.Abs(screenPointNormalised.x), Mathf.Abs(screenPointNormalised.y));
            bool isOnScreen = distFromCentre <= 1f;
            
            if (viewPoint.z < 0f || !isOnScreen)
            {
                // Wrong side of the screen. Return early.
                m_isInRange = false;
                return 0f;
            }

            // now we check that there are no objects in front of this one.
            m_hasHit = Physics.Raycast(
                m_camPos,
                vec.normalized,
                out m_hit,
                vec.magnitude,
                raycastLayerMask.value
            );
            
            // If the ray did not hit anything,
            // or if the collider is this object's own, 
            // then we are indeed in range.
            m_isInRange = !m_hasHit || m_colliders.Any(el => el == m_hit.collider);
            // the closer to the centre of the screen, the more weight!
            // Quadratic root in order to only activate when really looking at the object!
            float weight =  m_isInRange ? 1f - Mathf.Pow(distFromCentre, 0.25f) : 0f;
            return weight;
        }

        #endregion

        #region Private data

        Collider[] m_colliders;
        Vector3 m_camPos;
        
        bool m_isInRange;
        RaycastHit m_hit;
        bool m_hasHit;

        #endregion

    }
}