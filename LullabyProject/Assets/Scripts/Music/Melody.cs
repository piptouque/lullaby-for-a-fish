
namespace Music
{
    /// <summary>
    /// Intermediate class in order to serialise arrays of NodeColour elements.
    /// </summary>
    [System.Serializable]
    public struct Melody
    {
        public int index;
        public ENoteColour[] noteColours;

        public Melody(int aIndex, ENoteColour[] aNoteColours)
        {
            index = aIndex;
            noteColours = (ENoteColour[]) aNoteColours.Clone();
        }
    }
}