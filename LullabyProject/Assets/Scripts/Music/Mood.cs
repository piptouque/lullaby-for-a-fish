
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Assertions;
using FullSerializer;

namespace Music
{
    public enum EMood
    {
        eDeep = 0,
        eReflective = 1,
        eAwake = 2,
        eReminiscent = 3,
    }

    public static class Moods
    {
        public static int GetNumber()
        {
            return System.Enum.GetValues(typeof(EMood)).Length; 
        }

        public static Color GetColour(float[] moodValues)
        {
            Assert.IsTrue(moodValues.Length == GetNumber());
            // pretty bad.
            // not able to see the 'reminiscent' component.
            return new Color(moodValues[0], moodValues[1], moodValues[2], 1f);
        }

        public static Color GetColour(EMood mood)
        {
            var arr = new float[GetNumber()];
            arr[(int) mood] = 1.0f;
            return GetColour(arr);
        }
    }
}