
using UnityEngine;

namespace Music
{
    [System.Serializable]
    public struct MoodValue
    {
        public EMood kind;
        [Range(0, 1)] public float value;
    }
    
    [System.Serializable]
    public struct AmbianceValue
    {
        public EAmbiance kind;
        [Range(0, 1)] public float value;
    }
}