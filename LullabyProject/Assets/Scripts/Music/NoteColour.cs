
using UnityEngine;

namespace Music
{
    public enum ENoteColour
    {
        eBlue   = 0, // RGB -> bleue ??
        eGreen  = 1,
        eYellow = 2, // RGB ->
        eOrange = 3, // RGB -> Orange ??
        eRed    = 4
    }

    public static class NoteColours
    {
        public static int GetNumber()
        {
            return System.Enum.GetValues(typeof(ENoteColour)).Length; 
        }

        public static Color GetColour(ENoteColour noteColour)
        {
            Color colour = Color.clear;
            switch (noteColour)
            {
                case ENoteColour.eBlue:   colour = Color.blue;
                    break;
                case ENoteColour.eGreen:  colour = Color.green;
                    break;
                case ENoteColour.eYellow: colour = Color.yellow;
                    break;
                case ENoteColour.eOrange: colour = Color.Lerp(Color.red, Color.yellow, 0.5f);
                    break;
                case ENoteColour.eRed:    colour = Color.red;
                    break;
            }
            return colour;
        }

        public static int GetTone(ENoteColour noteColour)
        {
            int tone = -1;
            switch (noteColour)
            {
                case ENoteColour.eBlue: tone = 50; break;
                case ENoteColour.eGreen:
                    tone = 53;
                    break;
                case ENoteColour.eYellow:
                    tone = 57;
                    break;
                case ENoteColour.eOrange:
                    tone = 59;
                    break;
                case ENoteColour.eRed:
                    tone = 62;
                    break;
                default:
                    break;
            }
            return tone;
        }

        public static ENoteColour FromName(string name)
        {
            switch (name)
            {
            case "Note1": return ENoteColour.eBlue;
            case "Note2": return ENoteColour.eGreen;
            case "Note3": return ENoteColour.eYellow;
            case "Note4": return ENoteColour.eOrange;
            case "Note5": return ENoteColour.eRed;
            default: return ENoteColour.eBlue;
            }
        }
        
    }
}