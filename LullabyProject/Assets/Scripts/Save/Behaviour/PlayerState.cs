﻿
using System.Linq;
using UnityEngine.Assertions;

using Utility;
using Common.Behaviour;

namespace Save.Behaviour
{
    public class PlayerState : LullabyBehaviour
    {
        #region Public

        /// <summary>
        /// Is the save newly created?
        /// </summary>
        /// <returns></returns>
        public bool IsNew()
        {
            return m_data.isNew;
        }


        public void Save()
            // go back.
        {
            m_data.isNew = false;
            SaveSystem.SaveGame(m_data);
        }

        public int GetNumberMelodyPlayed()
        {
            return GetNumberMelodyPlayed(m_data);
        }

        /// <summary>
        /// Loads a save file at default location if found,
        /// Creates at new one otherwise.
        /// </summary>
        public void Load()
        {
            bool found = SaveSystem.LoadGame(out PlayerData aData);
            if (!found)
            {
                aData.isNew = true;
            }
            if (GetNumberMelodyPlayed(aData) >= Melodies.c_numberMelodies)
            {
                // new game+
                ResetData();
            }
            else
            {
                SetData(aData);
            }
        }

        public void ResetData()
        {
            // the player data is then set to new.
            SetData(new PlayerData());
            // save the fresh data.
            SaveSystem.SaveGame(m_data);
        }
        
        // update the melodyLevel
        public void SetMelodyPlayed(int melodyIndex)
        {
            Assert.IsTrue(melodyIndex >= 0 && melodyIndex < Levels.c_numberLevels);
            m_data.melodyLevel[melodyIndex] = true;
            // we can save the update in the save file at this step for example
        }

        public bool HasMelodyBeenPlayed(int melodyIndex)
        {
            Assert.IsTrue(melodyIndex >= 0 && melodyIndex < Levels.c_numberLevels);
            return m_data.melodyLevel[melodyIndex];
        }
        
        #endregion

        #region MonoBehaviour events

        void OnEnable()
        {
            LevelManager.AddOnLevelSwitchListener(OnLevelSwitch);
        }

        void OnDisable()
        {
            // As is also remarked elsewhere, LevelManager might be null by this point,
            // if we are not careful.
            LevelManager.RemoveOnLevelSwitchListener(OnLevelSwitch);
        }

        #endregion

        #region Private utility

        void OnLevelSwitch(int level)
        {
            // kind of a ping-pong between LevelManager and PlayerState,
            // since this method will be called
            // when the LevelManager is set the level == PlayerState.GetNumberMelodyPlayed()
            // But it kind of makes sense: when the latter is Levels.c_numberLevels,
            // it means that it's game over and the game must be reset.
            if (level == Levels.c_numberLevels)
            {
                ResetData();
            }
        }
        
        void SetData(PlayerData aData)
        {
            m_data = aData;
        }
        
        public static int GetNumberMelodyPlayed(PlayerData data)
        {
            int number = data.melodyLevel.Count(hasBeenPlayed => hasBeenPlayed);
            // We can have number == Levels.c_numberLevels, that means game over.
            Assert.IsTrue(number >= 0 && number <= Levels.c_numberLevels);
            return number;
        }
        
        #endregion
        
        #region Private


        PlayerData m_data;

        #endregion
    }
}
