﻿
namespace Save
{
    [System.Serializable]
    public class PlayerData
    {
        // The state of the solving solution in each zone
        public bool[] melodyLevel;
        // is this a new save -> not played yet?
        public bool isNew;

        // Constructor of PlayerData();
        // default constructor would be a 'new save' state.
        public PlayerData()
        {
            melodyLevel = new bool[Utility.Levels.c_numberLevels];
            isNew = true;
        }     
    }
}
