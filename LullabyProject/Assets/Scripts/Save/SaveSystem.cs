﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Save
{
    public static class SaveSystem
    {
        static readonly string c_defaultSavePath = Application.persistentDataPath + "/stateplayer.save";
        
        public static void SaveGame(PlayerData data, string path = null)
        {
            if (path == null)
            {
                path = c_defaultSavePath;
            }
            BinaryFormatter formatter = new BinaryFormatter();

            // create the file on the system
            FileStream stream = new FileStream(path, FileMode.Create);

            // insert the data into the file
            formatter.Serialize(stream, data);
            stream.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path"></param>
        /// <returns>Was a save file found?</returns>
        public static bool LoadGame(out PlayerData data, string path = null)
        {
            if (path == null)
            {
                path = c_defaultSavePath;
            }

            bool found = false;
            if(File.Exists(path))
            {
                found = true;
                
                BinaryFormatter formatter = new BinaryFormatter();

                FileStream stream = new FileStream(path, FileMode.Open);
                data = formatter.Deserialize(stream) as PlayerData;
                stream.Close();
            }
            else
            {
                // initialise new save.
                data = new PlayerData();
            }
            return found;
        }
    }
}
