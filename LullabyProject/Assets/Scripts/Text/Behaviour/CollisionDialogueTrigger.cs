﻿
using UnityEngine;
using UnityEngine.Assertions;

using Common.Behaviour;

namespace Text.Behaviour
{
    /// <summary>
    /// Instructions about specific object will be visible when the player is colliding with the colliders.
    /// </summary>
    [RequireComponent(typeof(Dialogue))]
    public class CollisionDialogueTrigger : LullabyBehaviour
    {
        #region InteractiveMusicObject resolution

        void OnTriggerEnter(Collider other)
        {
            if (other != Player.GetCollider())
            {
                return;
            }
            m_dialogue.SetSentenceIndex(0);
            m_dialogue.Pop(true);
        }

        void OnTriggerExit(Collider other)
        {
            if (other != Player.GetCollider())
            {
                return;
            }
            m_dialogue.Pop(false);
        }

        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            m_dialogue = GetComponent<Dialogue>();
            Assert.IsNotNull(m_dialogue);

            m_collider = GetComponent<Collider>();
            Assert.IsNotNull(m_collider);
            m_collider.isTrigger = true;
        }
        
        #endregion

        #region Private data
        
        Dialogue m_dialogue;
        Collider m_collider;

        #endregion
    }
}