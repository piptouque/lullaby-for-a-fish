﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
    
using TMPro;
using FullSerializer;

using Traduction;

namespace Text.Behaviour
{
    using DialogueDictionary = Dictionary<string, string>;
    /// <summary>
    /// Instructions that can be popped up on demand -> text box.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class Dialogue : PopUp
    {
        #region Serialised data

        [Header("Dialogue objects")]
        [SerializeField] GameObject titleObject;
        [SerializeField] GameObject sentenceObject;
        
        [SerializeField] TextAsset dialogueAsset;
        [SerializeField] DialogueData dialogueData;
        
        #endregion
        
        #region Public

        public DialogueDictionary dialogueDictionary;

        public int GetNumberSentences()
        {
            return dialogueData.sentenceKeys.Length;
        }
        
        public void SetSentenceIndex(int sentenceIndex)
        {
            Assert.IsTrue(sentenceIndex >= 0 && sentenceIndex < dialogueData.sentenceKeys.Length);
            bool found = dialogueDictionary.TryGetValue(dialogueData.nameKey, out string aName);
            found &= dialogueDictionary.TryGetValue(dialogueData.sentenceKeys[sentenceIndex], out string sentence);
            Assert.IsTrue(found);
            
            m_nameText.text = aName;
            m_sentenceText.text = sentence;
        }

        #endregion

        #region MonoBehaviour events

        protected override void Awake()
        {
            base.Awake();
            
            Assert.IsNotNull(dialogueAsset);
            dialogueDictionary = ParseDialogueDictionary(dialogueAsset.text);
            Assert.IsNotNull(dialogueDictionary);
            
            Assert.IsNotNull(dialogueData);
            Assert.IsTrue(dialogueData.sentenceKeys.All(s => s != null));
            
            Assert.IsNotNull(titleObject);
            Assert.IsNotNull(sentenceObject);

            m_nameText = titleObject.GetComponent<TextMeshProUGUI>();
            m_sentenceText = sentenceObject.GetComponent<TextMeshProUGUI>();
            Assert.IsNotNull(m_nameText);
            Assert.IsNotNull(m_sentenceText);
        }

        #endregion

        #region Private utility
        
        static DialogueDictionary ParseDialogueDictionary(string dialogueString)
        {
            fsData data = fsJsonParser.Parse(dialogueString);
            object dict = null;
            fsResult result = s_serialiser.TryDeserialize(
                data, 
                typeof(DialogueDictionary), 
                ref dict
            );
            return result.Failed ? null : (DialogueDictionary) dict;
        }
        
        void SetDialogueData(DialogueData aDialogueData)
        {
            Assert.IsNotNull(dialogueData);
            Assert.IsTrue(dialogueData.sentenceKeys.All(s => s != null));
            dialogueData = aDialogueData;
        }
        
        #endregion

        #region Private data
        
        TextMeshProUGUI m_nameText;
        TextMeshProUGUI m_sentenceText;
        
        
        static readonly fsSerializer s_serialiser = new fsSerializer();

        #endregion
    }
}