﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
    
using Utility;
using Common.Behaviour;

namespace Text.Behaviour
{
    public interface IPopUp 
    {
        #region Public

        void Pop(bool isShowing);

        bool IsPopped();

        #endregion
    }
}