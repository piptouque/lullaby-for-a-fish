﻿
using UnityEngine;
using UnityEngine.Assertions;

using Interaction.Behaviour;

namespace Text.Behaviour
{
    /// <summary>
    /// Instructions about specific object will be visible when the player is in range.
    /// </summary>
    [RequireComponent(typeof(Dialogue))]
    public class InteractiveDialogueTrigger : AbstractMusicInteractiveObject
    {
        
        #region InteractiveMusicObject resolution

        protected override void OnRangeEnter()
        {
            m_dialogue.SetSentenceIndex(0);
            m_dialogue.Pop(true);
        }

        protected override void OnRangeExit()
        {
            m_dialogue.Pop(false);
        }

        protected override void OnStopInteraction()
        {
            m_dialogue.Pop(false);
        }
        
        #endregion

        #region MonoBehaviour events

        protected override void Awake()
        {
            base.Awake();

            m_dialogue = GetComponent<Dialogue>();
            Assert.IsNotNull(m_dialogue);
        }
        
        #endregion

        #region Private data
        
        Dialogue m_dialogue;
        
        #endregion
    }
}