﻿
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Assertions;

using Utility;
using Environment.Behaviour;
using MptUnity.Audio;

namespace Text.Behaviour
{
    /// <summary>
    /// </summary>
    [RequireComponent(typeof(SkipDialogueTrigger))]
    public class LevelDialogueTrigger : LevelDependentAction
    {
        #region LevelDependentAction resolution

        protected override void OnLevelSwitch(int level, bool isActive)
        {
            if (isActive)
            {
                m_dialogueTrigger.BeginDialogue();
            }
        }
        #endregion

        #region MonoBehaviour events

        protected override void Awake()
        {
            base.Awake();
            
            m_dialogueTrigger = GetComponent<SkipDialogueTrigger>();
            Assert.IsNotNull(m_dialogueTrigger);
        }

        #endregion

        #region Private data

        SkipDialogueTrigger m_dialogueTrigger;

        #endregion

    }
}