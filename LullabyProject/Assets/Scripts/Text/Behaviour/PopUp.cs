﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
    
using Utility;
using Common.Behaviour;

namespace Text.Behaviour
{
    /// <summary>
    /// Instructions that can be popped up on demand -> text box.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class PopUp : LullabyBehaviour, IPopUp
    {
        #region Serialised data

        [Header("Animation")]
        [Range(0.5f, 4f)]
        [SerializeField] float animationSpeed = 1.0f;

        #endregion
        
        #region Public

        public void Pop(bool isShowing)
        {
            SetVisible(isShowing);
        }

        public bool IsPopped()
        {
            return m_isPopped;
        }
        
        #endregion

        #region MonoBehaviour events

        protected virtual void Awake()
        {
            m_signAnimator = GetComponent<Animator>();
            Assert.IsNotNull(m_signAnimator);
            
            m_signAnimator.speed = animationSpeed;
        }

        void OnValidate()
        {
            if (m_signAnimator != null)
            {
                m_signAnimator.speed = animationSpeed;
            }
        }

        #endregion

        #region Private utility
        
        void SetVisible(bool visible)
        {
            m_isPopped = visible;
            
            m_signAnimator.ResetTrigger(m_isPopped ? Animations.s_tSignOut : Animations.s_tSignIn);
            m_signAnimator.SetTrigger(m_isPopped ?   Animations.s_tSignIn  : Animations.s_tSignOut);
        }
        
        #endregion

        #region Private data
        
        Animator m_signAnimator;
        bool m_isPopped;

        #endregion
    }
}