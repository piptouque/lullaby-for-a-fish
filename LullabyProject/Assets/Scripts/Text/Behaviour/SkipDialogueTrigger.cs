﻿
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Assertions;

using Common.Behaviour;
using Utility;

namespace Text.Behaviour
{
    /// <summary>
    /// </summary>
    [RequireComponent(typeof(Dialogue))]
    public class SkipDialogueTrigger : LullabyBehaviour
    {

        #region Serialised data

        [SerializeField] GameObject skipTextObject;
        [SerializeField] string skipActionName = InputActions.c_validatePopUpActionName;
        [SerializeField] string skipTextKey;

        #endregion
        
        #region Public

        public void OnContinue(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                AdvanceDialogue();
            }
        }
        
        public void BeginDialogue()
        {
            SetSkipText();
            m_dialogue.SetSentenceIndex(m_currentSentenceIndex = 0); // reset sentence index and set it in pop-up
            m_dialogue.Pop(true);
            // switch to dialogue state.
            m_validateAction = Player.GetAction(skipActionName);
            m_validateAction.performed += OnContinue;
        }

        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            Assert.IsNotNull(skipTextObject);
            m_skipText = skipTextObject.GetComponent<TMPro.TextMeshProUGUI>();
            Assert.IsNotNull(m_skipText);
            
            m_dialogue = GetComponent<Dialogue>();
        }

        void OnDisable()
        {
            if (m_validateAction != null)
            {
                m_validateAction.performed -= OnContinue;
            }
        }
        
        #endregion

        #region Private utility


        void AdvanceDialogue()
        {
            // à faire: passer à la phrase suivante (réplique???) 
            // Si la dernière réplique est attente, on arrête le dialogue!
            if (m_currentSentenceIndex >= m_dialogue.GetNumberSentences() - 1)
            {
                EndDialogue();
            }
            else
            {
                m_dialogue.SetSentenceIndex(++m_currentSentenceIndex);
            }
        }

        void EndDialogue()
        {
            m_dialogue.Pop(false);
            m_validateAction.performed -= OnContinue;
        }

        void SetSkipText()
        {
            bool found = m_dialogue.dialogueDictionary.TryGetValue(skipTextKey, out string skipText);
            Assert.IsTrue(found);
            m_skipText.text = skipText;
        }

        #endregion

        #region Private data

        TMPro.TextMeshProUGUI m_skipText;
        Dialogue m_dialogue;
        InputAction m_validateAction;

        int m_currentSentenceIndex;

        #endregion

    }
}