﻿
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

using Common.Behaviour;

namespace Text.Behaviour
{
    /// <summary>
    /// Instructions about specific object will be visible when the player is in range.
    /// </summary>
    [RequireComponent(typeof(PopUp))]
    public class TogglePopUp : LullabyBehaviour
    {
        #region Serialised data

        /// <summary>
        /// Name of the 'toggle' action.
        /// </summary>
        [SerializeField] string[] toggleActionNames;

        #endregion
        
        #region Input system events

        void OnToggle(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                m_popUp.Pop(!m_popUp.IsPopped()); 
            }
        }
        
        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            m_popUp = GetComponent<PopUp>();
            Assert.IsNotNull(m_popUp);
            m_toggleActions = new InputAction[toggleActionNames.Length];
        }

        void Start()
        {
            // We know that by this point, every go's Awake has been called.
            Subscribe();
            m_isStarted = true;
        }

        void OnEnable()
        {
            if (m_isStarted)
            {
                Subscribe();
            }
        }

        void OnDisable()
        {
            if (m_toggleActions != null)
            {
                UnSubscribe();
            }
        }

        #endregion

        #region Private utility

        void Subscribe()
        {
            if (m_isSubscribed)
            {
                return;
            }

            for (int i = 0; i < toggleActionNames.Length; ++i)
            {
                m_toggleActions[i] = Player.GetAction(toggleActionNames[i]);
                Assert.IsNotNull(m_toggleActions[i]);
                m_toggleActions[i].performed += OnToggle;
            }
            m_isSubscribed = true;
        }

        void UnSubscribe()
        {
            if (!m_isSubscribed)
            {
                return;
            }
            for (int i = 0; i < toggleActionNames.Length; ++i)
            {
                m_toggleActions[i].performed -= OnToggle;
            }
            m_isSubscribed = false;
        }
        #endregion

        #region Private data
        
        PopUp m_popUp;
        InputAction[] m_toggleActions;
        bool m_isStarted;
        bool m_isSubscribed;

        #endregion
    }
}