﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Traduction
{
    /// <summary>
    /// Keys in the Dialogue dictionary corresponding to the dialogue to be displayed.:w
    /// 
    /// </summary>
    [System.Serializable]
    public class DialogueData
    {
        public string nameKey;

        [TextArea(1,5)]
        public string[] sentenceKeys;
    }
}
