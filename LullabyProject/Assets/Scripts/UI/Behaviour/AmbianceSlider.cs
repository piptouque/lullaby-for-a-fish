

using Music;
using Music.Behaviour;

namespace UI.Behaviour
{
    public class AmbianceSlider : MusicSlider<EAmbiance>
    {
        protected override AbstractMusicController<EAmbiance> GetController()
        {
            return AmbianceController;
        }
    }
}
