﻿

using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Assertions;

using TMPro;

using MptUnity.Utility;

namespace UI.Behaviour
{
    /// <summary>
    /// A class for handling colour transition
    /// with combined TMPro.TextMeshProUGUI and Unity.UI.Button.
    /// From totntonpiero's answer here:
    /// https://forum.unity.com/threads/best-way-to-change-button-label-color-along-with-button-sprite.458629/#post-6777089
    /// Also plays audio on navigation and stuff.
    /// Routing to UnityEngine.UI selection handling by Douglas Gaskell,
    /// see: https://gamedev.stackexchange.com/a/102097
    /// </summary>
    [ExecuteAlways]
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(Button))]
    public class LullabyButton : Selectable, 
        IEventSystemHandler, 
        ISelectHandler, 
        IPointerDownHandler,
        IPointerEnterHandler,
        IPointerClickHandler
    {
        #region Serialised data

        [Header("Audio")] 
        [SerializeField] AudioClip[] selectClips;
        [SerializeField] AudioClip[] hoverClips;

        // most of this is copied from UnityEngine.UI.Button

        [Header("Button")]
        [SerializeField]
        ButtonClickedEvent m_OnClick = new ButtonClickedEvent();
        
        [System.Serializable]
        public class ButtonClickedEvent : UnityEvent {}

        #endregion
        
        #region Selectable interfaces implementation

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);
            
            /*
            int i = Random.Range(0, selectClips.Length);
            m_source.PlayOneShot(selectClips[i]);
            */
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            // don't forget the case to base implementation!!
            base.OnPointerEnter(eventData);

            int i = Random.Range(0, hoverClips.Length);
            m_source.PlayOneShot(hoverClips[i]);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            // don't forget the case to base implementation!!
            base.OnPointerDown(eventData);
            // see:
            // https://gamedev.stackexchange.com/a/102097
            EventSystem.current.SetSelectedGameObject(gameObject, eventData);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
           int i = Random.Range(0, selectClips.Length);
           m_source.PlayOneShot(selectClips[i]);
           m_OnClick.Invoke(); 
        }

        public ButtonClickedEvent onClick
        {
            get { return m_OnClick; }
            set { m_OnClick = value; }
        }

        #endregion

        #region MonoBehaviour events

        protected override void Awake()
        {
            base.Awake();
            
            Assert.IsTrue(selectClips.Length > 0);
            foreach (var clip in selectClips)
            {
               Assert.IsNotNull(clip); 
            }
            Assert.IsTrue(hoverClips.Length > 0);
            foreach (var clip in hoverClips)
            {
               Assert.IsNotNull(clip); 
            }

            m_text = GetComponentInChildren<TextMeshProUGUI>();
            Assert.IsNotNull(m_text);

            m_buttonCanvasRenderer = GetComponent<CanvasRenderer>();
            Assert.IsNotNull(m_buttonCanvasRenderer);
            
            var audioSource = GetComponent<AudioSource>();
            Assert.IsNotNull(audioSource);
            // AudioSource.PlayClipAtPoint creates a temporary audio source
            // that is destroy when the clip ends,
            // This prevents the audio from being terminated when the scene is unload
            // -> happens when the button leads to another scene.
            // However, we can't configure it (change audio mixer group, for one thing),
            // So we create a temporary audio source by hand.
            // Taken from: https://gamedev.stackexchange.com/a/168904
            m_sourceObject = new GameObject($"{gameObject.name}_AudioSource_TEMP");
            m_sourceObject.hideFlags = HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild;
            m_source = m_sourceObject.AddComponent<AudioSource>();
            // Signal that this object should not be destroyed upon loading another scene!
            // CANNOT be used in the editor
            if (!Application.isEditor)
            {
                DontDestroyOnLoad(m_sourceObject);
            }
            // copy settings to temporary
            UnityAudioUtility.CopyAudioSourceSettings(audioSource, m_source);

        }

        void Update()
        {
            m_text.color = m_buttonCanvasRenderer.GetColor();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            // Invoke destruction the temp source when the clip is done playing.
            // We don't know the length of all playing clips on the object,
            // so we destroy it once we know they all ended,
            // that is, when the maximum clip length has passed.
            float maxClipLength = selectClips.Concat(hoverClips).Max(clip => clip.length);
            if (m_sourceObject != null)
            {
                if (!Application.isEditor)
                {
                    Destroy(m_sourceObject, maxClipLength);
                }
                else
                {
                    // We can't use Destroy in Editor,
                    // Fallback to DestroyImmediate.
                    DestroyImmediate(m_sourceObject);
                }
            }
        }

        #endregion

        #region Private data

        TextMeshProUGUI m_text;
        CanvasRenderer m_buttonCanvasRenderer;

        GameObject m_sourceObject;
        AudioSource m_source;

        #endregion
    }
}
