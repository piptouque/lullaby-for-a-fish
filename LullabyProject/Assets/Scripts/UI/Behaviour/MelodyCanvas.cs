
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

using Music;
using Interaction.Behaviour;
using Utility;

namespace UI.Behaviour
{
    [RequireComponent(typeof(Canvas))]
    public class MelodyCanvas : MonoBehaviour
    {

        #region Serialised data
        
        [Header("Melody object")]
        [SerializeField] GameObject melodyInteractiveObject;
        
        [Header("Background sign")]
        [SerializeField] GameObject melodySignObject;

        [Header("Buttons")]
        [SerializeField] GameObject melodyButtonPrefab;
        [Range(0f, 200f)] [SerializeField] float buttonSpread = 20;
        [SerializeField] Vector3 spreadDirection = Vector3.right;
        [Range(0f, 0.8f)] [SerializeField] float normalAlpha = 0.7f;
        [Range(0.8f, 1f)] [SerializeField] float selectedAlpha = 1f;
        

        [Header("Transitions")] 
        [Range(0f, 2f)]
        [SerializeField] float animationSpeed = 2f;
        [Range(0f, 2f)]
        [SerializeField] float fadeDuration = 0.3f;
        
        #endregion
        
        #region Public
        
        public void SetNumberButtons(int numberButtons)
        {
            // Buttons
            if (m_melodyButtonObjects != null)
            {
                // quick and dirty, whatever.
                foreach (var melodyButtonObject in m_melodyButtonObjects)
                {
                    Destroy(melodyButtonObject);
                }
            }
            
            AllocateButtons(numberButtons);
            
            for (int i = 0; i < numberButtons; ++i)
            {
                m_melodyButtonObjects[i] = Instantiate(melodyButtonPrefab, melodySignObject.transform, false);
                
                // Setting the rect transform
                var buttonOffset = buttonSpread * (i - numberButtons / 2) * spreadDirection;
                m_melodyButtonRectTransform[i] = m_melodyButtonObjects[i].GetComponent<RectTransform>();
                var rectTransform = m_melodyButtonRectTransform[i];
                Assert.IsNotNull(rectTransform);
                rectTransform.anchoredPosition3D = buttonOffset;
                
                //
                m_melodyButtons[i] = m_melodyButtonObjects[i].GetComponent<Button>();
                Assert.IsNotNull(m_melodyButtons[i]);
                
                // Set animation speeds
                var colours = m_melodyButtons[i].colors;
                colours.fadeDuration = fadeDuration;
                m_melodyButtons[i].colors = colours;
            }

            // Caching the colours
            m_buttonNormalColours = m_melodyButtons.Select(button => button.colors.normalColor).ToArray();
            m_buttonSelectedColours = m_melodyButtons.Select(button => button.colors.selectedColor).ToArray();
            
            // Sign
            // Set the sign's dimensions according to the button number and spread.
            var signRect = m_melodySignRectTransform.rect;
            m_melodySignRectTransform.sizeDelta = new Vector2(numberButtons * buttonSpread, signRect.height);
            
            // Set animation speed
            m_melodySignAnimator.speed = animationSpeed;
        }

        public void SetMelodyNote(int melodyIndex, ENoteColour noteColour)
        {
            Assert.IsTrue(melodyIndex >= 0 && melodyIndex < m_melodyButtons.Length);
            
            var button = m_melodyButtons[melodyIndex];
            // Doesn't seem to be optimal, but well.
            // see: https://forum.unity.com/threads/changing-the-color-of-a-ui-button-with-scripting.267093/#post-1766894
            var colours = button.colors;
            // caching the colours.
            
            // Setting the colour according to the note.
            Color colour = NoteColours.GetColour(noteColour);
            colour.a = normalAlpha;
            colours.normalColor = colour;
            colour.a = selectedAlpha;
            colours.selectedColor = colour;
            button.colors = colours;
            // Select this button!
            button.Select();

        }

        public void ResetMelodyNote(int melodyIndex)
        {
            Assert.IsTrue(melodyIndex >= 0 && melodyIndex < m_melodyButtons.Length);
            var button = m_melodyButtons[melodyIndex];
            var normalColour = m_buttonNormalColours[melodyIndex];
            var selectedColour = m_buttonSelectedColours[melodyIndex];
            
            var colours = button.colors;
            colours.normalColor = normalColour;
            colours.selectedColor = selectedColour;
            button.colors = colours;
        }

        public void ResetMelody()
        {
            for (int i = 0; i < m_melodyButtons.Length; ++i)
            {
                ResetMelodyNote(i);
            }
        }
        
        public void SetVisible(bool visible)
        {
            m_melodySignAnimator.ResetTrigger(visible ? Animations.s_tSignOut : Animations.s_tSignIn);
            m_melodySignAnimator.SetTrigger(visible ?   Animations.s_tSignIn  : Animations.s_tSignOut);
        }

        #endregion
        
        #region MonoBehaviour events

        void Awake()
        {
            Assert.IsNotNull(melodyInteractiveObject);
            Assert.IsNotNull(melodySignObject);
            Assert.IsNotNull(melodyButtonPrefab);

            m_melodyInteractive = melodyInteractiveObject.GetComponent<AbstractMelodyInteractiveObject>();
            Assert.IsNotNull(m_melodyInteractive);
            
            m_canvas = GetComponent<Canvas>();
            Assert.IsNotNull(m_canvas);

            m_melodySignRectTransform = melodySignObject.GetComponent<RectTransform>();
            Assert.IsNotNull(m_melodySignRectTransform);
            m_melodySignAnimator = melodySignObject.GetComponent<Animator>();
            Assert.IsNotNull(m_melodySignAnimator);

        }

        void Start()
        {
            if (!m_isSet)
            {
                Set();
            }
        }

        void OnEnable()
        {
            // Beware, some of these event will be called at Start, we need to be ready, maybe?
            if (!m_isSet)
            {
                Set();
            }
            m_melodyInteractive.AddMelodyEnableListener(OnMelodyChange);
        }

        void OnDisable()
        {
            if (m_isSet)
            {
                m_melodyInteractive.RemoveMelodyEnableListener(OnMelodyChange);
            }
        }

        #endregion

        #region Private utility

        void Set()
        {
            if (m_isSet)
            {
                return;
            }
            // force update and set values!
            SetNumberButtons(m_initNumberButtons);
            
            SetVisible(false);
            m_isSet = true;
        }
        
        void AllocateButtons(int numberButtons)
        {
            m_melodyButtonObjects = new GameObject[numberButtons];
            m_melodyButtons = new Button[numberButtons];
            m_melodyButtonRectTransform = new RectTransform[numberButtons];
            m_buttonNormalColours = new Color[numberButtons];
            m_buttonSelectedColours = new Color[numberButtons];
        }

        void OnMelodyChange(int _, bool __)
        {
            int numberButtons = m_melodyInteractive.GetDepthMaxEnabled();
            if (!m_isSet)
            {
                m_initNumberButtons = numberButtons;
                return;
            }
            SetNumberButtons(numberButtons);
        }

        #endregion
        
        #region Private data

        Canvas m_canvas;
        
        // MelodyInteractiveObject (fish, mostly)
        AbstractMelodyInteractiveObject m_melodyInteractive;
        
        // Sign
        RectTransform m_melodySignRectTransform;
        Animator m_melodySignAnimator;
        
        // Buttons.
        GameObject[] m_melodyButtonObjects;
        Button[] m_melodyButtons;
        RectTransform[] m_melodyButtonRectTransform;

        Color[] m_buttonNormalColours;
        Color[] m_buttonSelectedColours;

        bool m_isSet;
        int m_initNumberButtons;

        #endregion
    }
}