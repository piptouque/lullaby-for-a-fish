
using UnityEngine;
using UnityEngine.Assertions;

using Common.Behaviour;

namespace UI.Behaviour
{
    [RequireComponent(typeof(LullabyButton))]
    public class MoodSignButton : LullabyBehaviour
    {

        #region Serialised data

        [SerializeField] string automaticText = "Mood";
        [SerializeField] string manualText = "Manual";

        #endregion

        #region Button and Mood events

        void OnButtonClick()
        {
            MoodController.IsUpdateEnabled = !MoodController.IsUpdateEnabled;
        }

        void OnMusicControllerEnableEvent(bool isActive)
        {
            UpdateText();
        }
        
        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            m_moodControlButton = GetComponent<LullabyButton>();
            Assert.IsNotNull(m_moodControlButton);
            m_text = GetComponentInChildren<TMPro.TextMeshProUGUI>();
            Assert.IsNotNull(m_text);
            UpdateText();
        }

        void Start()
        {
            Subscribe();
            m_isStarted = true;
        }

        void OnEnable()
        {
            if (m_isStarted)
            {
                Subscribe();
            }
        }

        void OnDisable()
        {
            UnSubscribe();
        }
        
        #endregion

        #region Private data

        void Subscribe()
        {
            if (m_isSubscribed)
            {
                return;
            }
            MoodController.AddOnEnableUpdateListener(OnMusicControllerEnableEvent);
            m_moodControlButton.onClick.AddListener(OnButtonClick);
            m_isSubscribed = true;
        }

        void UnSubscribe()
        {
            if (!m_isSubscribed)
            {
                return;
            }
            m_moodControlButton.onClick.RemoveListener(OnButtonClick);
            MoodController.RemoveOnEnableUpdateListener(OnMusicControllerEnableEvent);
            m_isSubscribed = false;
        }

        void UpdateText()
        {
            m_text.text = MoodController.IsUpdateEnabled ? automaticText : manualText;
        }

        #endregion

        #region Private data

        LullabyButton m_moodControlButton;
        TMPro.TextMeshProUGUI m_text;
        bool m_isSubscribed;
        bool m_isStarted;

        #endregion
    }
}