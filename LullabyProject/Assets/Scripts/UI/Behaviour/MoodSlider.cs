
using Music;
using Music.Behaviour;

namespace UI.Behaviour
{
    public class MoodSlider : MusicSlider<EMood>
    {
        protected override AbstractMusicController<EMood> GetController()
        {
            return MoodController;
        }
    }
}