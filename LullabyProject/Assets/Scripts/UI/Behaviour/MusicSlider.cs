
using UnityEngine;
using UnityEngine.Assertions;

using Music.Behaviour;
using Common.Behaviour;

namespace UI.Behaviour
{
    [RequireComponent(typeof(UnityEngine.UI.Slider))]
    public abstract class MusicSlider<E> : LullabyBehaviour
        where E : struct, System.Enum
    {
        #region Serialised data

        [SerializeField] E e;
        
        #endregion

        #region To resolve

        protected abstract AbstractMusicController<E> GetController();

        #endregion

        #region MonoBehaviour events

        void Awake()
        {
            m_slider = GetComponent<UnityEngine.UI.Slider>();
            Assert.IsNotNull(m_slider);
            
            m_text = GetComponentInChildren<TMPro.TextMeshProUGUI>();
            Assert.IsNotNull(m_text);
        }

        void Start()
        {
            m_controller = GetController(); 

            m_slider.onValueChanged.AddListener(OnValueChanged);
            m_isMoving = false;


            m_text.text = e.ToString();

            m_slider.minValue = 0f;
            m_slider.maxValue = 1;
        }

        void Update()
        {
           if (!m_isMoving)
           {
               float moodValue = m_controller.GetValue(e);
               m_slider.SetValueWithoutNotify(moodValue);
           }
        }
        
        #endregion
        
        #region Private utility

        void OnValueChanged(float value)
        {
            if (m_controller != null)
            {
                m_controller.SetValue(e, value);
            }
        }

        #endregion

        #region Private

        UnityEngine.UI.Slider m_slider;
        
        AbstractMusicController<E> m_controller;

        TMPro.TextMeshProUGUI m_text;

        bool m_isMoving;

        #endregion
    }
}
