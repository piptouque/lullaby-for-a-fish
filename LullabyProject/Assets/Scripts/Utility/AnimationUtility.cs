
using UnityEngine;

namespace Utility
{
    public class Animations
    {
        // My IDE does wonders.
        // Flute animation
        public static readonly int s_eNoteColourId = Animator.StringToHash("ENoteColour");
        public static readonly int s_noteToneId = Animator.StringToHash("NoteTone");
        public static readonly int s_noteVolumeId = Animator.StringToHash("NoteVolume");
        public static readonly int s_notePanningId = Animator.StringToHash("NotePanning");
        public static readonly int s_tNoteStoppedId = Animator.StringToHash("TNoteStopped");
        public static readonly int s_tNoteStartedId = Animator.StringToHash("TNoteStarted");
        // Zone Animator
        public static readonly int s_eZoneId = Animator.StringToHash("EZone");
        public static readonly int s_tZoneExitId = Animator.StringToHash("TExit");
        public static readonly int s_tZoneEnterId = Animator.StringToHash("TEnter");
        // Scene management
        public static readonly int s_isLoadingId = Animator.StringToHash("IsLoading");
        public static readonly int s_tExitConfigMenuId = Animator.StringToHash("TExitConfigMenu");
        public static readonly int s_tEnterMainMenuId = Animator.StringToHash("TEnterMainMenu");
        public static readonly int s_tEnterPauseMenuId = Animator.StringToHash("TEnterPauseMenu");
        public static readonly int s_tExitPlayId = Animator.StringToHash("TExitPlay");
        public static readonly int s_tEnterPauseId = Animator.StringToHash("TEnterPause");
        public static readonly int s_tExitGameCutsceneId = Animator.StringToHash("TExitGameCutscene");
        public static readonly int s_tExitMainMenuId = Animator.StringToHash("TExitMainMenu");
        public static readonly int s_tExitMainId = Animator.StringToHash("TExitMain");
        public static readonly int s_tEnterGameId = Animator.StringToHash("TEnterGame");
        public static readonly int s_tEnterPlayId = Animator.StringToHash("TEnterPlay");
        public static readonly int s_tEnterCreditsId = Animator.StringToHash("TEnterCredits");
        public static readonly int s_tEnterConfigMenuId = Animator.StringToHash("TEnterConfigMenu");
        public static readonly int s_tExitCreditsId = Animator.StringToHash("TExitCredits");
        public static readonly int s_tExitCutscenePlayId = Animator.StringToHash("TExitCutscenePlay");
        public static readonly int s_tEnterMainId = Animator.StringToHash("TEnterMain");
        public static readonly int s_tExitPauseMenuId = Animator.StringToHash("TExitPauseMenu");
        public static readonly int s_tExitPauseId = Animator.StringToHash("TExitPause");
        public static readonly int s_previousSceneId = Animator.StringToHash("PreviousSceneIndex");
        public static readonly int s_tCutsceneId = Animator.StringToHash("CutsceneIndex");
        public static readonly int s_tEnterCutsceneId = Animator.StringToHash("TEnterCutscene");
        public static readonly int s_tEnterCutscenePlayId = Animator.StringToHash("TEnterCutscenePlay");
        // Garden
        public static readonly int s_tBornId = Animator.StringToHash("TBorn");
        public static readonly int s_tDyingId = Animator.StringToHash("TDying");
        // Footstep controller
        public static readonly int s_tStepId = Animator.StringToHash("TStep");
        // Signs
        public static readonly int s_tSignIn = Animator.StringToHash("TEnterSign");
        public static readonly int s_tSignOut = Animator.StringToHash("TExitSign");
        // Dialogue
        public static readonly int s_tEnterDialogue = Animator.StringToHash("TEnterDialogue");
        public static readonly int s_tExitDialogue = Animator.StringToHash("TExitDialague");
    }

    public static class InputActions
    {
        public const string c_quitGameActionName = "QuitGame";
        public const string c_pauseActionName = "Pause";
        public const string c_unpauseActionName = "Unpause";
        public const string c_skipCutsceneActionName = "SkipCutscene";
        public const string c_backCreditsActionName = "BackCredits";
        public const string c_validatePopUpActionName = "Validate";
        public const string c_toggleMoodCanvasActionName = "ToggleMoodCanvas";
        
        public const string c_mainControlsActionMapName = "Main Controls";
        public const string c_configControlsActionMapName = "Config Controls";
        public const string c_creditsControlsActionMapName = "Credits Controls";
        public const string c_cutsceneControlsActionMapName = "Cutscene Controls";
        public const string c_pauseControlsActionMapName = "Pause Controls";
        public const string c_gameControlsActionMapName = "Game Controls";
        public const string c_popUpControlsActionMapName = "Dialogue Controls";
    }

}