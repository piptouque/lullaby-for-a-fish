

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Utility
{
    public static class Levels
    {
        public const int c_numberLevels = 5; // 3 + tutorial + end
    }

    public static class Melodies
    {
        public const int c_numberMelodies = 4; // 3 + tutorial
    }

    public enum EZone
    {
        eHub = 0,
        eZone1 = 1,
        eZone2 = 2,
        eZone3 = 3,
        eNone
    }
    public static class Zones
    {
        public static int GetNumber()
        {
            return System.Enum.GetValues(typeof(EZone)).Length;
        }

        public static int GetNumberNotNone()
        {
            return GetNumber() - 1;
        }

        public static string GetManagerTag(EZone zone)
        {
            Assert.IsTrue(s_tagToZones.ContainsValue(zone));
            return s_tagToZones.FirstOrDefault(tag => tag.Value == zone).Key;
        }

        public static EZone GetZone(string tag)
        {
            bool found = s_tagToZones.TryGetValue(tag, out EZone zone);
            Assert.IsTrue(found);
            return zone;
        }

        static readonly Dictionary<string, EZone> s_tagToZones = new Dictionary<string, EZone>
        {
            { Tags.c_hubManagerTag, EZone.eHub },
            { Tags.c_zone1ManagerTag, EZone.eZone1 },
            { Tags.c_zone2ManagerTag, EZone.eZone2 },
            { Tags.c_zone3ManagerTag, EZone.eZone3 },
        };
    }
    
    public enum ESky
    {
        eDawn,
        eMorning,
        eNoon,
        eEvening,
        eNight
    }
    
    public static class Skies
    {
        public const int c_numberSkies = 5;

        public static readonly ESky[] s_levelToSky = new ESky[Levels.c_numberLevels]
        {
            ESky.eMorning, ESky.eNoon, ESky.eEvening, ESky.eNight, ESky.eDawn
        };
    }


    public enum EGround
    {
        eGrass,
        eInterior,
        eBridge,
        eDefault = eGrass
    }
    
    public static class Grounds
    {
        public static int GetNumber()
        {
            int number =  System.Enum.GetValues(typeof(EGround)).Length;
            // stupid to put a check here, but I don't know where to otherwise.
            // if it's false then change c_numberGroundsNonDefault. 
            UnityEngine.Assertions.Assert.IsTrue(c_numberGroundsNonDefault == number - 1);
            return number;
        }
        
        public static int GetNumberNonDefault()
        {
            return c_numberGroundsNonDefault;
        }
        // also public in order to use constant array size alloc.
        public const int c_numberGroundsNonDefault = 3;

        /// <summary>
        /// Get the layer mask formed by all 'ground' layers.
        /// </summary>
        /// <returns></returns>
        public static LayerMask GetGroundLayerMask()
        {
            if (!s_isGroundLayerMaskSet)
            {
                s_groundLayerMask = LayerMask.GetMask(Layers.s_groundLayerNames);
                s_isGroundLayerMaskSet = true;
            }
            return s_groundLayerMask;
        }

        public static int GetGroundLayer(EGround kind)
        {
            return LayerMask.NameToLayer(Layers.s_groundLayerNames[(int) kind]);
        }

        public static EGround GetGroundKind(int groundLayer)
        {
            string groundLayerName = LayerMask.LayerToName(groundLayer);
            int groundIndex = System.Array.IndexOf(Layers.s_groundLayerNames, groundLayerName);
            // will be 'default' if the ground 
            return groundIndex < 0 ? EGround.eDefault : (EGround) groundIndex;
        }

        static LayerMask s_groundLayerMask;
        static bool s_isGroundLayerMaskSet;
    }
}
