
using System;
using System.Collections.Generic;

namespace Utility
{
    public interface ITreeNode<T, TWeight>
        where T : new()
        where TWeight : /* IComparable, */ new()
    {

        /// <summary>
        /// Adds a child TreeNode with value and weight to this TreeNode.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="weight"></param>
        /// <returns>Added child TreeNode.</returns>
        ITreeNode<T, TWeight> AddChild(T val, TWeight weight);

        /// <summary>
        /// Looks for a *direct* child with the given value.
        /// </summary>
        /// <param name="val"></param>
        /// <returns>TreeNode corresponding to the first child with
        /// given value on success, /// null on failure.</returns>
        ITreeNode<T, TWeight> FindChild(Predicate<ITreeNode<T, TWeight>> pred);
        
        /// <summary>
        /// Looks for a *direct* child with a predicate.
        /// </summary>
        /// <param name="val"></param>
        /// <returns>TreeNode corresponding to the first child with
        /// given value on success, /// null on failure.</returns>
        ITreeNode<T, TWeight> FindChild(T val);
        
        T Value { get; set; }
        /// <summary>
        /// Weight of connection to parent node.
        /// </summary>
        TWeight Weight { get; set; }

        /// <summary>
        /// Checks whether the node is a leaf node (ie. has no children).
        /// </summary>
        /// <returns></returns>
        bool IsLeaf();
        /// <summary>
        /// Checks whether the node is a root node (ie. has no parent).
        /// </summary>
        /// <returns></returns>
        bool IsRoot();

        /// <summary>
        /// Checks whether the node has a parent.
        /// </summary>
        /// <returns></returns>
        bool HasParent();

        /// <summary>
        /// Returns the depth of this node: the length of the path from the root.
        /// </summary>
        /// <returns></returns>
        int GetDepth();

        /// <summary>
        /// Returns the altitude of this node: the greatest length of paths to leaves.
        /// </summary>
        /// <returns></returns>
        int GetAltitude(Predicate<ITreeNode<T, TWeight>> pred);
        int GetAltitude();

        /// <summary>
        /// Get the list of ancestors from root to this TreeNode.
        /// Beware, shallow copy!
        /// </summary>
        /// <returns></returns>
        List<TreeNode<T, TWeight>> GetPathFromRoot();
    }
}