
using UnityEngine.Assertions;

namespace Utility
{
    public enum EAmbientNoise
    {
        eFireplace = 0,
        eForest    = 1,
        eInterior  = 2,
        eWaterfall = 3,
        eNight     = 4
    }
    public class AmbientNoises
    {
        public int GetNumber()
        {
            return System.Enum.GetValues(typeof(EAmbientNoise)).Length;
        } 
    }

    public enum EBackgroundMusic
    {
       eMenu = 0,
       eHub = 1,
       eZone1 = 2,
       eZone2 = 3,
       eZone3 = 4
    }
    public static  class BackgroundMusics
    {
        public static int GetNumber()
        {
            return System.Enum.GetValues(typeof(EBackgroundMusic)).Length;
        }
    }
}