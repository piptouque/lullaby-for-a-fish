

namespace Utility
{
    public static class Tags
    {
        // General
        public const string c_levelManagerTag = "LevelManager";
        public const string c_lightManagerTag = "LightManager";
        public const string c_playerTag = "Player";
        public const string c_playerStateTag = "PlayerState";
        public const string c_gameAnimatorTag = "GameAnimator";
        
        
        // GameManagers
        public const string c_playManagerTag = "PlayManager";
        public const string c_hubManagerTag = "HubManager";
        public const string c_zone1ManagerTag = "Zone1Manager";
        public const string c_zone2ManagerTag = "Zone2Manager";
        public const string c_zone3ManagerTag = "Zone3Manager";
        public const string c_zoneAnimatorTag = "ZoneAnimator";
        
        // Music
        public const string c_backgroundMusicTag = "BackgroundMusic";
        public const string c_ambientNoiseTag = "AmbientNoise";
        public const string c_moodControllerTag = "MoodController";
        public const string c_ambianceControllerTag = "AmbianceController";
        public const string c_moodModifierTag = "MoodModifier";
        public const string c_ambianceModifierTag = "AmbianceModifier";
    }

    public static class Layers
    {

        // Ground
        public static readonly string[] s_groundLayerNames = new string [Grounds.c_numberGroundsNonDefault]
        {
            "GroundGrass", "GroundInterior", "GroundBridge"
        };
    }

    public static class Audio
    {
        public const string c_audioMixerDirectoryPath = "Audio/AudioMixers/";
        public const string c_mainAudioMixerName = "MainAudioMixer";
        public const string c_footstepAudioMixerGroupName = "Footsteps";
        public const string c_footstepPitchBendPropertyName = "FootstepPitchBend";
    }
}