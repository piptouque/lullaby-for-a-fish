
using System.Collections.Generic;

namespace Utility
{
    public enum ECutscene 
    {
        eIntroCutscene,
        eEndIntroCutscene,
        eEndFirstLevelCutscene,
        eEndSecondLevelCutscene,
        eEndThirdLevelCutscene,
    }
    public static class Cutscenes
    {
        public static int GetNumber()
        {
            return System.Enum.GetValues(typeof(ECutscene)).Length; 
        }
    }

    public static class Scenes
    {
        public enum EScene
        {
            eMain,
            ePause,
            eConfig,
            eCredits,
            ePlay,
            eCutscene,
        }
        
        public const string c_rootSceneName = "Root";
        public const string c_mainMenuSceneName = "MainMenu";
        public const string c_pauseMenuSceneName = "PauseMenu";
        public const string c_configMenuSceneName = "ConfigMenu";
        public const string c_creditsSceneName = "Credits";
        public const string c_introCutsceneName = "IntroCutscene";
        public const string c_endIntroCutsceneName = "HutCutscene";
        public const string c_endFirstLevelCutsceneName = "HouseCutscene";
        public const string c_endSecondLevelCutsceneName = "CastleCutscene";
        public const string c_endThirdLevelCutsceneName = "FinalCutscene";
        
        public static readonly List<string> s_gameSceneNames = new List<string>
        {
            "Common",
            "Background",
            "Hub",
            "Zone1",
            "Zone2",
            "Zone3",
        };
        
        public const string c_mainMenuBatchName = "MainMenuBatch";
        public const string c_pauseMenuBatchName = "PauseMenuBatch";
        public const string c_configMenuBatchName = "ConfigMenuBatch";
        public const string c_creditsBatchName = "CreditsBatch";
        public const string c_gameBatchName = "GameBatch";
        public const string c_introCutsceneBatchName = "IntroCutsceneBatch";
        public const string c_endIntroCutsceneBatchName = "EndIntroCutsceneBatch";
        public const string c_endFirstLevelCutsceneBatchName = "EndFirstLevelCutsceneBatch";
        public const string c_endSecondLevelCutsceneBatchName = "EndSecondLevelCutsceneBatch";
        public const string c_endThirdLevelCutsceneBatchName = "EndThirdLevelCutsceneBatch";
        
        // IMPORTANT:
        // ELEMENTS OF s_batchNames AND s_sceneNameBatches
        // SHOULD MATCH
        
        public static readonly Dictionary<string, List<string>> s_batchToSceneNames = new Dictionary<string, List<string>>
        {
            { c_mainMenuBatchName,                new List<string> { c_mainMenuSceneName } },
            { c_pauseMenuBatchName,               new List<string> { c_pauseMenuSceneName } },
            { c_configMenuBatchName,              new List<string> { c_configMenuSceneName } },
            { c_creditsBatchName,                 new List<string> { c_creditsSceneName } },
            { c_gameBatchName,                    s_gameSceneNames },
            { c_introCutsceneBatchName,           new List<string> { c_introCutsceneName } },
            { c_endIntroCutsceneBatchName,        new List<string> { c_endIntroCutsceneName } },
            { c_endFirstLevelCutsceneBatchName,   new List<string> { c_endFirstLevelCutsceneName } },
            { c_endSecondLevelCutsceneBatchName,  new List<string> { c_endSecondLevelCutsceneName } },
            { c_endThirdLevelCutsceneBatchName,   new List<string> { c_endThirdLevelCutsceneName } },
        };
    }
}