
using System;
using System.Collections.Generic;
using System.Linq;

namespace Utility
{
    /// <summary>
    /// idea from; https://stackoverflow.com/a/11879200
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TWeight"></typeparam>
    public class TreeNode<T, TWeight> : ITreeNode<T, TWeight>
        where T : new()
         where TWeight : /*IComparable, */ new()
    {

        #region Life-cycle
        
        public TreeNode(T val)
        {
            m_parent = null;
            m_children = new List<TreeNode<T, TWeight>>();
            Value = val;
        }

        public TreeNode() : this(new T())
        {
        }
        
        #endregion

        #region Public

        public T Value { get; set; }
        public TWeight Weight { get; set; }

        public ITreeNode<T, TWeight> AddChild(T val, TWeight weight)
        {
            var child = new TreeNode<T, TWeight>(val);
            m_children.Add(child);
            child.m_parent = this;
            child.Weight = weight;
            return child;
        }
        
        public ITreeNode<T, TWeight> FindChild(T val)
        {
            // Can't use operator== with generic types.
            // use this instead: https://stackoverflow.com/a/390974
            return FindChild(
                child => 
                    EqualityComparer<T>.Default.Equals(child.Value, val)
                    );
        }
        
        public ITreeNode<T, TWeight> FindChild(Predicate<ITreeNode<T, TWeight>> pred)
        {
            return m_children.Find(pred);
        }

        public List<TreeNode<T, TWeight>> ClearChildren()
        {
            foreach (var child in m_children)
            {
                child.m_parent = null;
            }
            var children = m_children;
            m_children = new List<TreeNode<T, TWeight>>();
            return children;
        }

        public List<TreeNode<T, TWeight>> GetPathFromRoot()
        {
            var path = new List<TreeNode<T, TWeight>>();
            var currentNode = this;
            while (currentNode.HasParent())
            {
                path.Add(currentNode);
                currentNode = currentNode.m_parent;
            }
            return path;
        }

        public bool IsLeaf()
        {
            return m_children.Count == 0;
        }

        public bool HasParent()
        {
            return m_parent != null;
        }

        public int GetDepth()
        {
            int depth = 0;
            var parent = m_parent;
            while (parent != null)
            {
                parent = parent.m_parent;
                ++depth;
            }
            return depth;
        }

        public int GetAltitude()
        {
            return GetAltitude(node => true);
        }
        
        public int GetAltitude(Predicate<ITreeNode<T, TWeight>> pred)
        {
            return GetAltitudeRecursive(pred, 0);
        }

        public bool IsRoot()
        {
            return !HasParent();
        }

        /// <summary>
        /// Creates a Tree structure from an ordered two-dimensional list of values, with a leading root node.
        /// Initialises weights with functor
        /// from the indices (i, j) in the list
        /// and the parent's / child's values.
        /// </summary>
        /// <param name="arrays"></param>
        /// <param name="weightFunctor"></param>
        /// <returns></returns>
        public static TreeNode<T, TWeight> FromArrays(
            IEnumerable<IEnumerable<T>> arrays,
            Func<TWeight, int, int, TWeight> weightFunctor
            )
        {
            var root = new TreeNode<T, TWeight>();
            int i = 0;
            foreach (var array in arrays)
            {
                ITreeNode<T, TWeight> currentNode = root;
                int depth = 0;
                foreach (T el in array)
                {
                    var childNode = currentNode.FindChild(el);
                    if (childNode == null)
                    {
                        childNode = currentNode.AddChild(el, weightFunctor(new TWeight(), depth, i));
                    }
                    else
                    {
                        childNode.Weight = weightFunctor(childNode.Weight, depth, i);
                    }
                    currentNode = childNode;
                    ++depth;
                }

                ++i;
            }
            return root;
        }
        #endregion

        #region Private utility


        int GetAltitudeRecursive(Predicate<ITreeNode<T, TWeight>> pred, int currentAltitude)
        {
            var validChildren = m_children.Where(node => pred(node));
            // if no children satisfy the condition (or if this node is a leaf, as a stopping condition),
            // then return the accumulator.
            return validChildren.Any()
                ? m_children.Max(node => node.GetAltitudeRecursive(pred, currentAltitude + 1))
                : currentAltitude;
        }

        #endregion
        
        #region Private data

        TreeNode<T, TWeight> m_parent;
        List<TreeNode<T, TWeight>> m_children;

        #endregion
    }
}