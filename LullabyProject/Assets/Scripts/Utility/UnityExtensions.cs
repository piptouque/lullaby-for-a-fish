

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Utility
{
    public static class UnityExtensions
    {
        public static TComponent FindComponentWithTagUnique<TComponent>(TComponent target, string objectTag)
            where TComponent : class
        {
            if (target == null)
            {
               var comps = FindComponentsWithTag<TComponent>(objectTag).ToArray();
               Debug.Assert(comps.Length == 1, 
                   $"Requested unique Component of type {typeof(TComponent)} " +
                   $"in object of tag {objectTag} was found {comps.Length} instead of once.");
               Assert.IsTrue(comps.Length == 1);
               target = comps.First();
            }
            return target;
        }
        // Don't restrict to MonoBehaviour, could be an interface for a type inheriting from MonoBehaviour!
        public static IEnumerable<TComponent> FindComponentsWithTag<TComponent>(string objectTag)
            where TComponent : class 
        {
            return GameObject.FindGameObjectsWithTag(objectTag).Select(go =>
            {
                var comp = go.GetComponent<TComponent>();
                Assert.IsNotNull(comp);
                return comp;
            });
        }
    }
}
