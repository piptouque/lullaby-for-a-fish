﻿
using UnityEngine.Events;
using UnityEngine.Assertions;

namespace MptUnity.Audio.Behaviour
{
    public abstract class AbstractInstrumentSource
        : AbstractTrackSource, IInstrumentSource
    {

        #region Serialiesd

        [UnityEngine.SerializeField] TrackFile file;
        [UnityEngine.SerializeField] int startingNumberVoices;

        #endregion


        #region Unity Audio management

        protected override void Awake()
        {
            base.Awake();
            
            Assert.IsTrue(file.IsValid());
            
            instrumentSourceEvents = new InstrumentEvents();
            m_isLoaded = false;

            Load(file, startingNumberVoices);
        }

        #endregion
        
        #region IInstrumentSource implementation

        public int StartNote(MusicalNote note)
        {
            Assert.IsTrue(IsLoaded());
            int voice = GetInstrument().PlayNote(note);
            if (voice == -1)
            {
                UnityEngine.Debug.Log("Failed to play note, err: "
                                      + GetInstrument().GetLastErrorMessage());
            }

            return voice;
        }
        
        public bool StopNote(int voice)
        {
            Assert.IsTrue(IsLoaded());
            return GetInstrument().StopNote(voice);
        }

        public void StopAllNotes()
        {
            Assert.IsTrue(IsLoaded());
            GetInstrument().StopAllNotes();
        }


        public bool CanStart(MusicalNote note)
        {
            Assert.IsTrue(IsLoaded());
            return GetInstrument().CanPlay(note);
        }

        public bool CanStop(int voice)
        {
            Assert.IsTrue(IsLoaded());
            return GetInstrument().CanStop(voice);
        }

        public MusicalNote GetNote(int voice)
        {
            Assert.IsTrue(IsLoaded());
            return GetInstrument().GetNote(voice);
        }

        public int GetNumberVoices()
        {
            Assert.IsTrue(IsLoaded());
            return GetInstrument().GetNumberVoices();
        }

        
        public int NumberVoices
        {
            get
            {
                Assert.IsTrue(IsLoaded());
                return GetInstrument().GetNumberVoices();
            }
            set
            {
                
                Assert.IsTrue(IsLoaded());
                if (value != NumberVoices)
                {
                    GetInstrument().SetNumberVoices(value);
                }
            }
        }

        #endregion

        #region To resolve

        protected abstract IInstrument CreateInstrument(byte[] data, string info, int numberVoices);

        #endregion

        #region Event handling

        protected class InstrumentEvents
        {
            public readonly OnInstrumentNoteStartEvent instrumentNoteStartEvent;
            public readonly OnInstrumentNoteStopEvent instrumentNoteStopEvent;

            public InstrumentEvents()
            {
                instrumentNoteStartEvent = new OnInstrumentNoteStartEvent();
                instrumentNoteStopEvent  = new OnInstrumentNoteStopEvent();
            }
        }

        public void AddOnNoteStartListener(UnityAction<MusicalNote> onNoteStart)
        {
            instrumentSourceEvents.instrumentNoteStartEvent.AddListener(onNoteStart);
        }

        public void RemoveOnNoteStartListener(UnityAction<MusicalNote> onNoteStart)
        {
            instrumentSourceEvents.instrumentNoteStartEvent.RemoveListener(onNoteStart);
        }

        public void AddOnNoteStopListener(UnityAction<MusicalNote> onNoteStop)
        {
            instrumentSourceEvents.instrumentNoteStopEvent.AddListener(onNoteStop);
        }

        public void RemoveOnNoteStopListener(UnityAction<MusicalNote> onNoteStop)
        {
            instrumentSourceEvents.instrumentNoteStopEvent.RemoveListener(onNoteStop);
        }
        #endregion

        
        #region IInstrumentSource partial implementaion

        public override bool IsLoaded()
        {
            return m_isLoaded;
        }

        public override ITrack GetTrack()
        {
            return GetInstrument();
        }

        public IInstrument GetInstrument()
        {
            Assert.IsTrue(IsLoaded());
            return m_instrument;
        }

        public override void Unload()
        {
            m_instrument = null;
        }

        public override void Load()
        {
            Load(file, startingNumberVoices);
        }


        #endregion
        
        #region Protected data

        
        protected InstrumentEvents instrumentSourceEvents;

        #endregion
        
        #region Private utility

        void Load(TrackFile aFile, int numberVoices)
        {
            try
            {
                m_instrument = CreateInstrument(aFile.track.bytes, aFile.info.text, numberVoices);
                m_instrument.SetRepeatCount(-1);
                m_isLoaded = true;
                // Play it continuously for now.
                // todo
                source.Play();
            }
            catch (System.ArgumentException)
            {
                UnityEngine.Debug.LogError(
                    "Failed to load MOD instrument."
                    ) ;
            }
        }

        #endregion
        
        #region Private data

        IInstrument m_instrument;
        bool m_isLoaded;

        #endregion
    }
}