﻿
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Assertions;

namespace MptUnity.Audio.Behaviour
{
    public abstract class AbstractJukebox : AbstractMusicSource, IJukebox
    {
        #region Serialised data

        [Header("Playback")] public int startingTrackIndex;
        [Header("Files")]
        [SerializeField] TrackFile[] files;

        #endregion

        #region AbstractMusicSource partial resolution

        public override IMusic GetMusic()
        {
            return m_musics[m_currentTrackIndex];
        }

        public override bool IsLoaded()
        {
            return (m_currentTrackIndex >= 0)
                   && (m_currentTrackIndex < GetNumberTracks())
                   && IsLoaded(m_currentTrackIndex);
        }

        public override void Load()
        {
            LoadAll();
        }

        public override void Unload()
        {
            for (int musicIndex = 0; musicIndex < GetNumberTracks(); ++musicIndex)
            {
                if (IsLoaded(musicIndex))
                {
                    Unload(musicIndex);
                }
            }
        }


        #endregion

        #region Unity MonoBehaviour event functions

        protected override void Awake()
        {
            base.Awake();

            Assert.IsNotNull(files);
            Assert.IsTrue(files.All(file => file.IsValid()));
            
            Assert.IsTrue((files.Length == 0 || startingTrackIndex >= 0) && startingTrackIndex < files.Length);

            m_currentTrackIndex = -1;
            m_musics = new IMusic[files.Length];
        }

        void Start()
        {
            // Load at least one track at start-up, so that the jukebox be 'loaded'.
            if (m_musics.Length > 0)
            {
                SwitchTrack(startingTrackIndex);
            }
        }

        #endregion

        #region IJukebox implementation

        public void LoadAll()
        {
            for (int musicIndex = 0; musicIndex < GetNumberTracks(); ++musicIndex)
            {
                if (!IsLoaded(musicIndex))
                {
                    Load(musicIndex);
                }
            }
        }

        public void SwitchTrack(int musicIndex)
        {
            Assert.IsTrue(musicIndex >= 0 && musicIndex < GetNumberTracks());

            // load the track if not already done.
            if (!IsLoaded(musicIndex))
            {
                Load(musicIndex);
            }
            
            // set the new index.
            m_currentTrackIndex = musicIndex;

            // an Invoke a day keeps the bugs away!!
            jukeboxEvents.musicSwitchEvent.Invoke(GetMusic());
        }

        public void SwitchTrackNext()
        {
            SwitchTrack((GetCurrentTrackIndex() + 1) % GetNumberTracks());
        }
        
        public void SwitchTrackPrevious()
        {
            SwitchTrack((GetCurrentTrackIndex() - 1 + GetNumberTracks()) % GetNumberTracks());
        }


        public int GetNumberTracks()
        {
            return m_musics.Length;
        }

        public int GetCurrentTrackIndex()
        {
            return m_currentTrackIndex;
        }

        #endregion // IMusicPlayer implementation

        #region Event handling

        protected class JukeboxEvents
        {
            public readonly MusicSwitchEvent musicSwitchEvent;

            public JukeboxEvents()
            {
                musicSwitchEvent = new MusicSwitchEvent();
            }

        }

        public void AddMusicSwitchListener(UnityAction<IMusic> onMusicSwitch)
        {
            jukeboxEvents.musicSwitchEvent.AddListener(onMusicSwitch);
        }

        public void RemoveMusicSwitchListener(UnityAction<IMusic> onMusicSwitch)
        {
            jukeboxEvents.musicSwitchEvent.RemoveListener(onMusicSwitch);
        }

        #endregion // Event Handling

        #region Private utility



        void Unload(int musicIndex)
        {
            Assert.IsTrue(musicIndex >= 0 && musicIndex < GetNumberTracks());

            m_musics[musicIndex] = null;
        }

        void Load(int musicIndex)
        {
            Assert.IsTrue(musicIndex >= 0 && musicIndex < GetNumberTracks());
            if (IsLoaded(musicIndex))
            {
                // nothing to do.
                return;
            }
            //
            try
            {
                m_musics[musicIndex] = CreateMusic(files[musicIndex].track.bytes, files[musicIndex].info.text);
                m_musics[musicIndex].SetRepeatCount(-1);
                if (m_currentTrackIndex == -1)
                {
                    m_currentTrackIndex = musicIndex;
                }
                // 
                // time to notify the gang.
                trackSourceEvents.musicLoadEvent.Invoke(m_musics[musicIndex], true);
            }
            catch (System.ArgumentException)
            {
                UnityEngine.Debug.LogError("Failed to load MOD music.");
            }
        }

        bool IsLoaded(int musicIndex)
        {
            Assert.IsTrue(musicIndex >= 0 && musicIndex < GetNumberTracks());
            return m_musics[musicIndex] != null;
        }

        #endregion

        #region Protected data

        protected readonly JukeboxEvents jukeboxEvents = new JukeboxEvents();

        #endregion

        #region Private data

        IMusic[] m_musics;
        int m_currentTrackIndex;

        #endregion
    }
}