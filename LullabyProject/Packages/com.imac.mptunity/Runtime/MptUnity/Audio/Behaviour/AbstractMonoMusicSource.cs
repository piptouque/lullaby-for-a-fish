﻿
using UnityEngine.Assertions;

namespace MptUnity.Audio.Behaviour
{

    public abstract class AbstractMonoMusicSource : AbstractMusicSource 
    {
        #region Serialised

        public TrackFile file;

        #endregion

        #region AbstractMusicSource partial resolution

        public override IMusic GetMusic()
        {
            return m_music;
        }

        public override void Load()
        {
            if (IsLoaded())
            {
                // nothing to do.
                return;
            }
            try
            {
                m_music = CreateMusic(file.track.bytes, file.info.text);
                m_music.SetRepeatCount(-1);
                // time to notify the gang.
                trackSourceEvents.musicLoadEvent.Invoke(m_music, true);
            }
            catch (System.ArgumentException)
            {
                UnityEngine.Debug.LogError("Failed to load MOD music.");
            }
        }

        public override void Unload()
        {
            // nothing to do, RAII.
            // Note that due to garbage collection,
            // the music will actually be unloaded on next garbage collection.
            m_music = null;
        }

        public override bool IsLoaded()
        {
            return GetMusic() != null;
        }

        #endregion

        #region Unity MonoBehaviour event functions

        protected override void Awake()
        {
            base.Awake();
            
            Assert.IsTrue(file.IsValid());
        }

        void Start()
        {
            // todo: remove this.
            // We have no insurance that the manager will be ready if we try to load the file
            // in Awake, so we do it in Start instead.
            // This however requires that we check whether the player is ready
            // before calling Restart, Resume, Pause or Stop.
            Load();

        }
        #endregion

        #region Private data

        IMusic m_music;
        
        #endregion
    }

}