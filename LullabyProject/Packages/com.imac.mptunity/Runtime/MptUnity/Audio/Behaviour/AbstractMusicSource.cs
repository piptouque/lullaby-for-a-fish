﻿
using UnityEngine.Events;
using UnityEngine.Assertions;

namespace MptUnity.Audio.Behaviour
{
    public abstract class AbstractMusicSource : AbstractTrackSource, IMusicSource
    {

        #region Unity MonoBehaviour event functions


        protected override void Awake()
        {
            base.Awake();
            
            musicSourceEvents = new MusicSourceEvents();
        }

        #endregion

        #region IMusicPlayer implementation

        public override ITrack GetTrack()
        {
            return GetMusic();
        }

        public int NumberSections
        {
            get
            {
                Assert.IsTrue(IsLoaded());
                return GetMusic().NumberSections;
            }
        }

        public MusicSection GetSection(int sectionIndex)
        {
            Assert.IsTrue(IsLoaded());
            return GetMusic().GetSection(sectionIndex);
        }

        public int GetSectionIndex(string sectionName)
        {
            Assert.IsTrue(IsLoaded());
            return GetMusic().GetSectionIndex(sectionName);
        }

        public void MuteSection(int sectionIndex, bool mute)
        {
            Assert.IsTrue(IsLoaded());
            GetMusic().MuteSection(sectionIndex, mute);
            musicSourceEvents.muteSectionChangeEvent.Invoke(
                GetMusic().GetSection(sectionIndex), mute
            );
        }

        public bool IsSectionMuted(int sectionIndex)
        {
            Assert.IsTrue(IsLoaded());
            return GetMusic().IsSectionMuted(sectionIndex);
        }

        public void StopSectionNotes(int sectionIndex)
        {
            Assert.IsTrue(IsLoaded());
            GetMusic().StopSectionNotes(sectionIndex);

        }

        public void SetSectionVolume(int sectionIndex, double aVolume)
        {
            Assert.IsTrue(IsLoaded());
            GetMusic().SetSectionVolume(sectionIndex, aVolume);
        }

        public void ResetSectionVolumes()
        {
            // todo
            for (int i = 0; i < NumberSections; ++i)
            {
                SetSectionVolume(i, 1.0f);
            }
        }

        public double GetSectionVolume(int sectionIndex)
        {
            Assert.IsTrue(IsLoaded());
            return GetMusic().GetSectionVolume(sectionIndex);
        }

        public override void Reset()
        {
           base.Reset();
           ResetSectionVolumes();
        }

        #endregion // IMusicPlayer implementation

        #region Event handling

        protected class MusicSourceEvents
        {
            public readonly MuteSectionChangeEvent muteSectionChangeEvent;

            public MusicSourceEvents()
            {
                muteSectionChangeEvent = new MuteSectionChangeEvent();
            }

        }

        public void AddMuteSectionChangeListener(UnityAction<MusicSection, bool> onMuteSectionChange)
        {
            musicSourceEvents.muteSectionChangeEvent.AddListener(onMuteSectionChange);
        }

        public void RemoveMuteSectionChangeListener(UnityAction<MusicSection, bool> onMuteSectionChange)
        {
            musicSourceEvents.muteSectionChangeEvent.RemoveListener(onMuteSectionChange);
        }

        #endregion // Event Handling

        #region To resolve

        public abstract IMusic GetMusic();

        protected abstract IMusic CreateMusic(byte[] data, string info);


        #endregion

        #region Protected data 
            
        protected MusicSourceEvents musicSourceEvents;

        #endregion
        
    }

}