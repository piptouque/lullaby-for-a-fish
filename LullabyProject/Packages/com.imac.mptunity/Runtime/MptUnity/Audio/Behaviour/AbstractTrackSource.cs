﻿
using UnityEngine.Events;
using UnityEngine.Assertions;

namespace MptUnity.Audio.Behaviour
{
    [UnityEngine.RequireComponent(typeof(UnityEngine.AudioSource))]
    public abstract class AbstractTrackSource : UnityEngine.MonoBehaviour, ITrackSource
    {
        #region Unity MonoBehaviour event functions

        protected virtual void Awake()
        {
            source = GetComponent<UnityEngine.AudioSource>();
            Assert.IsNotNull(source);
            source.Stop();
            // In order to use our procedural filter as the input to the AudioSource,
            // we need to detach any potential AudioClip from it.
            // see: https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnAudioFilterRead.html
            source.clip = null;
            // must loop over the buffer, since it is refilled at each step.
            source.loop = true;

            SampleRate = MusicConfig.GetSampleRate();
            
            trackSourceEvents = new TrackSourceEvents();
            
        }


        void FixedUpdate()
        {
            SampleRate = MusicConfig.GetSampleRate();
        }

        #endregion

        #region IAudioSource

        public bool IsPaused()
        {
            return GetPlaybackState() == EAudioPlaybackState.ePaused;
        }

        public bool IsPlaying()
        {
            return GetPlaybackState() == EAudioPlaybackState.ePlaying;
        }

        public bool IsStopped()
        {
            return GetPlaybackState() == EAudioPlaybackState.eStopped;
        }

        public void Pause()
        {
            SetPlaybackState(EAudioPlaybackState.ePaused);
        }

        public void Play()
        {
            SetPlaybackState(EAudioPlaybackState.ePlaying);
        }

        /// <summary>
        /// Stops playback after having stopped the notes!
        /// </summary>
        public void Stop()
        {
            SetPlaybackState(EAudioPlaybackState.eStopped);
        }
        
        public void TogglePlay()
        {
            if (IsPlaying())
            {
                Pause();
            }
            else
            {
                Play();
            }
        }

        public void ToggleStop()
        {
            if (IsStopped())
            {
                Play();
            }
            else
            {
                Stop();
            }
        }

        #endregion

        #region Playback

        void SetPlaybackState(EAudioPlaybackState state)
        {
            // load it if it's not already the case.
            if (!IsLoaded())
            {
                Load();
            }

            SetStreamState(state, m_state);
            m_state = state;
            trackSourceEvents.playbackChangeEvent.Invoke(m_state);
        }


        public EAudioPlaybackState GetPlaybackState()
        {
            return m_state;
        }


        #endregion

        #region IMusicPlayer implementation

        public double GetPlayingTempo()
        {
            Assert.IsTrue(IsLoaded());
            return GetTrack().GetPlayingTempo();
        }

        public double GetTempoFactor()
        {
            Assert.IsTrue(IsLoaded());
            return GetTrack().GetTempoFactor();
        }

        public int GetRepeatCount()
        {
            Assert.IsTrue(IsLoaded());
            return GetTrack().GetRepeatCount();
        }

        public void SetRepeatCount(int count)
        {
            Assert.IsTrue(IsLoaded());
            GetTrack().SetRepeatCount(count);
        }

        public void SetTempoFactor(double tempo)
        {
            Assert.IsTrue(IsLoaded());
            
            GetTrack().SetTempoFactor(tempo);
            trackSourceEvents.bpmChangeEvent.Invoke(GetPlayingTempo());
        }

        public void MoveTempoFactor(double rate)
        {
            Assert.IsTrue(IsLoaded());
            SetTempoFactor(GetTempoFactor() * (1D + rate));
        }

        public double Volume
        {
            get => source.volume;
            set => source.volume = (float)value;
        }
        
        public int GetSpeed()
        {
            return GetTrack().GetSpeed();
        }

        public int GetCurrentRow()
        {
            return GetTrack().GetCurrentRow();
        }

        public virtual void Reset()
        {
            SetRepeatCount(-1);
            SetTempoFactor(1.0f);
            Volume = 1.0f;
        }

        public string GetTitle()
        {
            Assert.IsTrue(IsLoaded());
            return GetTrack().GetTitle();
        }

        public string GetAuthor()
        {
            Assert.IsTrue(IsLoaded());
            return GetTrack().GetAuthor();
        }

        public string GetComment()
        {
            Assert.IsTrue(IsLoaded());
            return GetTrack().GetMessage();
        }

        #endregion // IMusicPlayer implementation

        #region Event handling

        protected class TrackSourceEvents
        {
            public readonly MusicLoadEvent musicLoadEvent;
            public readonly PlaybackChangeEvent playbackChangeEvent;
            public readonly BpmChangeEvent bpmChangeEvent;

            public TrackSourceEvents()
            {
                musicLoadEvent = new MusicLoadEvent();
                playbackChangeEvent = new PlaybackChangeEvent();
                bpmChangeEvent = new BpmChangeEvent();
            }

        }

        public void AddMusicLoadListener(UnityAction<IMusic, bool> onMusicLoad)
        {
            trackSourceEvents.musicLoadEvent.AddListener(onMusicLoad);
        }

        public void AddBpmChangeListener(UnityAction<double> onBpmChange)
        {
            trackSourceEvents.bpmChangeEvent.AddListener(onBpmChange);
        }

        public void AddPlaybackChangeListener(UnityAction<EAudioPlaybackState> onPlaybackChange)
        {
            trackSourceEvents.playbackChangeEvent.AddListener(onPlaybackChange);
        }

        public void RemoveBpmChangeListener(UnityAction<double> onBpmChange)
        {
            trackSourceEvents.bpmChangeEvent.RemoveListener(onBpmChange);
        }

        public void RemovePlaybackChangeListener(UnityAction<EAudioPlaybackState> onPlaybackChange)
        {
            trackSourceEvents.playbackChangeEvent.RemoveListener(onPlaybackChange);
        }

        public void RemoveMusicLoadListener(UnityAction<IMusic, bool> onMusicLoad)
        {
            trackSourceEvents.musicLoadEvent.RemoveListener(onMusicLoad);
        }

        #endregion // Event Handling

        #region To resolve

        // public abstract IMusic GetMusic();
        public abstract ITrack GetTrack();

        public abstract void Unload();
        public abstract void Load();


        public abstract bool IsLoaded();

        /// <summary>
        /// It is required for MusicSources to use Unity's Filter API.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numberChannels"></param>
        public abstract void OnAudioFilterRead(float[] data, int numberChannels);


        #endregion

        #region Protected utility

        protected float[] GetAudioBuffer(int length, int numberChannels)
        {
            if (m_audioBuffer == null || m_audioBuffer.Length < length / numberChannels)
            {
                m_audioBuffer = new float[length / numberChannels];
            }
            return m_audioBuffer;
        }

        #endregion

        #region Protected data 
            
        protected TrackSourceEvents trackSourceEvents;

        protected UnityEngine.AudioSource source;
        
        protected int SampleRate { get; private set;  }

        #endregion

        #region Private utility
        
        /// <summary>
        /// Update the internal streaming according to playback state.
        /// </summary>
        /// <param name="updated"></param>
        /// <param name="previous"></param>
        void SetStreamState(EAudioPlaybackState updated, EAudioPlaybackState previous)
        {
            switch (updated)
            {
            case EAudioPlaybackState.ePaused:
                source.Pause();
                break;
            case EAudioPlaybackState.ePlaying:
                if (previous == EAudioPlaybackState.eStopped)
                {
                    source.Play();
                }
                else
                {
                    source.UnPause();
                }

                break;
            case EAudioPlaybackState.eStopped:
                GetTrack().Reset(); 
                source.Stop();
                break;
            }
        }
        #endregion

        #region Private data

        EAudioPlaybackState m_state;
        
        // IMPORTANT:
        // The audio buffer can't be shared or static,
        // since audio rendering of Filter instances happens simultaneously!!!
        /// <summary>
        /// Audio buffer utility for filter computations.
        /// </summary>
        float[] m_audioBuffer;

        #endregion

    }

}