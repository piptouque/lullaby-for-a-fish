﻿
using UnityEngine.Events;

namespace MptUnity.Audio.Behaviour
{
    
    public class MusicSwitchEvent : UnityEvent<IMusic>  {}
    
    public interface IJukebox : IMusicSource
    {

        void LoadAll();
        /// <summary>
        /// Resets the currently playing music, then switches playback to music loaded at index musicIndex.
        /// </summary>
        /// <param name="musicIndex"></param>
        void SwitchTrack(int musicIndex);
        void SwitchTrackNext();
        void SwitchTrackPrevious();
        int GetNumberTracks();
        int GetCurrentTrackIndex();

        void AddMusicSwitchListener(UnityAction<IMusic> onMusicSwitch);
        void RemoveMusicSwitchListener(UnityAction<IMusic> onMusicSwitch);

    }
}