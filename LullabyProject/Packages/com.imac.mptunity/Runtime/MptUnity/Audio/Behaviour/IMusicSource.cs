﻿using UnityEngine.Events;

namespace MptUnity.Audio.Behaviour
{
    public class MuteSectionChangeEvent : UnityEvent<MusicSection, bool> { }
    public interface IMusicSource : ITrackSource
    {

        IMusic GetMusic();
        
        int NumberSections { get;  }
        /// <summary>
        /// Set the volume for the section at index sectionIndex.
        /// </summary>
        /// <param name="sectionIndex"></param>
        /// <param name="volume">In [0, 1]</param>
        void SetSectionVolume(int sectionIndex, double volume);

        void ResetSectionVolumes();
        double GetSectionVolume(int sectionIndex);

        MusicSection GetSection(int sectionIndex);
        int GetSectionIndex(string sectionName);
        
        void MuteSection(int sectionIndex, bool mute);

        bool IsSectionMuted(int sectionIndex);
        
        /// <summary>
        /// Stops all currently playing notes in the section at sectionIndex.
        /// </summary>
        /// <param name="sectionIndex"></param>
        void StopSectionNotes(int sectionIndex);

        void AddMuteSectionChangeListener(UnityAction<MusicSection, bool> onMuteSectionChange);
        void RemoveMuteSectionChangeListener(UnityAction<MusicSection, bool> onMuteSectionChange);
        
    }
}