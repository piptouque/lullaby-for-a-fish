﻿using UnityEngine.Events;

namespace MptUnity.Audio.Behaviour
{
    public class PlaybackChangeEvent : UnityEvent<Audio.EAudioPlaybackState> { }
    public class MusicLoadEvent : UnityEvent<IMusic, bool> { }
    public class BpmChangeEvent : UnityEvent<double> { }
    public interface ITrackSource : IAudioSource
    {
        
        ITrack GetTrack();
        
        #region Playback (Unity Audio MonoBehaviour event)
        
        // Public interface forces this thing to be public
        // But it should be protected in the implementation.
        void OnAudioFilterRead(float[] data, int numberChannels);
        
        #endregion
        
        bool IsLoaded();
        /// <summary>
        /// Unload music file.
        /// </summary>
        void Unload();
        /// <summary>
        /// Load music file.
        /// todo: make asynchronous or launch thread, since the CreateMusic() operation can be pretty long.
        /// </summary>
        void Load();
        
        /// <summary>
        /// Get current tempo as modified by the tempo factor.
        /// </summary>
        /// <returns></returns>
        double GetPlayingTempo();
        /// <summary>
        /// Get current tempo modifier.
        /// </summary>
        /// <returns></returns>
        double GetTempoFactor();
        /// <summary>
        /// Set tempo modifier.
        /// </summary>
        /// <returns></returns>
        void SetTempoFactor(double factor);

        /// <summary>
        /// Multiplies current tempo factor by `factor * (1 + rate)`.
        /// </summary>
        /// <param name="rate"></param>
        void MoveTempoFactor(double rate);
        
        int GetSpeed();

        int GetCurrentRow();
        
        string GetTitle();
        string GetAuthor();
        string GetComment();

        /// <summary>
        /// Sets the current music repeat state.
        ///   -1 loops indefinitely.
        ///   0 plays once.
        ///   n > 0 plays once then repeats n times.
        /// </summary>
        /// <param name="count"></param>
        void SetRepeatCount(int count);
        int GetRepeatCount();

        void Reset();


        void AddPlaybackChangeListener(UnityAction<EAudioPlaybackState> onPlaybackChange);
        void AddMusicLoadListener(UnityAction<IMusic, bool> onMusicLoad);
        void AddBpmChangeListener(UnityAction<double> onBpmChange);
        void RemovePlaybackChangeListener(UnityAction<EAudioPlaybackState> onPlaybackChange);
        void RemoveMusicLoadListener(UnityAction<IMusic, bool> onMusicLoad);
        void RemoveBpmChangeListener(UnityAction<double> onBpmChange);
    }
}