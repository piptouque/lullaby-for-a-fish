﻿
namespace MptUnity.Audio.Behaviour
{
    [UnityEngine.RequireComponent(typeof(UnityEngine.AudioSource))]
    public class OpenMptInstrumentSource : AbstractInstrumentSource
    {

        #region AbstractInstrumentSource resolution

        protected override IInstrument CreateInstrument(byte[] data, string info, int aNumberVoices)
        {
            return new OpenMptInstrument(data, info, aNumberVoices);
        }

        #endregion

        #region Unity MonoBehaviour event


        public override void OnAudioFilterRead(float[] data, int numberChannels)
            {
                if (IsLoaded())
                {
                     var audioBuffer = GetAudioBuffer(data.Length, numberChannels);
                    MptUnity.Utility.UnityAudioUtility.ReadMusicAudioFilter(
                        GetTrack(), 
                        data, 
                        audioBuffer,
                        numberChannels, 
                    SampleRate);
                }
            }

        #endregion

    }
}