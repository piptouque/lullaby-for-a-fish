﻿#define FORCE_MONO

namespace MptUnity.Audio.Behaviour
{
    [UnityEngine.RequireComponent(typeof(UnityEngine.AudioSource))]
    public class OpenMptJukebox : AbstractJukebox
    {
        
        #region AbstractJukebox resolution

        protected override IMusic CreateMusic(byte[] data, string info)
        {
            return new OpenMptMusic(data, info);
        }

        #endregion

        #region Unity Filter behaviour

        public override void OnAudioFilterRead(float[] data, int numberChannels)
        {
            if (IsLoaded())
            {
                var audioBuffer = GetAudioBuffer(data.Length, numberChannels);
                MptUnity.Utility.UnityAudioUtility.ReadMusicAudioFilter(
                    GetTrack(), 
                    data, 
                    audioBuffer,
                    numberChannels, 
                SampleRate);
            }
        }

        #endregion
    }
}