﻿namespace MptUnity.Audio
{
    /// <summary>
    /// An interface for a the instrument the musician is playing.
    /// </summary>
    public interface IInstrument : ITrack, IAudioStream
    {
        
        #region Playing
        /// <summary>
        /// 
        /// </summary>
        /// <param name="voice"></param>
        /// <returns>Success.</returns>
        bool StopNote(int voice);
        /// <summary>
        /// Plays the MusicalNote on the instrument.
        /// The instrument will keep playing until StopNote is called.
        /// </summary>
        /// <param name="note"></param>
        /// <returns>id of the playing voice, -1 on failure.</returns>
        int PlayNote(MusicalNote note);

        void StopAllNotes();

        /// <summary>
        /// Checks whether the voice is currently playing, meaning that is can be stopped.
        /// </summary>
        /// <param name="voice"></param>
        /// <returns></returns>
        bool CanStop(int voice);
        /// <summary>
        /// Checks whether the note can currently be played by the Instrument.
        /// </summary>
        /// <param name="note"></param>
        /// <returns></returns>
        bool CanPlay(MusicalNote note);

        int GetNumberVoices();

        /// <summary>
        /// Get note currently playing at voice.
        /// 
        /// </summary>
        /// <param name="voice">In [0, numberVoice-1]</param>
        /// <returns></returns>
        MusicalNote GetNote(int voice);

        /// <summary>
        /// Sets polyphony (maximum number concurrently playing notes.)
        /// </summary>
        /// <param name="numberVoices"></param>
        void SetNumberVoices(int numberVoices);

        #endregion
    }
}