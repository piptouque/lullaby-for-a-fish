﻿namespace MptUnity.Audio
{
        
    
    public interface IMusic : ITrack
    {

        #region Settings and info

        int NumberSections { get; }
        
        MusicSection GetSection(int sectionIndex);
        int GetSectionIndex(string sectionName);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionIndex"></param>
        /// <param name="volume">In [0, 1]</param>
        void SetSectionVolume(int sectionIndex, double volume);
        void ResetSectionVolume(int sectionIndex);
        double GetSectionVolume(int sectionIndex);

        /// <summary>
        /// Stop all currently playing notes in the section at sectionIndex.
        /// </summary>
        /// <param name="sectionIndex"></param>
        void StopSectionNotes(int sectionIndex);
        /// <summary>
        /// Sets volume the section to 0 or previously set volume.
        /// </summary>
        /// <param name="sectionIndex"></param>
        /// <param name="mute">if true, then set to 0, else set to previous volume.</param>
        void MuteSection(int sectionIndex, bool mute);

        bool IsSectionMuted(int sectionIndex);
        void ResetSectionMuteStatus(int sectionIndex);
        
        #endregion
    }
}