﻿namespace MptUnity.Audio
{
        
    
    public interface ITrack : IAudioStream
    {

        #region Error Handling
        string GetLastErrorMessage();
        
        #endregion
        
        #region Settings and info

        double GetPlayingTempo();
        double GetTempoFactor();
        void SetTempoFactor(double factor);

        int GetSpeed();

        int GetCurrentRow();
        
        int NumberChannels { get;  }
        int NumberInstruments { get;  }
        
        string GetAuthor();

        string GetTitle();
        string GetMessage();


        /// <summary>
        /// Sets the music repeat state.
        ///   -1 loops indefinitely.
        ///   0 plays once.
        ///   n > 0 plays once then repeats n times.
        /// </summary>
        /// <param name="count"></param>
        void SetRepeatCount(int count);
        int GetRepeatCount();
        
        /// <summary>
        /// Resets BPM, volumes, repeat and mute state and rewinds the music.
        /// </summary>
        void Reset();
        
        #endregion
    }
}