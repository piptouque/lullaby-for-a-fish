﻿namespace MptUnity.Audio
{
    public class InstrumentInfo
    {
        // id of the instrument(s) in the MOD.
        public readonly int[] instruments;

        public InstrumentInfo(int[] a_instruments)
        {
            instruments = (int[]) a_instruments.Clone();
        }
    }
}