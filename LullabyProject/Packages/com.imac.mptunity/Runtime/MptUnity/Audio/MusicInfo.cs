﻿
namespace MptUnity.Audio
{
    
    /// <summary>
    /// Info on a MOD music file.
    /// Includes number of sections, their associated channels,
    /// and the patterns.
    /// </summary>
    public class MusicInfo
    {
        readonly MusicSection[] m_sections;

        public MusicInfo(MusicSection[] sections)
        {
            // shallow copy will be enough here
            m_sections = (MusicSection[]) sections.Clone();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns> Number of sections for this music</returns>
        public int GetNumberSections()
        {
            return m_sections.Length;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns> array of all the channels owned by section at partIndex.</returns>
        public int[] GetSectionChannels(int sectionIndex)
        {
            return GetSection(sectionIndex).channels;
        }

        public MusicSection GetSection(int sectionIndex)
        {
            return m_sections[sectionIndex];
        }

        public int GetSectionIndex(string sectionName)
        {
            return System.Array.FindIndex(m_sections, section => section.name == sectionName);
        }

    }
}