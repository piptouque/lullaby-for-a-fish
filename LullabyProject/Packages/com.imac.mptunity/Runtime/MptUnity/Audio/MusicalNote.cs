﻿namespace MptUnity.Audio
{
    /// <summary>
    /// A class for a note played by an Instrument.
    /// Should be one-time use, and constant.
    /// </summary>
    public class MusicalNote
    {
        public readonly int tone;

        public readonly double volume;
        
        public readonly double panning;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aTone">MIDI tone of the note, in semi-tones.</param>
        /// <param name="aVolume">In [0, 1]</param>
        /// <param name="aPanning">In [0, 1]</param>
        public MusicalNote(int aTone, double aVolume = 1.0, double aPanning = 0.5)
        {
            tone = aTone;
            volume = aVolume;
            panning = aPanning;
        }

        /// <summary>
        /// Default, a note which is not playing.
        /// </summary>
        public MusicalNote()
        {
            tone = -1;
            volume = 0.0;
            panning = 0.5;
        }

        public MusicalNote(MusicalNote other)
            : this(other.tone, other.volume, other.panning)
        {
            
        }

        public bool IsPlaying()
        {
            return tone != -1;
        }

    }
}