﻿using MptUnity.Utility;

namespace MptUnity.Audio
{
    public class OpenMptMusic : AbstractOpenMptTrack, IMusic
    {

        #region Life-cycle

        
        public OpenMptMusic(byte[] data, string info) 
            : base(data)
        {
            // parse the info (number of channels etc.) from the message.
            m_info = MusicInfoParser.ParseMusicInfo(info);
            
            // Init sections.
            m_sectionVolumes = new double[m_info.GetNumberSections()];
            m_sectionVolumes.Fill(1.0);

            m_sectionMuted = new bool[m_info.GetNumberSections()];
            m_sectionMuted.Fill(false);
        }
        
        #endregion

        #region IMusic implementation

        public int NumberSections => m_info.GetNumberSections();
        public MusicSection GetSection(int sectionIndex)
        {
            return m_info.GetSection(sectionIndex);
        }

        public int GetSectionIndex(string sectionName)
        {
            return m_info.GetSectionIndex(sectionName);
        }

        public  void MuteSection(int sectionIndex, bool mute)
        {
            foreach (int channel in m_info.GetSectionChannels(sectionIndex))
            {
                moduleExt.GetInteractive().SetChannelMuteStatus(channel, mute);
            }
            m_sectionMuted[sectionIndex] = mute;
        }
        
        public void SetSectionVolume(int sectionIndex, double volume)
        {
            foreach (int channel in m_info.GetSectionChannels(sectionIndex))
            {
                moduleExt.GetInteractive().SetChannelVolume(channel, volume);
                m_sectionVolumes[sectionIndex] = volume;
            }
        }
        
        public void ResetSectionVolume(int sectionIndex)
        {
            SetSectionVolume(sectionIndex, 1.0);
        }
        
        public double GetSectionVolume(int sectionIndex)
        {
            return m_sectionVolumes[sectionIndex];
        }

        public  void StopSectionNotes(int sectionIndex)
        {
            foreach (int channel in m_info.GetSectionChannels(sectionIndex))
            {
                moduleExt.GetInteractive().StopNote(channel);
            }
        }

        public  bool IsSectionMuted(int sectionIndex)
        {
            return m_sectionMuted[sectionIndex];
        }

        public void ResetSectionMuteStatus(int sectionIndex)
        {
            MuteSection(sectionIndex, false);
        }

        public override void Reset()
        {
            base.Reset();
            
            for (int i = 0; i < NumberSections; ++i)
            {
                ResetSectionVolume(i);                
                ResetSectionMuteStatus(i);
            }
            
            // Don't forget to set the track to loop!
            SetRepeatCount(-1);
        }

        #endregion

        #region Internal Data

        readonly MusicInfo m_info;

        readonly double[] m_sectionVolumes;
        readonly bool[] m_sectionMuted;
        
        #endregion
        
    }
}