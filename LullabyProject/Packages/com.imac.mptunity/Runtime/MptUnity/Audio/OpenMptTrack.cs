﻿using MptUnity.Utility;

namespace MptUnity.Audio
{
    public abstract class AbstractOpenMptTrack : ITrack
    {
        #region Life-cycle
        
        protected AbstractOpenMptTrack(byte[] data) 
        {
            // Load file and get handle to music.
            moduleExt =  OpenMptUtility.LoadModuleExt(data);
            if (moduleExt == null)
            {
                throw new System.ArgumentException("Failed to load MOD music.");
            }
        }
        
        #endregion

        #region IMusic implementation

        public  string GetLastErrorMessage()
        {
            return moduleExt.GetModule().ErrorGetLastMessage();
        }

        public  double GetPlayingTempo()
        {
            return moduleExt.GetModule().GetCurrentTempo()
                   * moduleExt.GetInteractive().GetTempoFactor();
        }
        
        public  double GetTempoFactor()
        {
            return moduleExt.GetInteractive().GetTempoFactor();
        }


        public void SetTempoFactor(double factor)
        {
            // The tempo can be reset at any time by the music.
            // We need to apply an independent modifier.
            moduleExt.GetInteractive().SetTempoFactor(factor);
        }
        
        public int GetSpeed()
        {
            return moduleExt.GetModule().GetCurrentSpeed();
        }

        public int GetCurrentRow()
        {
            return moduleExt.GetModule().GetCurrentRow();
        }

        public int NumberChannels => moduleExt.GetModule().GetNumChannels();

        public int NumberInstruments => moduleExt.GetModule().GetNumInstruments();

        public string GetMessage()
        {
            return OpenMptUtility.GetModuleExtMessage(moduleExt);
        }


        public void SetRepeatCount(int count)
        {
            moduleExt.GetModule().SetRepeatCount(count);
        }

        public int GetRepeatCount()
        {
            return moduleExt.GetModule().GetRepeatCount();
        }


        public virtual void Reset()
        {
            moduleExt.GetModule().SetPositionOrderRow(0, 0);
            moduleExt.GetModule().SetRepeatCount(0);
            
            SetTempoFactor(1D);
        }

        public string GetAuthor()
        {
            return OpenMptUtility.GetModuleExtAuthor(moduleExt);
        }
        
        public string GetTitle()
        {
            return OpenMptUtility.GetModuleExtTitle(moduleExt);
        }
        
        
        
        #endregion

        #region IAudioStream implementation

        public long Read(int sampleRate, long count, float[] mono)
        {
            return moduleExt.GetModule().Read(sampleRate, count, mono);
        }

        public long ReadInterleavedQuad(int sampleRate, long count, float[] interleavedQuad)
        {
            return moduleExt.GetModule().ReadInterleavedQuad(sampleRate, count, interleavedQuad);
        }

        public long ReadInterleavedStereo(int sampleRate, long count, float[] interleavedStereo)
        {
            return moduleExt.GetModule().ReadInterleavedStereo(sampleRate, count, interleavedStereo);
        }

        #endregion

        #region Protected data

        protected readonly OpenMpt.ModuleExt moduleExt;
        
        #endregion
        
    }
}