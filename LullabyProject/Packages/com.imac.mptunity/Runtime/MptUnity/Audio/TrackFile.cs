namespace MptUnity.Audio
{
    using TrackData = UnityEngine.TextAsset;
    using InfoData = UnityEngine.TextAsset;
    
    [System.Serializable]
    public struct TrackFile
    {
        public TrackData track;
        public InfoData info;

        public bool IsValid()
        {
            return track != null && info != null;
        }
    }
}