﻿
namespace MptUnity.Utility
{
    using T = System.Single;
    // see: https://github.com/cantina-lib/cantina_common/blob/master/inline/cant/maths/approx.inl
    static class ApproxFloat
    {
        const int c_ulpDefault = 2;

        public static bool AreApproximatelyEqual(T a, T b, int ulp = c_ulpDefault)
        {
            // see example:
            // https://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
            T dist = System.Math.Abs(b - a);
            return dist <= GetErrorMargin(a, b, ulp) || dist < GetMin();
        }

        public static bool IsApproximatelyLowerOrEqual(T a, T b, int ulp = c_ulpDefault)
        {
            return a <= b - GetErrorMargin(a, b, ulp);
        }

        static T GetErrorMargin(T a, T b, int ulp)
        {
            return GetEpsilon() * System.Math.Abs(b - a) * ulp;
        }

        static T GetEpsilon()
        {
            return T.Epsilon;
        }

        static T GetMin()
        {
            return T.MinValue;
        }
    }
}