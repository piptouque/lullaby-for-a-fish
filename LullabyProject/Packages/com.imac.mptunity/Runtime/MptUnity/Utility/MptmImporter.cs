﻿
#if UNITY_EDITOR

using System.IO;


namespace MptUnity.Utility
{
    // taken from:
    // https://forum.unity.com/threads/loading-a-file-with-a-custom-extension-as-a-textasset.625078/#post-5839426
    [UnityEditor.AssetImporters.ScriptedImporter(1, "mptm")]
    public class MptmImporter : UnityEditor.AssetImporters.ScriptedImporter
    {
        public override void OnImportAsset(UnityEditor.AssetImporters.AssetImportContext ctx)
        {
            byte[] data = File.ReadAllBytes(ctx.assetPath);
            // add the bytes extension.
            string textPath = Path.ChangeExtension(ctx.assetPath, "mptm.bytes");
            
            File.WriteAllBytes(textPath, data);
        }
    }
}

#endif