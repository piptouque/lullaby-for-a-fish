
using System;

namespace MptUnity.Utility
{
    // Can't manage to make this one generic.
    // It's far from necessary anyway.
    using TSlide = System.Single;
    using TValue = System.Single;
    /// <summary>
    /// Copied from C++ version :
    /// https://github.com/cantina-lib/cantina_common/blob/master/include/cant/patterns/Slider.hpp
    /// </summary>
    public class Slider
    {
        #region Public

        public Slider(Func<TSlide, TValue> speedFunc)
        {
            m_speedFunc = speedFunc;
        }

        public void UpdateDelta(TSlide dt)
        {
            if (m_isDone)
            {
                return;
            }
           // todo 
           var previousDiff = m_target - m_valueCurrent;
           int dir = Math.Sign(previousDiff);
           m_valueCurrent += dir * m_speedFunc(dt);
           var diff = m_target - m_valueCurrent;
           if (Math.Sign(diff) != Math.Sign(previousDiff))
           {
               m_valueCurrent = m_target;
               m_isDone = true;
           }
        }

        public TValue GetValue()
        {
           return m_valueCurrent;
        }

        public void SetTarget(TValue target)
        {
            m_target = target;
            m_isDone = false;
        }

        public void SetSpeedFunctor(Func<TSlide, TValue> speedFunc)
        {
            m_speedFunc = speedFunc;
        }
        
        #endregion

        #region Private

        Func<TSlide, TValue> m_speedFunc;

        TValue m_target;
        TValue m_valueCurrent;
        bool m_isDone;
        

        #endregion
    }
}