
#define FORCE_MONO

using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Assertions;

using MptUnity.Audio;

namespace MptUnity.Utility
{
    public static class UnityAudioUtility
    {

        /// <summary>
        /// Changes playback speed of audio group without changing the pitch.
        /// Requires that audioSources belong to group.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="sources"></param>
        /// <param name="speed"></param>
        /// <param name="propertyName"></param>
        public static void StretchAudioGroup(AudioMixerGroup group, AudioSource[] sources, float speed, string propertyName)
        {
            // Awesome hack to time stretch an audio using both AudioSource and AudioMixerSpeed
            // see: https://forum.unity.com/threads/change-speed-in-music-sound.502429/#post-3839620
            foreach (var source in sources)
            {
                // just a consistency check.
                // Assert.IsTrue(source.outputAudioMixerGroup == group);
                source.pitch = speed;
            }
            group.audioMixer.SetFloat(propertyName, 1f / speed);
        }

        public static void CopyAudioSourceSettings(AudioSource from, AudioSource to)
        {
            // I can't find any resources online about how to duplicate a MonoBehaviour,
            // WITHOUT duplicating the GameObject it's attached to (Instantiate).
            // so we'll do this the hard, silly way.
            to.clip = from.clip;
            to.loop = from.loop;
            to.mute = from.mute;
            to.pitch = from.pitch;
            to.priority = from.priority;
            to.spatialize = from.spatialize;
            to.spread = from.spread;
            to.time = from.time;
            to.volume = from.volume;
            to.bypassEffects = from.bypassEffects;
            to.dopplerLevel = from.dopplerLevel;
            // to.isPlaying = from.isPlaying; read-only
            // to.isVirtual = from.isVirtual; read-only
            to.maxDistance = from.maxDistance;
            to.minDistance = from.minDistance;
            to.panStereo = from.panStereo;
            to.rolloffMode = from.rolloffMode;
            to.spatialBlend = from.spatialBlend;
            to.timeSamples = from.timeSamples;
            to.bypassListenerEffects = from.bypassListenerEffects;
            to.bypassReverbZones = from.bypassReverbZones;
            to.ignoreListenerPause = from.ignoreListenerPause;
            to.ignoreListenerVolume = from.ignoreListenerVolume;
            to.playOnAwake = from.playOnAwake;
            to.reverbZoneMix = from.reverbZoneMix;
            to.spatializePostEffects = from.spatializePostEffects;
            to.velocityUpdateMode = from.velocityUpdateMode;
            to.outputAudioMixerGroup = from.outputAudioMixerGroup;
            // that was annoying.
        }
        
        public static void ReadMusicAudioFilter(IAudioStream stream, float[] data, float[] audioBuffer, int channels, int sampleRate)
        {
            #if FORCE_MONO
            // [piptouque]: This seems to work.
            // It's honestly a proper fix, but what bothers me
            // is that the issue might come from the C# bindings of OpenMPT,
            // or me just not RTFM.
            // todo.
            long numberFrames = ForceReadMono(stream, data, audioBuffer, channels, sampleRate);
            #else
            long numberFrames = 0;
            switch (channels)
            {
            case 1:
                numberFrames = stream.Read(sampleRate, data.Length, data);
                break;
            case 2:
                numberFrames = stream.ReadInterleavedStereo(sampleRate, data.Length, data);
                break;
            case 4:
                numberFrames = stream.ReadInterleavedQuad(sampleRate, data.Length, data);
                break;
            default:
                UnityEngine.Debug.LogError("Number of channels not supported: " + channels);
                break;
            }
            #endif
        }

        #region Private utility

        /// <summary>
        /// Fills mono-channel data with multi-channel stream by duplicating samples.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        /// <param name="audioBuffer"></param>
        /// <param name="channels"></param>
        /// <param name="sampleRate"></param>
        /// <returns></returns>
        static long ForceReadMono(IAudioStream stream, float[] data, float[] audioBuffer, int channels, int sampleRate)
        {
            Assert.IsTrue(audioBuffer.Length >= data.Length / channels);

            long numberFrames = stream.Read(sampleRate, audioBuffer.Length, audioBuffer);
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = audioBuffer[i / channels];
            }

            return numberFrames;
        }

        #endregion
    }
}
