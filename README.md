## Lullaby For a Fish

## Project Development 

- Unity 2021.1.0f1
- Blender

## Commands

- Z : Forward
- Q : Left
- D : Right
- S : Backward
- Y / U / I / O / P : flute notes
- echap : pause menu
- shift : to run
- tab : music mood mode (can be switch automatic or manual in pause menu)

## Cheats Code For Melody

- Melody Didacticiel : Y - U - Y
- Melody Zone 1 : Y - U - I - Y - U
- Melody Zone 2 : P - I - Y - I - Y
- Melody Zone 3 : O - P - I - O - I

## Credits

- Programmers:
  - Louisa CHIKAR
  - Monica LISACEK
  - Line RATHONIE
  - Pierre THIEL
  - Roxane VALLEE

- Music:
  - Thomas BINGHAM :
    - [Personal](https://soundcloud.com/user-434305798)
    - [Soundcloud](https://soundcloud.com/user-434305798)
  - Hugo THIEL :
    - [Soundcloud](https://soundcloud.com/user-48833542-617268687) 

- A story told by Sarai LAMBERT

### Attributions

- Instrument samples taken VSCO: Community Edition, from  Versilian Studios, Sam Gossner, Ivy Audio and Simon Dalzell under CC0 1.0 Universal.
  - Source vailable here: https://github.com/sgossner/VSCO-2-CE
- Sound effects taken from <a href='https://freesound.org/' title='FreeSound' target='_blank'>FreeSound.org</a>, all under CC 0 licence.
- Footstep effect Pas sur cailloux by Joseph Sardin https://lasonotheque.org/detail-0839-pas-sur-cailloux.html
- Background audio tracks provided by <a href='https://noises.online' title='Noises.Online' target='_blank'>Noises.Online</a> under a <a href='http://creativecommons.org/licenses/by/3.0/' title='Creative Commons BY 3.0' target='_blank'>CC 3.0 BY</a> licence.

- Imported models from Sketchfab website :
   - "Low Poly Camping" (https://skfb.ly/6X9JX) by tolotedesign is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/). 
   - "Stove" (https://skfb.ly/XvDR) by Splodeman is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
   - "Double Bed 3" (https://skfb.ly/6WWCL) by rhcreations is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/). 
   - "Old Chest Of Drawers 1" (https://skfb.ly/6WpwH) by rhcreations is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
   - "Old wooden chair" (https://skfb.ly/6WOxv) by Aartee is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
